FROM node:16.13.1

EXPOSE 5000

# Use latest version of npm
RUN npm i npm@latest -g

COPY package.json package-lock.json* ./

RUN npm install --no-optional && npm cache clean --force
RUN npm install -g nodemon

# copy in our source code last, as it changes the most
WORKDIR /app
COPY . .

CMD [ "nodemon", "index.js" ]