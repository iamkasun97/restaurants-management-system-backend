const app = require("./app");
const debug = require("debug")("node-angular");
const http = require("http");
var fs = require("fs");
const db = require("./models");
const socketio = require("socket.io");

const normalizePort = (val) => {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
};

const onError = (error) => {
  if (error.syscall !== "listen") {
    throw error;
  }
  const bind = typeof addr === "string" ? "pipe " + addr : "port " + port;
  switch (error.code) {
    case "EACCES":
      console.error(bind + " requires elevated privileges");
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(bind + " is already in use");
      process.exit(1);
      break;
    default:
      throw error;
  }
};

const onListening = () => {
  const addr = server.address();
  const bind = typeof addr === "string" ? "pipe " + addr : "port " + port;
  debug("Listening on " + bind);
};

const port = normalizePort(process.env.EXTERNAL_PORT || "5000");
app.set("port", port);

const server = http.createServer(app);
const io = socketio(server);

// // where the scoket io initialize
// io.on("connection" , (socket) => {
//   socket.emit('login' , {userName : 'kasun'});

//   socket.on('updateProduct' , productDetails => {
//     console.log(productDetails);
//     socket.emit('updatedDetails' , productDetails);
//   })

//   socket.on('disconnect' , () => {
//     io.emit('loginout' , 'user left the chat');
//   });
// });


server.on("error", onError);
server.on("listening", onListening);

db.sequelize.sync().then((req) => {
  server.listen(port, () => {
    console.log("Auth Server Started...");
    console.log("Listening on port 5000 auth");
  });
});

