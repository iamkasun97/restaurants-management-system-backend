const jwt = require("jsonwebtoken");
require("dotenv").config();
const config = require("../config");

exports.extractToken = async (req , res , next) => {
    const customError = Object.assign({}, config.custom_error);
    try{
        let token = req.headers.authorization.split(' ')[1];
        let data = jwt.verify(token, process.env.SECRET_TOKEN);
        if(data.role.includes('owner')){
            req.body.userId = data.userId;
            next();
        }else {
            throw new Error();
        }
    }catch(error){
        customError.comment = 'unauthorized access!!';
        return res.status(401).json(customError);
    }
}


exports.extractToken1 = async (req , res , next) => {
    const customError = Object.assign({}, config.custom_error);
    try{
        let token = req.headers.authorization.split(' ')[1];
        let data = jwt.verify(token, process.env.SECRET_TOKEN);
        if(data.role.includes('owner') ){
            req.body.userId = data.userId;
            req.body.role = data.role;
            next();
        }else if(data.role.includes('manager')){
            req.body.role = data.role;
            req.body.employeeId = data.employeeId
            next();
        }
        else {
            throw new Error();
        }
    }catch(error){
        customError.comment = 'unauthorized access!!';
        return res.status(401).json(customError);
    }
}


exports.extractToken2 = async (req , res , next) => {
    const customError = Object.assign({}, config.custom_error);
    try{
        let token = req.headers.authorization.split(' ')[1];
        let data = jwt.verify(token, process.env.SECRET_TOKEN);
        if(data.role.includes('owner') ){
            req.body.userId = data.userId;
            req.body.role = data.role;
            next();
        }else if(data.role.includes('manager')){
            req.body.role = data.role;
            req.body.employeeId = data.employeeId
            next();
        }else if(data.role.includes('waiter')){
            req.body.role = data.role;
            req.body.employeeId = data.employeeId
            next();
        }
        else {
            throw new Error();
        }
    }catch(error){
        customError.comment = 'unauthorized access!!';
        return res.status(401).json(customError);
    }
}

exports.extractToken3 = async (req , res , next) => {
    const customError = Object.assign({}, config.custom_error);
    try{
        let token = req.headers.authorization.split(' ')[1];
        let data = jwt.verify(token, process.env.SECRET_TOKEN);
        if(data.role.includes('owner') ){
            req.body.userId = data.userId;
            req.body.role = data.role;
            next();
        }else if(data.role.includes('manager')){
            req.body.role = data.role;
            req.body.employeeId = data.employeeId
            next();
        }else if(data.role.includes('chef')){
            req.body.role = data.role;
            req.body.employeeId = data.employeeId
            next();
        }
        else {
            throw new Error();
        }
    }catch(error){
        customError.comment = 'unauthorized access!!';
        return res.status(401).json(customError);
    }
}