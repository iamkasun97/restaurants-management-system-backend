const express = require("express");
const bodyParse = require("body-parser");
const cors = require('cors');
const path = require('path');

const configureRoutes = require('./routes/configure');
const registrationRoutes = require('./routes/registration');
const loginRoutes = require('./routes/login');
const menuRouets = require('./routes/menu');
const promotionRoutes = require('./routes/promotions');
const orderRoutes = require('./routes/order');
const tableRoutes = require('./routes/table');
const departmentOrderRoutes = require('./routes/departmentOrder');
const paymentRoute = require('./routes/payment');




const app = express();

app.use(bodyParse.json({limit: '2mb'}));
app.use(bodyParse.urlencoded({extended:true , parameterLimit:100000,limit:"2mb"}));
app.use(cors());
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization,RefreshToken,RegisterToken,VerifyMobileToken,ChangePasswordToken,AddNewPasswordtoken,AddVerifyNewPasswordtoken,AdminAuthorization,GetTwoStepAuthToken"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, DELETE, OPTIONS"
  );
  next();
});

app.use(bodyParse.urlencoded({ extended: false }));
app.set("trust proxy", true); // trust first proxy
app.use('/images', express.static('images'));
app.use(express.static(path.join(__dirname , 'public')));


//user routes
app.use("/api/login" , loginRoutes);
app.use("/api/registration" , registrationRoutes);
app.use("/api/configure" , configureRoutes);
app.use("/api/menu" , menuRouets);
app.use("/api/promotion" , promotionRoutes);
app.use("/api/order" , orderRoutes);
app.use("/api/table" , tableRoutes);
app.use("/api/department-order" , departmentOrderRoutes);
app.use("/api/payment" , paymentRoute);

app.all("*", async (req, res, next) => {
  // next(new NotFoundError());
  return res.status(404).json({
    status : 'not found',
    comment : "not found",
    data : ''
});
});
// app.use(errorHandler);

module.exports = app;
