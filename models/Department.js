module.exports = (sequelize , Datatypes) => {
    const department = sequelize.define("department" , {
        departmentName : {
            type : Datatypes.STRING,
        },
        departmentDescription : {
            type : Datatypes.STRING,
        },
        departmentStatus : {
            type : Datatypes.INTEGER
        },
        statusComment : {
            type : Datatypes.STRING
        },
        ownerId : {
            type : Datatypes.INTEGER,
            references : {
                model : "owners",
                key : "id"
            }
        }
    });

    return department;
}