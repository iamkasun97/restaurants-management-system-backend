module.exports = (sequelize , Datatypes) => {
    const promotion_item = sequelize.define("promotion_item" , {
        promotionId : {
            type : Datatypes.INTEGER,
            references : {
                model : "promotions",
                key : "id"
            }
        },
        itemId : {
            type : Datatypes.INTEGER,
            references : {
                model : 'items',
                key : "id"
            }
        },
        quantity : {
            type : Datatypes.INTEGER
        }
    });

    return promotion_item;
}