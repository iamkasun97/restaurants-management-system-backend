module.exports = (sequelize , Datatypes) => {
    const item = sequelize.define("item" , {
        itemName : {
            type : Datatypes.STRING,
        },
        itemDescription : {
            type : Datatypes.STRING,
        },
        keywords : {
            type : Datatypes.STRING,
        },
        unit : {
            type : Datatypes.STRING
        },
        unitAmount : {
            type : Datatypes.STRING
        },
        unitCost : {
            type : Datatypes.DOUBLE,
        },
        unitPrice : {
            type : Datatypes.DOUBLE,
        },
        serviceCharge : {
            type : Datatypes.DOUBLE
        },
        serviceChargeType : {
            type : Datatypes.INTEGER,
        },
        avgPrepareTime : {
            type : Datatypes.INTEGER
        },
        serveTime : {
            type : Datatypes.STRING,
        },
        availability : {
            type : Datatypes.INTEGER
        },
        image : {
            type : Datatypes.STRING
        },
        hotelId : {
            type : Datatypes.INTEGER,
            references : {
                model : "hotels",
                key : "id"
            }
        },
        restaurantId : {
            type : Datatypes.INTEGER,
            references : {
                model : 'restaurants',
                key : "id"
            }
        },
        departmentId : {
            type : Datatypes.INTEGER,
            references : {
                model : 'departments',
                key : 'id'
            }
        }
    });

    return item;
}