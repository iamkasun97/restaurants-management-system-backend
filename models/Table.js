module.exports = (sequelize , Datatypes) => {
    const table = sequelize.define("table" , {
        tableStatus : {
            type : Datatypes.INTEGER
        },
        tableName : {
            type : Datatypes.STRING
        },
        numberOfChairs : {
            type : Datatypes.INTEGER
        },
        enableStatus : {
            type : Datatypes.INTEGER
        },
        hotelId : {
            type : Datatypes.INTEGER,
            references : {
                model : "hotels",
                key : "id"
            }
        },
        restaurantId : {
            type : Datatypes.INTEGER,
            references : {
                model : 'restaurants',
                key : "id"
            }
        }
    });

    return table;
}