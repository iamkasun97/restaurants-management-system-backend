module.exports = (sequelize , Datatypes) => {
    const promotion = sequelize.define("promotion" , {
        promotionName : {
            type : Datatypes.STRING,
        },
        promotionDescription : {
            type : Datatypes.STRING,
        },
        unitPrice : {
            type : Datatypes.DOUBLE
        },
        unitCost : {
            type : Datatypes.DOUBLE
        },
        serviceCharge : {
            type : Datatypes.DOUBLE
        },
        serviceChargeType : {
            type : Datatypes.INTEGER
        },
        keywords : {
            type : Datatypes.STRING,
        },
        fromDate : {
            type : Datatypes.DATEONLY,
        },
        toDate : {
            type : Datatypes.DATEONLY,
        },
        serveTime : {
            type : Datatypes.STRING
        },
        availability : {
            type : Datatypes.INTEGER
        },
        avgPrepareTime : {
            type : Datatypes.INTEGER
        },
        image : {
            type : Datatypes.STRING
        },
        hotelId : {
            type : Datatypes.INTEGER,
            references : {
                model : "hotels",
                key : "id"
            }
        },
        restaurantId : {
            type : Datatypes.INTEGER,
            references : {
                model : 'restaurants',
                key : "id"
            }
        }
    });

    return promotion;
}