module.exports = (sequelize , Datatypes) => {
    const hotel = sequelize.define("hotel" , {
        hotelName : {
            type : Datatypes.STRING,
        },
        hotelAddress : {
            type : Datatypes.STRING,
        },
        hotelStatus : {
            type : Datatypes.INTEGER
        },
        statusComment : {
            type : Datatypes.STRING
        },
        ownerId : {
            type : Datatypes.INTEGER,
            references : {
                model : "owners",
                key : "id"
            }
        }
    });

    return hotel;
}