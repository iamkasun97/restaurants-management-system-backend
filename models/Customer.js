module.exports = (sequelize , Datatypes) => {
    const customer = sequelize.define("customer" , {
        customerName : {
            type : Datatypes.STRING
        },
        mobile : {
            type : Datatypes.STRING
        },
        email : {
            type : Datatypes.STRING
        },
        hotelId : {
            type : Datatypes.INTEGER,
            references : {
                model : "hotels",
                key : "id"
            }
        },
        restaurantId : {
            type : Datatypes.INTEGER,
            references : {
                model : 'restaurants',
                key : "id"
            }
        }
    });

    return customer;
}