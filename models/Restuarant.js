module.exports = (sequelize , Datatypes) => {
    const restaurant = sequelize.define("restaurant" , {
        restaurantName : {
            type : Datatypes.STRING,
        },
        restaurantDescription : {
            type : Datatypes.STRING
        },
        mobile : {
            type : Datatypes.STRING
        },
        restaurantStatus : {
            type : Datatypes.INTEGER
        },
        statusComment : {
            type : Datatypes.STRING
        },
        ownerId : {
            type : Datatypes.INTEGER,
            references : {
                model : "owners",
                key : "id"
            }
        },
        hotelId : {
            type : Datatypes.INTEGER,
            references : {
                model : "hotels",
                key : "id"
            }
        }
    });

    return restaurant;
}