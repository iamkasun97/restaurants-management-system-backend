const {role} = require('../models');

module.exports = (sequelize , Datatypes) => {
    const users = sequelize.define("users" , {
        firstName : {
            type : Datatypes.STRING,
        },
        lastName : {
            type : Datatypes.STRING,
        },
        userName : {
            type : Datatypes.STRING,
        },
        password : {
            type : Datatypes.STRING,
        },
        employeeNumber : {
            type : Datatypes.STRING,
        },
        mobile : {
            type : Datatypes.STRING,
        },
        roleId : {
            type : Datatypes.INTEGER,
            references : {
                model : "roles",
                key : "id"
            }
        },
        createdOwner : {
            type : Datatypes.INTEGER,
            references : {
                model : "owners",
                key : "id"
            }
        },
        hotelId : {
            type : Datatypes.INTEGER,
            references : {
                model : "hotels",
                key : "id"
            }
        },
        restaurantId : {
            type : Datatypes.INTEGER,
            references : {
                model : 'restaurants',
                key : "id"
            }
        },
        departmentId : {
            type : Datatypes.INTEGER,
            references : {
                model : 'departments',
                key : "id"
            }
        }
    });


    return users;
}