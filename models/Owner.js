module.exports = (sequelize , Datatypes) => {
    const owner = sequelize.define("owner" , {
        firstName : {
            type : Datatypes.STRING,
        },
        lastName : {
            type : Datatypes.STRING,
        },
        userName : {
            type : Datatypes.STRING,
        },
        email : {
            type : Datatypes.STRING,
            // add validation here
        },
        mobile : {
            type : Datatypes.STRING,
            // add validation here
        },
        password : {
            type : Datatypes.STRING
        },
        emailValidation : {
            type : Datatypes.INTEGER
        }
    });

    return owner;
}