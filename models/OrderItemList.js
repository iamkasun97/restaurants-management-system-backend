module.exports = (sequelize , Datatypes) => {
    const order_item = sequelize.define("order_item" , {
        itemId : {
            type : Datatypes.INTEGER,
            references : {
                model : "items",
                key : "id"
            }
        },
        itemName : {
            type : Datatypes.STRING
        },
        promotionId : {
            type : Datatypes.INTEGER,
            references : {
                model : "promotions",
                key : "id"
            }
        },
        promotionName : {
            type : Datatypes.STRING
        },
        itemList : {
            type : Datatypes.TEXT('long'),
        },
        unit : {
            type : Datatypes.STRING
        },
        unitAmount : {
            type : Datatypes.STRING
        },
        avgPrepareTime : {
            type : Datatypes.INTEGER
        },
        diningStatus : {
            type : Datatypes.INTEGER
        },
        unitCost : {
            type : Datatypes.DOUBLE
        },
        unitPrice : {
            type : Datatypes.DOUBLE
        },
        quantity : {
            type : Datatypes.INTEGER
        },
        processingStatus : {
            type : Datatypes.INTEGER
        },
        serviceCharge : {
            type : Datatypes.DOUBLE
        },
        serviceChargeType : {
            type : Datatypes.INTEGER
        },
        orderId : {
            type : Datatypes.INTEGER,
            references : {
                model : "orders",
                key : "id"
            }
        },
        hotelId : {
            type : Datatypes.INTEGER,
            references : {
                model : "hotels",
                key : "id"
            }
        },
        restaurantId : {
            type : Datatypes.INTEGER,
            references : {
                model : 'restaurants',
                key : "id"
            }
        },
        departmentId : {
            type : Datatypes.INTEGER,
            references : {
                model : 'departments',
                key : 'id'
            }
        }
    });

    return order_item;
}