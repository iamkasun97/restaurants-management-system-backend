module.exports = (sequelize , Datatypes) => {
    const order = sequelize.define("order" , {
        orderDateTime : {
            type : Datatypes.DATE,
        },
        paymentStatus : {
            type : Datatypes.INTEGER
        },
        customerId : {
            type : Datatypes.INTEGER,
            references: {
                model : 'customers',
                key : 'id'
            }
        },
        userId : {
            type : Datatypes.INTEGER,
            references : {
                model : 'users',
                key : 'id'
            }
        },
        userType : {
            type : Datatypes.STRING,
        },
        serviceCharge : {
            type : Datatypes.DOUBLE,
        },
        taxRate : {
            type : Datatypes.DOUBLE,
        },
        cancelStatus : {
            type : Datatypes.INTEGER,
        },
        tableId : {
            type : Datatypes.INTEGER,
            references : {
                model : 'tables',
                key : "id"
            }
        },
        hotelId : {
            type : Datatypes.INTEGER,
            references : {
                model : "hotels",
                key : "id"
            }
        },
        restaurantId : {
            type : Datatypes.INTEGER,
            references : {
                model : 'restaurants',
                key : "id"
            }
        }
    });

    return order;
}