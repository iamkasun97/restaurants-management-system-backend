module.exports = (sequelize , Datatypes) => {
    const tax = sequelize.define("tax" , {
        taxRate : {
            type : Datatypes.DOUBLE,
        },
        restaurantId : {
            type : Datatypes.INTEGER,
            references : {
                model : "restaurants",
                key : "id"
            }
        },
        ownerId : {
            type : Datatypes.INTEGER,
            references : {
                model : "owners",
                key : "id"
            }
        },
        hotelId : {
            type : Datatypes.INTEGER,
            references : {
                model : "hotels",
                key : "id"
            }
        }
    });

    return tax;
}