module.exports = (sequelize , Datatypes) => {
    const category_item = sequelize.define("category_item" , {
        categoryId : {
            type : Datatypes.INTEGER,
            references : {
                model : "categories",
                key : "id"
            }
        },
        itemId : {
            type : Datatypes.INTEGER,
            references : {
                model : 'items',
                key : "id"
            }
        }
    });

    return category_item;
}