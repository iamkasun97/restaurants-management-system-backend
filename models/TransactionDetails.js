module.exports = (sequelize , Datatypes) => {
    const transaction_detail = sequelize.define("transaction_detail" , {
        orderId : {
            type : Datatypes.INTEGER,
            references : {
                model : "orders",
                key : "id"
            }
        },
        totalCost : {
            type : Datatypes.DOUBLE
        },
        totalPrice : {
            type : Datatypes.DOUBLE
        },
        totalServiceCharge : {
            type : Datatypes.DOUBLE
        },
        profit : {
            type : Datatypes.DOUBLE
        },
        cardNumber : {
            type : Datatypes.STRING
        },
        paymentDetails : {
            type : Datatypes.STRING
        },
        balance : {
            type : Datatypes.DOUBLE
        },
        hotelId : {
            type : Datatypes.INTEGER,
            references : {
                model : "hotels",
                key : "id"
            }
        },
        restaurantId : {
            type : Datatypes.INTEGER,
            references : {
                model : "restaurants",
                key : "id"
            }
        }
    });

    return transaction_detail;
}