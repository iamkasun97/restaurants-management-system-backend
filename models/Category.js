module.exports = (sequelize , Datatypes) => {
    const category = sequelize.define("category" , {
        categoryName : {
            type : Datatypes.STRING,
        },
        categoryDescription : {
            type : Datatypes.STRING,
        },
        hotelId : {
            type : Datatypes.INTEGER,
            references : {
                model : "hotels",
                key : "id"
            }
        },
        restaurantId : {
            type : Datatypes.INTEGER,
            references : {
                model : 'restaurants',
                key : "id"
            }
        }
    });

    return category;
}