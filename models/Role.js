module.exports = (sequelize , Datatypes) => {
    const role = sequelize.define("role" , {
        roleName : {
            type : Datatypes.STRING,
            unique : true,
        },
        permissionLevel : {
            type : Datatypes.STRING,
            unique : true,
        }
    });

    return role;
}