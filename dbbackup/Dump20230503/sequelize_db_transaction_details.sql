-- MySQL dump 10.13  Distrib 8.0.31, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: sequelize_db
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `transaction_details`
--

DROP TABLE IF EXISTS `transaction_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction_details` (
  `id` int NOT NULL AUTO_INCREMENT,
  `orderId` int DEFAULT NULL,
  `totalCost` double DEFAULT NULL,
  `totalPrice` double DEFAULT NULL,
  `totalServiceCharge` double DEFAULT NULL,
  `profit` double DEFAULT NULL,
  `cardNumber` varchar(255) DEFAULT NULL,
  `paymentDetails` varchar(255) DEFAULT NULL,
  `balance` double DEFAULT NULL,
  `hotelId` int DEFAULT NULL,
  `restaurantId` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderId` (`orderId`),
  KEY `hotelId` (`hotelId`),
  KEY `restaurantId` (`restaurantId`),
  CONSTRAINT `transaction_details_ibfk_1` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`),
  CONSTRAINT `transaction_details_ibfk_2` FOREIGN KEY (`hotelId`) REFERENCES `hotels` (`id`),
  CONSTRAINT `transaction_details_ibfk_3` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_details`
--

LOCK TABLES `transaction_details` WRITE;
/*!40000 ALTER TABLE `transaction_details` DISABLE KEYS */;
INSERT INTO `transaction_details` VALUES (1,140,4550,5600,242.43,1050,'xxxx',NULL,104657.57,1,1,'2023-05-02 12:13:59','2023-05-02 12:13:59'),(2,NULL,5100,6038.43,242.43,938.4300000000003,'xxxx','{\"card\":{\"amount\":110000,\"cardNumber\":\"xxxx\"},\"cash\":{\"amount\":500}}',104461.57,1,1,'2023-05-03 10:11:16','2023-05-03 10:11:16'),(3,NULL,5100,6038.43,242.43,938.4300000000003,'xxxx','{\"card\":{\"amount\":110000,\"cardNumber\":\"xxxx\"},\"cash\":{\"amount\":500}}',104461.57,1,1,'2023-05-03 10:12:47','2023-05-03 10:12:47'),(4,NULL,5100,6038.43,242.43,938.4300000000003,'xxxx','{\"card\":{\"amount\":110000,\"cardNumber\":\"xxxx\"},\"cash\":{}}',0,1,1,'2023-05-03 10:14:42','2023-05-03 10:14:42'),(5,NULL,5100,6038.43,242.43,938.4300000000003,'xxxx','{\"card\":{\"amount\":110000,\"cardNumber\":\"xxxx\"},\"cash\":{\"amount\":500}}',104461.57,1,1,'2023-05-03 10:16:08','2023-05-03 10:16:08'),(6,NULL,5100,6038.43,242.43,938.4300000000003,'xxxx','{\"card\":{\"amount\":110000,\"cardNumber\":\"xxxx\"}}',0,1,1,'2023-05-03 10:16:16','2023-05-03 10:16:16'),(7,NULL,5100,6038.43,242.43,938.4300000000003,'xxxx','{\"card\":{\"amount\":110000,\"cardNumber\":\"xxxx\"},\"cash\":{\"amount\":500}}',104461.57,1,1,'2023-05-03 10:35:05','2023-05-03 10:35:05'),(8,NULL,5100,6038.43,242.43,938.4300000000003,'xxxx','{\"card\":{\"amount\":110000,\"cardNumber\":\"xxxx\"},\"cash\":{\"amount\":500}}',104461.57,1,1,'2023-05-03 10:35:53','2023-05-03 10:35:53'),(9,NULL,5100,6038.43,242.43,938.4300000000003,'xxxx','{\"card\":{\"amount\":110000,\"cardNumber\":\"xxxx\"},\"cash\":{\"amount\":500}}',104461.57,1,1,'2023-05-03 10:55:32','2023-05-03 10:55:32');
/*!40000 ALTER TABLE `transaction_details` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-05-03 11:32:18
