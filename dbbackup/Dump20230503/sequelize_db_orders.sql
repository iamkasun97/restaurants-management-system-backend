-- MySQL dump 10.13  Distrib 8.0.31, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: sequelize_db
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orders` (
  `id` int NOT NULL AUTO_INCREMENT,
  `orderDateTime` datetime DEFAULT NULL,
  `paymentStatus` int DEFAULT NULL,
  `customerId` int DEFAULT NULL,
  `userId` int DEFAULT NULL,
  `userType` varchar(255) DEFAULT NULL,
  `serviceCharge` double DEFAULT NULL,
  `totalTax` double DEFAULT NULL,
  `cancelStatus` int DEFAULT NULL,
  `tableId` int DEFAULT NULL,
  `hotelId` int DEFAULT NULL,
  `restaurantId` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `customerId` (`customerId`),
  KEY `userId` (`userId`),
  KEY `tableId` (`tableId`),
  KEY `hotelId` (`hotelId`),
  KEY `restaurantId` (`restaurantId`),
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`customerId`) REFERENCES `customers` (`id`),
  CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`tableId`) REFERENCES `tables` (`id`),
  CONSTRAINT `orders_ibfk_4` FOREIGN KEY (`hotelId`) REFERENCES `hotels` (`id`),
  CONSTRAINT `orders_ibfk_5` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (123,'2023-04-20 14:39:54',1,159,1,'owner',50,434.7266,0,1,1,1,'2023-04-20 14:39:54','2023-05-02 11:56:39'),(127,'2023-04-24 13:12:06',1,159,1,'owner',50,434.7266,0,1,1,1,'2023-04-24 13:12:06','2023-04-24 16:09:08'),(128,'2023-04-25 15:39:54',1,159,1,'owner',50,434.7266,0,1,1,1,'2023-04-25 15:39:54','2023-04-25 19:20:40'),(136,'2023-04-25 19:32:37',1,159,1,'owner',50,196,0,1,1,1,'2023-04-25 19:32:37','2023-04-26 11:47:51'),(138,'2023-04-26 11:48:35',1,159,1,'owner',50,196,0,1,1,1,'2023-04-26 11:48:35','2023-04-27 11:18:58'),(140,'2023-04-27 11:19:49',1,162,1,'owner',242.43,196,0,1,1,1,'2023-04-27 11:19:49','2023-05-02 12:13:59'),(141,'2023-05-02 16:13:23',0,162,1,'owner',242.43,196,0,NULL,1,1,'2023-05-02 16:13:23','2023-05-02 16:13:23'),(142,'2023-05-03 07:59:32',0,163,1,'owner',242.43,196,0,NULL,1,1,'2023-05-03 07:59:32','2023-05-03 07:59:32'),(152,'2023-05-03 10:11:16',0,163,1,'owner',242.43,196,0,1,1,1,'2023-05-03 10:11:16','2023-05-03 10:11:16'),(153,'2023-05-03 10:12:47',0,163,1,'owner',242.43,196,0,NULL,1,1,'2023-05-03 10:12:47','2023-05-03 10:12:47'),(155,'2023-05-03 10:14:42',0,163,1,'owner',242.43,196,0,NULL,1,1,'2023-05-03 10:14:42','2023-05-03 10:14:42'),(156,'2023-05-03 10:16:07',0,163,1,'owner',242.43,196,0,NULL,1,1,'2023-05-03 10:16:07','2023-05-03 10:16:07'),(157,'2023-05-03 10:16:16',0,163,1,'owner',242.43,196,0,NULL,1,1,'2023-05-03 10:16:16','2023-05-03 10:16:16'),(159,'2023-05-03 10:35:05',0,163,1,'owner',242.43,196,0,1,1,1,'2023-05-03 10:35:05','2023-05-03 10:35:05'),(160,'2023-05-03 10:35:52',1,163,1,'owner',242.43,196,0,1,1,1,'2023-05-03 10:35:52','2023-05-03 10:35:52'),(165,'2023-05-03 10:55:32',1,163,1,'owner',242.43,196,0,1,1,1,'2023-05-03 10:55:32','2023-05-03 10:55:32');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-05-03 11:32:18
