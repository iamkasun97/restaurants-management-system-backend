-- MySQL dump 10.13  Distrib 8.0.31, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: sequelize_db
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `items` (
  `id` int NOT NULL AUTO_INCREMENT,
  `itemName` varchar(255) DEFAULT NULL,
  `itemDescription` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `unit` varchar(255) DEFAULT NULL,
  `unitAmount` varchar(255) DEFAULT NULL,
  `unitCost` double DEFAULT NULL,
  `unitPrice` double DEFAULT NULL,
  `serviceCharge` double DEFAULT NULL,
  `serviceChargeType` int DEFAULT NULL,
  `avgPrepareTime` int DEFAULT NULL,
  `serveTime` varchar(255) DEFAULT NULL,
  `availability` int DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `hotelId` int DEFAULT NULL,
  `restaurantId` int DEFAULT NULL,
  `departmentId` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hotelId` (`hotelId`),
  KEY `restaurantId` (`restaurantId`),
  KEY `departmentId` (`departmentId`),
  CONSTRAINT `items_ibfk_1` FOREIGN KEY (`hotelId`) REFERENCES `hotels` (`id`),
  CONSTRAINT `items_ibfk_2` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`),
  CONSTRAINT `items_ibfk_3` FOREIGN KEY (`departmentId`) REFERENCES `departments` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (2,'wild apple','test4 description','[\"keyword1\" , \"keyword2\" , \"keyword3\"]','ml','100',100.98,120.76,10.56,0,16,'[{\"startTime\":\"07:55:00\",\"endTime\":\"12:00:00\"},{\"startTime\":\"12:00:00\",\"endTime\":\"22:00:00\"}]',1,'1681898872643.jpeg',1,1,1,'2023-04-06 07:15:30','2023-04-25 07:47:44'),(3,'Chicken Kottu','new description','[\"keyword1\" , \"keyword2\" , \"keyword3\"]','1','1',1250,1600,10.56,1,15,'[{\"startTime\":\"07:55:00\",\"endTime\":\"12:00:00\"},{\"startTime\":\"12:00:00\",\"endTime\":\"22:00:00\"}]',1,'1680843694426.jpeg',1,1,1,'2023-04-07 05:01:34','2023-04-07 05:01:34'),(4,'Chicken Biriyani','new description','[\"keyword1\" , \"keyword2\" , \"keyword3\"]','1','1',1350,1700,10.56,1,15,'[{\"startTime\":\"05:55:00\",\"endTime\":\"12:00:00\"},{\"startTime\":\"12:00:00\",\"endTime\":\"22:00:00\"}]',1,'1681173190660.jpeg',1,1,1,'2023-04-11 00:33:10','2023-04-11 00:33:10'),(5,'Egg Role','vishwa 1 new description','[\"keyword1\" , \"keyword2\" , \"keyword3\"]','1','1',150,250,10.56,1,15,'[{\"startTime\":\"05:55:00\",\"endTime\":\"12:00:00\"},{\"startTime\":\"12:00:00\",\"endTime\":\"22:00:00\"}]',1,'1681189472515.jpeg',1,1,1,'2023-04-11 05:04:32','2023-04-11 05:04:32'),(6,'Lion Beer Can','vishwa 1 new description','[\"keyword1\" , \"keyword2\" , \"keyword3\"]','1','1',550,650,10.56,1,15,'[{\"startTime\":\"05:55:00\",\"endTime\":\"12:00:00\"},{\"startTime\":\"12:00:00\",\"endTime\":\"22:00:00\"}]',1,'1681190444557.jpeg',1,1,1,'2023-04-11 05:20:44','2023-04-11 05:20:44'),(7,'Fried Fish','vishwa 1 new description','[\"keyword1\" , \"keyword2\" , \"keyword3\"]','1','1',650,700,10.56,1,15,'[{\"startTime\":\"05:55:00\",\"endTime\":\"12:00:00\"},{\"startTime\":\"12:00:00\",\"endTime\":\"22:00:00\"}]',1,'1681203866080.jpeg',1,1,1,'2023-04-11 09:04:26','2023-04-11 09:04:26'),(8,'Black Pork Curry','vishwa 1 new description','[\"keyword1\" , \"keyword2\" , \"keyword3\"]','1','1',550,850,10.56,1,15,'[{\"startTime\":\"05:55:00\",\"endTime\":\"12:00:00\"},{\"startTime\":\"12:00:00\",\"endTime\":\"22:00:00\"}]',1,'1681208029490.jpeg',1,1,1,'2023-04-11 10:13:49','2023-04-11 10:13:49'),(9,'chicken kottu','vishwa 1 new description','[\"keyword1\" , \"keyword2\" , \"keyword3\"]','ml','100',100.98,120.76,10.56,1,15,'[{\"startTime\":\"05:55:00\",\"endTime\":\"12:00:00\"},{\"startTime\":\"12:00:00\",\"endTime\":\"22:00:00\"}]',1,'1681899775865.jpeg',1,1,1,'2023-04-19 15:52:55','2023-04-19 15:52:55'),(10,'chicken kottu new','vishwa 1 new description','[\"keyword1\" , \"keyword2\" , \"keyword3\"]','ml','100',100.98,120.76,10.56,1,15,'[{\"startTime\":\"05:55:00\",\"endTime\":\"12:00:00\"},{\"startTime\":\"12:00:00\",\"endTime\":\"22:00:00\"}]',1,'1681901833677.jpeg',1,1,1,'2023-04-19 16:27:13','2023-04-19 16:27:13');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-05-03 11:32:18
