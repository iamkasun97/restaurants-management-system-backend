const express = require("express");
const router = express.Router();
const TableController = require('../controllers/table');

// middlewares
const configureMiddleware = require('../middleware/configure');

router.post("/dropdown-details-add-table" ,configureMiddleware.extractToken1, TableController.dropdownDetailsForAddTable);
router.post("/add-table" ,configureMiddleware.extractToken1, TableController.addNewTable);
router.post("/get-tables" ,configureMiddleware.extractToken1, TableController.getTables);
router.post("/get-available-tables" ,configureMiddleware.extractToken1, TableController.getAvailableTables);
router.post("/update-table-details" ,configureMiddleware.extractToken1, TableController.updateTableDetails);
router.post("/change-table-status" ,configureMiddleware.extractToken1, TableController.changeTableStatus);

module.exports = router;