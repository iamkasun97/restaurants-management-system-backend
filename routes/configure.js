const express = require("express");
const router = express.Router();
const ConfigureController = require('../controllers/configure');

// middlewares
const configureMiddleware = require('../middleware/configure');

router.post("/enter-new-hotel" ,configureMiddleware.extractToken, ConfigureController.enterNewHotel);
router.post("/update-hotel" ,configureMiddleware.extractToken, ConfigureController.updateHotel);
router.post('/get-hotels' , configureMiddleware.extractToken, ConfigureController.getHotels);
router.post('/change-status-hotel' , configureMiddleware.extractToken, ConfigureController.changeHotelStatus);
router.post('/enter-new-restaurant' , configureMiddleware.extractToken, ConfigureController.enterNewRestaurant);
router.post('/get-restaurants' , configureMiddleware.extractToken, ConfigureController.getRestaurants);
router.post('/update-restaurant' , configureMiddleware.extractToken, ConfigureController.updateRestaurant);
router.post('/change-status-restaurant' , configureMiddleware.extractToken, ConfigureController.changeRestaurantStatus);
router.post('/enter-new-department' , configureMiddleware.extractToken, ConfigureController.enterNewDepartment);
router.post('/get-departments' , configureMiddleware.extractToken, ConfigureController.getDepartments);
router.post('/update-department' , configureMiddleware.extractToken, ConfigureController.updateDepartment);
router.post('/change-status-department' , configureMiddleware.extractToken, ConfigureController.changeDepartmentStatus);
router.post('/enter-new-role' , configureMiddleware.extractToken, ConfigureController.enterNewRole);
router.post('/get-roles' , configureMiddleware.extractToken, ConfigureController.getRoles);
router.post('/update-role' , configureMiddleware.extractToken, ConfigureController.updateRole);
router.post('/dropdown-details-add-category' , configureMiddleware.extractToken, ConfigureController.dropDownDetailsForCategory);
router.post('/enter-new-category' , configureMiddleware.extractToken, ConfigureController.enterNewCategory);
router.post('/get-categories' , configureMiddleware.extractToken, ConfigureController.getCategories);
router.post('/update-category' , configureMiddleware.extractToken, ConfigureController.updateCategoryDetails);
router.post('/set-tax-rate' , configureMiddleware.extractToken, ConfigureController.setTaxRate);
router.post('/get-tax-rate' , configureMiddleware.extractToken , ConfigureController.getTaxRate);

router.post('/dropdown-details-add-users' , configureMiddleware.extractToken1, ConfigureController.dropDownDetails);
router.post('/enter-new-user' , configureMiddleware.extractToken1, ConfigureController.enterNewUser);
router.post('/get-users' , configureMiddleware.extractToken1, ConfigureController.getUserDetails);
router.post('/update-user' , configureMiddleware.extractToken1, ConfigureController.updateUserDetails);


module.exports = router;