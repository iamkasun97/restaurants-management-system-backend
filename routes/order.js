const express = require("express");
const router = express.Router();
const OrderController = require('../controllers/order');

// middlewares
const configureMiddleware = require('../middleware/configure');
// const upload = require('../middleware/imageUpload');
// const UploadMiddleware = require('../middleware/imageUpload2');

router.post("/dropdown-details-view-orders" ,configureMiddleware.extractToken2, OrderController.dropdownDetailsForViewOrders);
router.post("/get-category-promotions-items" ,configureMiddleware.extractToken2, OrderController.getItemsAndPromotionsUnderCategory);
router.post("/place-order" ,configureMiddleware.extractToken2, OrderController.placeAnOrderNew);
router.post("/view-orders" ,configureMiddleware.extractToken2, OrderController.viewOrders);
router.post("/view-orders-date-wise" ,configureMiddleware.extractToken2, OrderController.getOrderDateWise);
router.post("/view-order-items" ,configureMiddleware.extractToken2, OrderController.viewOrderItems);
router.post("/edit-order" ,configureMiddleware.extractToken2, OrderController.editAnOrder);
router.post("/cancel-order" ,configureMiddleware.extractToken2, OrderController.cancelAnOrder);

module.exports = router;