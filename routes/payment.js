const express = require("express");
const router = express.Router();
const PaymentController = require('../controllers/payment');

// middlewares
const configureMiddleware = require('../middleware/configure');

router.post("/add-payment-order" ,configureMiddleware.extractToken2, PaymentController.addPaymentOrder);
router.post("/add-direct-payment-order" ,configureMiddleware.extractToken2, PaymentController.addDirectPaymentOrder);

module.exports = router;