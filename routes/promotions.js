const express = require("express");
const router = express.Router();
const PromotionController = require('../controllers/promotions');

// middlewares
const configureMiddleware = require('../middleware/configure');
// const upload = require('../middleware/imageUpload');
const UploadMiddleware = require('../middleware/imageUpload2');

router.post("/dropdown-details-add-promotion" ,configureMiddleware.extractToken1, PromotionController.dropdownDetailsForAddPromotion);
router.post("/add-promotion" ,configureMiddleware.extractToken1,UploadMiddleware.imageUpload, PromotionController.addNewPromotion);
router.post("/get-restaurant-promotions" ,configureMiddleware.extractToken1, PromotionController.getRestaurantPromotions);
router.post("/get-category-promotions" ,configureMiddleware.extractToken1, PromotionController.getCategoryPromotions);
router.post("/update-restaurant-promotion" ,configureMiddleware.extractToken1,UploadMiddleware.imageUpload, PromotionController.updatePromotion);


module.exports = router;