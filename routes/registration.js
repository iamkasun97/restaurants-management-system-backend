const express = require("express");
const router = express.Router();
const RegistrationController = require('../controllers/registration');


router.post("/enter-new-owner" , RegistrationController.registerNewOwner);
module.exports = router;