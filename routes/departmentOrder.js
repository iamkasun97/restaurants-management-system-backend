const express = require("express");
const router = express.Router();
const DepartmentOrderController = require('../controllers/departmentOrder');

// middlewares
const configureMiddleware = require('../middleware/configure');

router.post("/dropdown-details-get-order-items" ,configureMiddleware.extractToken3, DepartmentOrderController.dropdownDetailsForGetOrderItems);
router.post("/get-pending-order-ids" ,configureMiddleware.extractToken3, DepartmentOrderController.getOrdersIds);
router.post("/get-order-items" ,configureMiddleware.extractToken3, DepartmentOrderController.getItemsForAnOrder);
router.post("/get-pending-orders" ,configureMiddleware.extractToken3, DepartmentOrderController.getPendingOrdersForDepartment);
router.post("/get-pending-orders-and-items" ,configureMiddleware.extractToken3, DepartmentOrderController.getPendingOrdersAndItemsUnderDepartment);
router.post("/change-order-item-status" ,configureMiddleware.extractToken3, DepartmentOrderController.changeStatusPendingOrder);

module.exports = router;