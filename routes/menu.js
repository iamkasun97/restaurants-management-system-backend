const express = require("express");
const router = express.Router();
const MenuController = require('../controllers/menu');

// middlewares
const configureMiddleware = require('../middleware/configure');
// const upload = require('../middleware/imageUpload');
const UploadMiddleware = require('../middleware/imageUpload2');

router.post("/dropdown-details-add-menuitem" ,configureMiddleware.extractToken1, MenuController.dropdownDetailsForAddItem);
router.post("/add-menuitem" , configureMiddleware.extractToken1,UploadMiddleware.imageUpload, MenuController.addNewItem);
router.post("/get-restaurant-items" ,configureMiddleware.extractToken1, MenuController.getRestaurantItems);
router.post("/get-restaurant-items-without-paging" ,configureMiddleware.extractToken1, MenuController.getRestaurantItemsWithoutPaging);
router.post("/get-category-items" ,configureMiddleware.extractToken1, MenuController.getCategoryItems);
router.post("/update-restaurant-item" ,configureMiddleware.extractToken1,UploadMiddleware.imageUpload, MenuController.updateRestaurantsItems);

module.exports = router;