const {sequelize} = require("../models");
const {owner} = require('../models');
const {users} = require('../models');
const {role} = require('../models');
const {hotel} = require('../models');
const {restaurant} = require('../models');
const {department} = require('../models');
const {item} = require('../models');
const {category_item} = require('../models');
const {category} = require('../models');


const config = require("../config");
const isEmpty = require("is-empty");
const bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const moment = require('moment');
require("dotenv").config();

const imagePath = Object.assign({} , config.imagePath);



exports.dropdownDetailsForAddItem = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // owners enter a item scenario
        if(req.body.role.includes('owner')){
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }

            const hotels = await hotel.findAll({
                where : {ownerId : req.body.userId , hotelStatus : 1}
            });
            const restaurants = await restaurant.findAll({
                where : {ownerId : req.body.userId , restaurantStatus : 1}
            });
            const departments = await department.findAll({
                where: {ownerId : req.body.userId , departmentStatus : 1}
            });
            const categories = await category.findAll({});

            // add departments array to the response
            let departmentArray = [];
            departments.forEach(department => {
                departmentArray.push(department.dataValues);
            });

            // do the mapping
            let hotelAndRestaurantArray = [];
            hotels.forEach(hotel => {
                let restaurantArray = [];
                restaurants.forEach(restaurant => {
                    let categoryArray = [];
                    categories.forEach(category => {
                        if(hotel.dataValues.id == category.dataValues.hotelId && restaurant.dataValues.id == category.dataValues.restaurantId){
                            categoryArray.push(category.dataValues);
                        }
                    });
                    if(restaurant.dataValues.hotelId == hotel.dataValues.id){
                        restaurantArray.push({
                            id : restaurant.dataValues.id,
                            restaurantName : restaurant.dataValues.restaurantName,
                            ownerId : restaurant.dataValues.ownerId,
                            hotelId : restaurant.dataValues.hotelId,
                            categories : categoryArray
                        });
                    }
                });
                let temp = {
                    id : hotel.dataValues.id,
                    hotelName : hotel.dataValues.hotelName,
                    hotelAddress : hotel.dataValues.hotelAddress,
                    statusComment : hotel.dataValues.statusComment,
                    ownerId : hotel.dataValues.ownerId,
                    restaurants : restaurantArray,
                }
                hotelAndRestaurantArray.push(temp);
            });
            let response = {
                hotels : hotelAndRestaurantArray,
                departments : departmentArray
            }
            // console.log(response);
            statusCode = 200;
            await tr.commit();
            customSuccess.data = response;
            return res.status(statusCode).json(customSuccess);

        }else if(req.body.role.includes('manager')){
            // manager employee scenario
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            // get the hotel , restaurant and department details for and employee
            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            const hotelE = await hotel.findOne({
                where : {id : searchResult.dataValues.hotelId , hotelStatus : 1}
            });
            const restaurantE = await restaurant.findOne({
                where : {id : searchResult.dataValues.restaurantId , restaurantStatus : 1}
            });
            const departmentE = await department.findOne({
                where: {id : searchResult.dataValues.departmentId , departmentStatus : 1}
            });
            const categoryE = await category.findAll({
                where : {hotelId : searchResult.dataValues.hotelId , restaurantId : searchResult.dataValues.restaurantId}
            });

            let restaurantArray = [];
            let departmentArray = [];
            let categoryArray = [];
            let hotelAndRestaurantArray = [];
            
            categoryE.forEach(e => {
                categoryArray.push(e.dataValues);
            });

            let restaurantAndCategories = {
                id : restaurantE.dataValues.id,
                restaurantName : restaurantE.dataValues.restaurantName,
                ownerId : restaurantE.dataValues.ownerId,
                hotelId : restaurantE.dataValues.hotelId,
                categories : categoryArray
            }

            departmentArray.push(departmentE.dataValues);
            restaurantArray.push(restaurantAndCategories);

            let temp = {
                id : hotelE.dataValues.id,
                hotelName : hotelE.dataValues.hotelName,
                hotelAddress : hotelE.dataValues.hotelAddress,
                statusComment : hotelE.dataValues.statusComment,
                ownerId : hotelE.dataValues.ownerId,
                restaurants : restaurantArray,
            }
            hotelAndRestaurantArray.push(temp);

            let response = {
                hotels : hotelAndRestaurantArray,
                departments : departmentArray
            }
            statusCode = 200;
            await tr.commit();
            customSuccess.data = response;
            return res.status(statusCode).json(customSuccess);
        }

        customError.comment = "error occurs!!";
        throw new Error();
    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// add item 
exports.addNewItem = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.departmentId)){
            statusCode = 400;
            customError.comment = "departmentId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.itemName)){
            statusCode = 400;
            customError.comment = "item name can't be empty!!";
            throw new Error();
        }
        // item description and keywords are optional
        if(isEmpty(req.body.unitCost)){
            statusCode = 400;
            customError.comment = "unit cost can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.unitPrice)){
            statusCode = 400;
            customError.comment = "unit price can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.serviceCharge)){
            statusCode = 400;
            customError.comment = "service charge can't be empty!!";
            throw new Error();
        }
        if(req.body.serviceChargeType === "" || req.body.serviceChargeType == undefined){
            statusCode = 400;
            customError.comment = "service charge type can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.unit)){
            statusCode = 400;
            customError.comment = "unit can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.unitAmount)){
            statusCode = 400;
            customError.comment = "unit amount can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.category)){
            statusCode = 400;
            customError.comment = "category can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.availability)){
            statusCode = 400;
            customError.comment = "availability can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.avgPrepareTime)){
            statusCode = 400;
            customError.comment = "average preparet time can't be empty!!";
            throw new Error();
        }

        if(req.body.role.includes('owner')){
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });
            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

            // have to check the department is available
            const departmentCheck = await department.findAll({
                where : {id : req.body.departmentId , ownerId : req.body.userId}
            });
            if(isEmpty(departmentCheck)){
                statusCode = 400;
                customError.comment = "departmentId is not valid!!";
                throw new Error(); 
            }
        }else if(req.body.role.includes('manager')){
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId || req.body.departmentId != searchResult.dataValues.departmentId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
            
            // have to check the department is available
            const departmentCheck = await department.findAll({
                where : {id : req.body.departmentId , ownerId : searchResult.dataValues.createdOwner}
            });
            if(isEmpty(departmentCheck)){
                statusCode = 400;
                customError.comment = "departmentId is not valid!!";
                throw new Error(); 
            }
        }

        // add item
        const newItem = await item.create({
            itemName : req.body.itemName,
            itemDescription : req.body.itemDescription,
            keywords : JSON.stringify(req.body.keywords),
            unit : req.body.unit,
            unitAmount : req.body.unitAmount,
            unitCost : req.body.unitCost,
            unitPrice : req.body.unitPrice,
            serviceCharge : req.body.serviceCharge,
            serviceChargeType : req.body.serviceChargeType,
            avgPrepareTime : req.body.avgPrepareTime,
            serveTime : JSON.stringify(req.body.serveTime),
            hotelId : req.body.hotelId,
            restaurantId : req.body.restaurantId,
            departmentId : req.body.departmentId,
            availability : req.body.availability,
            image : req.body.imageName
        } , {transaction : tr});

        if(isEmpty(newItem)){
            statusCode = 500;
            customError.comment = "new item not created!!";
            throw new Error();
        }else {

            // add validation for category items
            let searchCategoryList = await category.findAll({
                where : {id : req.body.category , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
            });

            if(searchCategoryList.length == req.body.category.length){}
            else {
                statusCode = 400;
                customError.comment = "bad request!!";
                throw new Error();
            }

            // create a entry in category items table
            let categoryItems = [];
            for(let i=0;i<req.body?.category.length;i++){
                categoryItems.push({
                    categoryId : req.body?.category[i],
                    itemId : newItem.dataValues.id
                });
            }
            // bulk entry
            const newCategoryItems = await category_item.bulkCreate(categoryItems , {transaction : tr});
            if(isEmpty(newCategoryItems)){
                statusCode = 500;
                customError.comment = "new category_item not created!!";
                throw new Error();
            }else {
                statusCode = 200;
                await tr.commit();
                customSuccess.data = newItem;
                return res.status(statusCode).json(customSuccess);
            }
        }
    }catch(error){
        console.log(error);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// get items restaurant wise
exports.getRestaurantItems = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        if(req.body.role.includes('owner')){
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

        }else if(req.body.role.includes('manager')){
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
        }

        // add the pagination concept here
        const pageAsNumber = Number.parseInt(req.query.page);
        const sizeAsNumber = Number.parseInt(req.query.size);

        let page = 0;
        if(!Number.isNaN(pageAsNumber) && pageAsNumber > 0){
            page = pageAsNumber;
        }

        let size = 5;
        if(!Number.isNaN(sizeAsNumber) && sizeAsNumber > 0){
            size = sizeAsNumber;
        }

        // get the search result
        const restaurantItems = await item.findAndCountAll({
            where : {hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , availability : 1},
            limit : size,
            offset : page * size
        });

        if(isEmpty(restaurantItems)){
            statusCode = 400;
            customError.comment = "no items found!!";
            throw new Error();
        }
        // get categoryid and tag items id
        let categoryDetails = await sequelize.query(
            `SELECT c.categoryId , c.itemId FROM category_items c
            JOIN items i
            ON i.id = c.itemId
            WHERE i.hotelId = ${req.body.hotelId} AND i.restaurantId = ${req.body.restaurantId}
            `
        );


        let response = [];
        restaurantItems.rows.forEach(item => {
            let itemCategories = categoryDetails[0].filter(e => e.itemId == item.dataValues.id).map(e => e.categoryId);
            let temp = {
                id : item.dataValues.id,
                itemName : item.dataValues.itemName,
                itemDescription : item.dataValues.itemDescription,
                keywords : JSON.parse(item.dataValues.keywords),
                unit : item.dataValues.unit,
                unitAmount : item.dataValues.unitAmount,
                unitCost : item.dataValues.unitCost,
                unitPrice : item.dataValues.unitPrice,
                serviceCharge : item.dataValues.serviceCharge,
                serviceChargeType : item.dataValues.serviceChargeType,
                avgPrepareTime : item.dataValues.avgPrepareTime,
                serveTime : JSON.parse(item.dataValues.serveTime),
                availability : item.dataValues.availability,
                image : `${imagePath.path}${item.dataValues.image}`,
                categories : itemCategories,
                hotelId : item.dataValues.hotelId,
                restaurantId : item.dataValues.restaurantId,
                departmentId : item.dataValues.departmentId
            }
            response.push(temp);
        });

        let result = {
            count : restaurantItems.count,
            rows : response
        }

        statusCode = 200;
        await tr.commit();
        customSuccess.data = result;
        return res.status(statusCode).json(customSuccess);

    }catch(error){
        console.log(error);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// get restaurant items without paging
exports.getRestaurantItemsWithoutPaging = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        if(req.body.role.includes('owner')){
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

        }else if(req.body.role.includes('manager')){
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
        }

        // get the search result
        const restaurantItems = await item.findAndCountAll({
            where : {hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , availability : 1},
        });

        if(isEmpty(restaurantItems)){
            statusCode = 400;
            customError.comment = "no items found!!";
            throw new Error();
        }
        // get categoryid and tag items id
        let categoryDetails = await sequelize.query(
            `SELECT c.categoryId , c.itemId FROM category_items c
            JOIN items i
            ON i.id = c.itemId
            WHERE i.hotelId = ${req.body.hotelId} AND i.restaurantId = ${req.body.restaurantId}
            `
        );


        let response = [];
        restaurantItems.rows.forEach(item => {
            let itemCategories = categoryDetails[0].filter(e => e.itemId == item.dataValues.id).map(e => e.categoryId);
            let keywordsArray = item.dataValues.keywords.split(" ");
            let temp = {
                id : item.dataValues.id,
                itemName : item.dataValues.itemName,
                itemDescription : item.dataValues.itemDescription,
                keywords : JSON.parse(item.dataValues.keywords),
                unit : item.dataValues.unit,
                unitAmount : item.dataValues.unitAmount,
                unitCost : item.dataValues.unitCost,
                unitPrice : item.dataValues.unitPrice,
                serviceCharge : item.dataValues.serviceCharge,
                serviceChargeType : item.dataValues.serviceChargeType,
                avgPrepareTime : item.dataValues.avgPrepareTime,
                serveTime : JSON.parse(item.dataValues.serveTime),
                availability : item.dataValues.availability,
                image : `${imagePath.path}${item.dataValues.image}`,
                categories : itemCategories,
                hotelId : item.dataValues.hotelId,
                restaurantId : item.dataValues.restaurantId,
                departmentId : item.dataValues.departmentId
            }
            response.push(temp);
        });

        let result = {
            count : restaurantItems.count,
            rows : response
        }

        statusCode = 200;
        await tr.commit();
        customSuccess.data = result;
        return res.status(statusCode).json(customSuccess);

    }catch(error){
        console.log(error);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// get items under the category
exports.getCategoryItems = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }
        if(isEmpty(req.body.categoryId)){
            statusCode = 400;
            customError.comment = "categoryId is missing!!";
            throw new Error();
        }

        if(req.body.role.includes('owner')){
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });
            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

        }else if(req.body.role.includes('manager')){
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
        }


        // check the category id in category_item table
        const [result , metadata] = await sequelize.query(
            `SELECT * FROM category_items ci
            JOIN items i
            ON i.id = ci.itemId
            WHERE ci.categoryId = ${req.body.categoryId} AND i.hotelId = ${req.body.hotelId} AND i.restaurantId = ${req.body.restaurantId} AND i.availability = 1
            `
        );

        let response = [];
        let time = new Date();
        let indiaTime = Number(time.getTime())+19800000;
        result.forEach(e => {
            JSON.parse(e.serveTime).forEach(t => {
                let start = new Date(`${moment().format('YYYY-MM-DD').toString()}T${t.startTime}.0Z`).getTime();
                let end = new Date(`${moment().format('YYYY-MM-DD').toString()}T${t.endTime}.0Z`).getTime();
                if(Number(start)<Number(indiaTime) && Number(end)>Number(indiaTime)){
                    // check response already hold the item or not
                    if(response.find(it => it.id == e.id)){}else {
                        let keywordsArray = e.keywords.split(" ");
                        let temp = {
                            id : e.id,
                            itemName : e.itemName,
                            itemDescription : e.itemDescription,
                            keywords : keywordsArray,
                            unit : e.unit,
                            unitAmount : e.unitAmount,
                            unitCost : e.unitCost,
                            unitPrice : e.unitPrice,
                            serviceCharge : e.serviceCharge,
                            serviceChargeType : e.serviceChargeType,
                            avgPrepareTime : e.avgPrepareTime,
                            serveTime : JSON.parse(e.serveTime),
                            availability : e.availability,
                            image : `${imagePath.path}${e.image}`,
                            hotelId : e.hotelId,
                            restaurantId : e.restaurantId,
                            departmentId : e.departmentId
                        }
                        response.push(temp);
                    }
                }
            });
        });

        // response object can't hold duplicate items
        if(isEmpty(response)){
            statusCode = 200;
            await tr.commit();
            customSuccess.comment = 'no items found!!';
            customSuccess.data = [];
            return res.status(statusCode).json(customSuccess);
        }

        statusCode = 200;
        await tr.commit();
        customSuccess.data = response;
        return res.status(statusCode).json(customSuccess);

    }catch(error){
        console.log(error);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// update the menu items
exports.updateRestaurantsItems = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        if(isEmpty(req.body.itemId)){
            statusCode = 400;
            customError.comment = "itemId is missing!!";
            throw new Error();
        }

        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.departmentId)){
            statusCode = 400;
            customError.comment = "departmentId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.itemName)){
            statusCode = 400;
            customError.comment = "item name can't be empty!!";
            throw new Error();
        }
        // item description and keywords are optional
        if(isEmpty(req.body.unitCost)){
            statusCode = 400;
            customError.comment = "unit cost can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.unitPrice)){
            statusCode = 400;
            customError.comment = "unit price can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.serviceCharge)){
            statusCode = 400;
            customError.comment = "service charge can't be empty!!";
            throw new Error();
        }
        if(req.body.serviceChargeType === "" || req.body.serviceChargeType == undefined){
            statusCode = 400;
            customError.comment = "service charge type can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.unit)){
            statusCode = 400;
            customError.comment = "unit can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.unitAmount)){
            statusCode = 400;
            customError.comment = "unit amount can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.category)){
            statusCode = 400;
            customError.comment = "category can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.availability)){
            statusCode = 400;
            customError.comment = "availability can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.avgPrepareTime)){
            statusCode = 400;
            customError.comment = "average preparet time can't be empty!!";
            throw new Error();
        }


        if(req.body.role.includes('owner')){
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });
            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

        }else if(req.body.role.includes('manager')){
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
        }

        // start the updating process
        const updatedResult = await item.update({
            itemName : req.body.itemName,
            itemDescription : req.body.itemDescription,
            keywords : JSON.stringify(req.body.keywords),
            unit : req.body.unit,
            unitAmount : req.body.unitAmount,
            unitCost : req.body.unitCost,
            unitPrice : req.body.unitPrice,
            serviceCharge : req.body.serviceCharge,
            serviceChargeType : req.body.serviceChargeType,
            avgPrepareTime : req.body.avgPrepareTime,
            serveTime : JSON.stringify(req.body.serveTime),
            availability : req.body.availability,
            image : req.body.imageName
        },{where : {id : req.body.itemId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , departmentId : req.body.departmentId} , transaction : tr})
    
        if(updatedResult != 1 || updatedResult < 0){
            statusCode = 500;
            customError.comment = "System error!!";
            throw new Error();
        }else {
            // remove the category items table
            const deleteResult = await category_item.destroy({
                where : {itemId : req.body.itemId}
            },{transaction : tr});

            if(deleteResult < 0){
                statusCode = 400;
                customError.comment = "update is not completed!!";
                throw new Error();
            }

            let categoryItemEntry = [];
            for(let i=0;i<req.body.category.length;i++){
                let temp = {
                    itemId : req.body.itemId,
                    categoryId : req.body.category[i]
                }
                categoryItemEntry.push(temp);
            }

            // bulk entry to category item table
            const newCategoryItems = await category_item.bulkCreate(categoryItemEntry , {transaction: tr});
            if(isEmpty(newCategoryItems)){
                statusCode = 400;
                customError.comment = "update is not completed!!";
                throw new Error();
            }

            // successfully updated
            statusCode = 200;
            await tr.commit();
            return res.status(statusCode).json(customSuccess);
        }
    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}