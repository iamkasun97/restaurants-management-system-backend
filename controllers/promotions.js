const {sequelize} = require("../models");
const {owner} = require('../models');
const {users} = require('../models');
const {role} = require('../models');
const {hotel} = require('../models');
const {restaurant} = require('../models');
const {department} = require('../models');
const {item} = require('../models');
const {category_item} = require('../models');
const {promotion} = require('../models');
const {promotion_item} = require('../models');
const {category} = require('../models');


const config = require("../config");
const isEmpty = require("is-empty");
const bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const moment = require('moment');
require("dotenv").config();

const imagePath = Object.assign({} , config.imagePath);

exports.dropdownDetailsForAddPromotion = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // owners enter a item scenario
        if(req.body.role.includes('owner')){
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }

            const hotels = await hotel.findAll({
                where : {ownerId : req.body.userId , hotelStatus : 1}
            });
            const restaurants = await restaurant.findAll({
                where : {ownerId : req.body.userId , restaurantStatus : 1}
            });
            const departments = await department.findAll({
                where: {ownerId : req.body.userId , departmentStatus : 1}
            });
            const categories = await category.findAll({});

            // add departments array to the response
            let departmentArray = [];
            departments.forEach(department => {
                departmentArray.push(department.dataValues);
            });

            // do the mapping
            let hotelAndRestaurantArray = [];
            hotels.forEach(hotel => {
                console.log(hotel);
                let restaurantArray = [];
                restaurants.forEach(restaurant => {
                    let categoryArray = [];
                    categories.forEach(category => {
                        if(hotel.dataValues.id == category.dataValues.hotelId && restaurant.dataValues.id == category.dataValues.restaurantId){
                            categoryArray.push(category.dataValues);
                        }
                    });
                    if(restaurant.dataValues.hotelId == hotel.dataValues.id){
                        restaurantArray.push({
                            id : restaurant.dataValues.id,
                            restaurantName : restaurant.dataValues.restaurantName,
                            ownerId : restaurant.dataValues.ownerId,
                            hotelId : restaurant.dataValues.hotelId,
                            categories : categoryArray
                        });
                    }
                });
                let temp = {
                    id : hotel.dataValues.id,
                    hotelName : hotel.dataValues.hotelName,
                    hotelAddress : hotel.dataValues.hotelAddress,
                    statusComment : hotel.dataValues.statusComment,
                    ownerId : hotel.dataValues.ownerId,
                    restaurants : restaurantArray,
                }
                hotelAndRestaurantArray.push(temp);
            });
            let response = {
                hotels : hotelAndRestaurantArray,
                departments : departmentArray
            }
            // console.log(response);
            statusCode = 200;
            await tr.commit();
            customSuccess.data = response;
            return res.status(statusCode).json(customSuccess);

        }else if(req.body.role.includes('manager')){
            // manager employee scenario
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            // get the hotel , restaurant and department details for and employee
            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            const hotelE = await hotel.findOne({
                where : {id : searchResult.dataValues.hotelId , hotelStatus : 1}
            });
            const restaurantE = await restaurant.findOne({
                where : {id : searchResult.dataValues.restaurantId , restaurantStatus : 1}
            });
            const departmentE = await department.findOne({
                where: {id : searchResult.dataValues.departmentId , departmentStatus : 1}
            });
            const categoryE = await category.findAll({
                where : {hotelId : searchResult.dataValues.hotelId , restaurantId : searchResult.dataValues.restaurantId}
            });

            let restaurantArray = [];
            let departmentArray = [];
            let categoryArray = [];
            let hotelAndRestaurantArray = [];

            categoryE.forEach(e => {
                categoryArray.push(e.dataValues);
            });

            let restaurantAndCategories = {
                id : restaurantE.dataValues.id,
                restaurantName : restaurantE.dataValues.restaurantName,
                ownerId : restaurantE.dataValues.ownerId,
                hotelId : restaurantE.dataValues.hotelId,
                categories : categoryArray
            }

            restaurantArray.push(restaurantAndCategories);
            departmentArray.push(departmentE.dataValues);

            let temp = {
                id : hotelE.dataValues.id,
                hotelName : hotelE.dataValues.hotelName,
                hotelAddress : hotelE.dataValues.hotelAddress,
                statusComment : hotelE.dataValues.statusComment,
                ownerId : hotelE.dataValues.ownerId,
                restaurants : restaurantArray,
            }

            hotelAndRestaurantArray.push(temp);

            let response = {
                hotels : hotelAndRestaurantArray,
                departments : departmentArray
            }
            statusCode = 200;
            await tr.commit();
            customSuccess.data = response;
            return res.status(statusCode).json(customSuccess);
        }

        customError.comment = "error occurs!!";
        throw new Error();
    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

exports.addNewPromotion = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.promotionName)){
            statusCode = 400;
            customError.comment = "promotion name can't be empty!!";
            throw new Error();
        }
        // promo description and keywords are optional

        if(isEmpty(req.body.unitPrice)){
            statusCode = 400;
            customError.comment = "unit price can't be empty!!";
            throw new Error();
        }
        // unit cost have calculate

        if(isEmpty(req.body.fromDate)){
            statusCode = 400;
            customError.comment = "promotion start date can't be empty!!";
            throw new Error();
        }

        if(isEmpty(req.body.toDate)){
            statusCode = 400;
            customError.comment = "promotion end date can't be empty!!";
            throw new Error();
        }
        
        if(isEmpty(req.body.serveTime)){
            statusCode = 400;
            customError.comment = "serve time can't be empty!!";
            throw new Error();
        }
        
        if(isEmpty(req.body.availability)){
            statusCode = 400;
            customError.comment = "availability can't be empty!!";
            throw new Error();
        }

        if(isEmpty(req.body.items)){
            statusCode = 400;
            customError.comment = "item list can't be empty!!";
            throw new Error();
        }

        if(isEmpty(req.body.serviceCharge)){
            statusCode = 400;
            customError.comment = "service charge can't be empty!!";
            throw new Error();
        }

        if(req.body.serviceChargeType === "" || req.body.serviceChargeType == undefined){
            statusCode = 400;
            customError.comment = "service charge type can't be empty!!";
            throw new Error();
        }

        if(req.body.role.includes('owner')){
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });
            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

        }else if(req.body.role.includes('manager')){
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
        }
        // calculate the unit cost by items
        // sort the list
        // breakdown item list into two arrays

        const sortedItemList = req.body.items.sort((a , b) => a.itemId - b.itemId);
        

        let itemsIdArray = sortedItemList.map(item => {
            return item.itemId;
        });
        

        let quantityArray = sortedItemList.map(item => {
            return item.quantity;
        });

        const searchItemResult = await item.findAll({
            where : {id : itemsIdArray , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , availability : 1}
        });

        // check arrays are equal length
        if(quantityArray.length != searchItemResult.length){
            statusCode = 400;
            customError.comment = "bad request!!";
            throw new Error();
        }

        let unitCost = 0;
        let avgPrepareTime = 0;
        for(let i=0;i<searchItemResult.length;i++){
            unitCost += Number(searchItemResult[i].dataValues.unitCost)*Number(quantityArray[i]);
            avgPrepareTime += Number(searchItemResult[i].dataValues.avgPrepareTime)*Number(quantityArray[i]);
        } 

        // add promotion process
        const newPromotion = await promotion.create({
            promotionName : req.body.promotionName,
            promotionDescription : req.body.promotionDescription,
            unitPrice : req.body.unitPrice,
            unitCost :  unitCost,
            keywords : JSON.stringify(req.body.keywords),
            fromDate : req.body.fromDate,
            toDate : req.body.toDate,
            serveTime : JSON.stringify(req.body.serveTime),
            availability : req.body.availability,
            hotelId : req.body.hotelId,
            restaurantId : req.body.restaurantId,
            avgPrepareTime : avgPrepareTime,
            serviceCharge : req.body.serviceCharge,
            serviceChargeType : req.body.serviceChargeType,
            image : req.body.imageName
        },{transaction : tr});

        if(isEmpty(newPromotion)){
            statusCode = 400;
            customError.comment = "promotion not created!!";
            throw new Error();
        }else {
            // create the entry for the promotion_item table
            let promotionItemEntry = [];
            for(let i=0;i<searchItemResult.length;i++){
                let temp = {
                    promotionId : newPromotion.dataValues.id,
                    itemId : itemsIdArray[i],
                    quantity : quantityArray[i]
                }
                promotionItemEntry.push(temp);
            }

            // enter bulk for promotion_items table
            const newPromotionItems = await promotion_item.bulkCreate(promotionItemEntry , {transaction : tr});
            if(isEmpty(newPromotionItems)){
                statusCode = 400;
                customError.comment = "promotion not created!!";
                throw new Error();
            }

            // successfully promotion created
            statusCode = 200;
            await tr.commit();
            customSuccess.data = newPromotion;
            return res.status(statusCode).json(customSuccess);
        }


    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// get promotions
exports.getRestaurantPromotions = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        if(req.body.role.includes('owner')){
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });
            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

        }else if(req.body.role.includes('manager')){
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
        }

        // get all promotions
        // add pagination
        // add the pagination concept here
        const pageAsNumber = Number.parseInt(req.query.page);
        const sizeAsNumber = Number.parseInt(req.query.size);

        let page = 0;
        if(!Number.isNaN(pageAsNumber) && pageAsNumber > 0){
            page = pageAsNumber;
        }

        let size = 5;
        if(!Number.isNaN(sizeAsNumber) && sizeAsNumber > 0){
            size = sizeAsNumber;
        }

        const restaurantPromotions = await promotion.findAndCountAll({
            where : {hotelId : req.body.hotelId , restaurantId : req.body.restaurantId},
            limit : size,
            offset : page * size
        });

        if(isEmpty(restaurantPromotions)){
            statusCode = 200;
            await tr.commit();
            customSuccess.comment = 'no promotions found!!';
            customSuccess.data = [];
            return res.status(statusCode).json(customSuccess);
        }

        // get items for promotion
        // get category ids for promotion
        let itemDetails = await sequelize.query(`
            SELECT i.id AS itemId , p.quantity , p.promotionId FROM items i
            JOIN promotion_items p
            ON p.itemId = i.id
            WHERE i.hotelId = ${req.body.hotelId} AND i.restaurantId = ${req.body.restaurantId}
        `);

        let response = [];
        restaurantPromotions.rows.forEach(promotion => {
            let items = itemDetails[0].filter(e => e.promotionId == promotion.dataValues.id);
            let temp = {
                id : promotion.dataValues.id,
                promotionName : promotion.dataValues.promotionName,
                promotionDescription : promotion.dataValues.promotionDescription,
                keywords : JSON.parse(promotion.dataValues.keywords),
                unitCost : promotion.dataValues.unitCost,
                unitPrice : promotion.dataValues.unitPrice,
                serviceCharge : promotion.dataValues.serviceCharge,
                serviceChargeType : promotion.dataValues.serviceChargeType,
                avgPrepareTime : promotion.dataValues.avgPrepareTime,
                serveTime : JSON.parse(promotion.dataValues.serveTime),
                availability : promotion.dataValues.availability,
                image : `${imagePath.path}${promotion.dataValues.image}`,
                items : items,
                hotelId : promotion.dataValues.hotelId,
                restaurantId : promotion.dataValues.restaurantId,
                departmentId : promotion.dataValues.departmentId,
                fromDate : promotion.dataValues.fromDate,
                toDate : promotion.dataValues.toDate
            }
            response.push(temp);
        });

        let result = {
            count : restaurantPromotions.count,
            rows : response
        }

        statusCode = 200;
        await tr.commit();
        customSuccess.data = result;
        return res.status(statusCode).json(customSuccess);

    }catch(error){
        console.log(error);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// get promotions under a category
exports.getCategoryPromotions = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }
        if(isEmpty(req.body.categoryId)){
            statusCode = 400;
            customError.comment = "categoryId is missing!!";
            throw new Error();
        }

        if(req.body.role.includes('owner')){
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });
            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

        }else if(req.body.role.includes('manager')){
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
        }

        // check the category id in category_item table
        const [result , metadata] = await sequelize.query(
            `SELECT DISTINCT i.id FROM category_items ci
            JOIN items i
            ON i.id = ci.itemId
            WHERE ci.categoryId = ${req.body.categoryId} AND i.hotelId = ${req.body.hotelId} AND i.restaurantId = ${req.body.restaurantId} AND i.availability = 1
            `
        );

        // get the item id array
        let itemIdArray = result.map(e => {
            return e.id
        });
        

        let promotionResult = await promotion_item.findAll({
            where : {itemId : itemIdArray}
        });

        let promotionIdArray = promotionResult.map(e => {
            return e.dataValues.promotionId
        });
        

        // distict the array
        let distictPromotionIdArray = [...new Set(promotionIdArray)] ;
        
        // get all promotions details
        const categoryPromotions = await promotion.findAll({
            where : {id : distictPromotionIdArray , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
        });

        if(isEmpty(categoryPromotions)){
            statusCode = 200;
            await tr.commit();
            customSuccess.comment = 'no promotions found!!';
            customSuccess.data = [];
            return res.status(statusCode).json(customSuccess);
        }

        // have to do the date and time validation
        let response = [];
        let time = new Date();
        let indiaTime = Number(time.getTime())+19800000;
        categoryPromotions.forEach(e => {
            let startDate = new Date(`${moment(e.dataValues.fromDate).format('YYYY-MM-DD').toString()}T00:00:00.0Z`).getTime();
            let finishDate = new Date(`${moment(e.dataValues.toDate).format('YYYY-MM-DD').toString()}T00:00:00.0Z`).getTime();
            
            if(Number(startDate) < Number(indiaTime) && Number(indiaTime) < Number(finishDate)){
                console.log("in");
                JSON.parse(e.dataValues.serveTime).forEach(t => {
                    let start = new Date(`${moment().format('YYYY-MM-DD').toString()}T${t.startTime}.0Z`).getTime();
                    let end = new Date(`${moment().format('YYYY-MM-DD').toString()}T${t.endTime}.0Z`).getTime();
                        if(Number(start)<Number(indiaTime) && Number(end)>Number(indiaTime)){
                            // check response already hold the item or not
                            if(response.find(it => it.id == e.dataValues.id)){}else {
                                let keywordsArray = e.dataValues.keywords.split(" ");
                                let temp = {
                                    id : e.dataValues.id,
                                    promotionName : e.dataValues.promotionName,
                                    promotionDescription : e.dataValues.promotionDescription,
                                    keywords : keywordsArray,
                                    unitCost : e.dataValues.unitCost,
                                    unitPrice : e.dataValues.unitPrice,
                                    serviceCharge : e.dataValues.serviceCharge,
                                    serviceChargeType : e.dataValues.serviceChargeType,
                                    avgPrepareTime : e.dataValues.avgPrepareTime,
                                    serveTime : JSON.parse(e.dataValues.serveTime),
                                    availability : e.dataValues.availability,
                                    image : `${imagePath.path}${e.dataValues.image}`,
                                    hotelId : e.dataValues.hotelId,
                                    restaurantId : e.dataValues.restaurantId,
                                    fromDate : e.dataValues.fromDate,
                                    toDate : e.dataValues.toDate
                                }
                                response.push(temp);
                            }
                        }     
                });
            }
        })

        statusCode = 200;
        await tr.commit();
        customSuccess.data = response;
        return res.status(statusCode).json(customSuccess);

    }catch(error){
        console.log(error);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    } 
}

// update promotion under a restaurant
exports.updatePromotion = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.promotionName)){
            statusCode = 400;
            customError.comment = "promotion name can't be empty!!";
            throw new Error();
        }
        // promo description and keywords are optional

        if(isEmpty(req.body.unitPrice)){
            statusCode = 400;
            customError.comment = "unit price can't be empty!!";
            throw new Error();
        }
        // unit cost have calculate

        if(isEmpty(req.body.fromDate)){
            statusCode = 400;
            customError.comment = "promotion start date can't be empty!!";
            throw new Error();
        }

        if(isEmpty(req.body.toDate)){
            statusCode = 400;
            customError.comment = "promotion end date can't be empty!!";
            throw new Error();
        }
        
        if(isEmpty(req.body.serveTime)){
            statusCode = 400;
            customError.comment = "serve time can't be empty!!";
            throw new Error();
        }
        
        if(isEmpty(req.body.availability)){
            statusCode = 400;
            customError.comment = "availability can't be empty!!";
            throw new Error();
        }

        if(isEmpty(req.body.items)){
            statusCode = 400;
            customError.comment = "item list can't be empty!!";
            throw new Error();
        }

        if(isEmpty(req.body.serviceCharge)){
            statusCode = 400;
            customError.comment = "service charge can't be empty!!";
            throw new Error();
        }

        if(req.body.serviceChargeType === "" || req.body.serviceChargeType == undefined){
            statusCode = 400;
            customError.comment = "service charge type can't be empty!!";
            throw new Error();
        }

        if(isEmpty(req.body.promotionId)){
            statusCode = 400;
            customError.comment = "promotion id is missing!!";
            throw new Error();
        }

        if(req.body.role.includes('owner')){
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });
            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

        }else if(req.body.role.includes('manager')){
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });

            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
        }

        // calculate the unit cost by items
        // sort the list
        // breakdown item list into two arrays

        const sortedItemList = req.body.items.sort((a , b) => a.itemId - b.itemId);
        

        let itemsIdArray = sortedItemList.map(item => {
            return item.itemId;
        });
        

        let quantityArray = sortedItemList.map(item => {
            return item.quantity;
        });

        const searchItemResult = await item.findAll({
            where : {id : itemsIdArray , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , availability : 1}
        });

        // check arrays are equal length
        if(quantityArray.length != searchItemResult.length){
            statusCode = 400;
            customError.comment = "bad request!!";
            throw new Error();
        }

        let unitCost = 0;
        let avgPrepareTime = 0;
        for(let i=0;i<searchItemResult.length;i++){
            unitCost += Number(searchItemResult[i].dataValues.unitCost)*Number(quantityArray[i]);
            avgPrepareTime += Number(searchItemResult[i].dataValues.avgPrepareTime)*Number(quantityArray[i]);
        } 

        // update the promotion based on promotion id
        const updatedResult = await promotion.update({
            promotionName : req.body.promotionName,
            promotionDescription : req.body.promotionDescription,
            unitPrice : req.body.unitPrice,
            unitCost :  unitCost,
            keywords : JSON.stringify(req.body.keywords),
            fromDate : req.body.fromDate,
            toDate : req.body.toDate,
            serveTime : JSON.stringify(req.body.serveTime),
            availability : req.body.availability,
            hotelId : req.body.hotelId,
            restaurantId : req.body.restaurantId,
            avgPrepareTime : avgPrepareTime,
            serviceCharge : req.body.serviceCharge,
            serviceChargeType : req.body.serviceChargeType,
            image : req.body.imageName
        } , {where : {id : req.body.promotionId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId} , transaction : tr});

        if(updatedResult != 1 || updatedResult < 0){
            statusCode = 500;
            customError.comment = "System error!!";
            throw new Error();
        }else {
            // delete all the reference to the promotion_item table
            const deleteResult = await promotion_item.destroy({
                where : {promotionId : req.body.promotionId}
            } , {transaction : tr});
            
            if(deleteResult < 0){
                statusCode = 400;
                customError.comment = "update is not completed!!";
                throw new Error();
            }

            // create the entry for the promotion_item table
            let promotionItemEntry = [];
            for(let i=0;i<searchItemResult.length;i++){
                let temp = {
                    promotionId : req.body.promotionId,
                    itemId : itemsIdArray[i],
                    quantity : quantityArray[i]
                }
                promotionItemEntry.push(temp);
            }

            // enter bulk for promotion_items table
            const newPromotionItems = await promotion_item.bulkCreate(promotionItemEntry , {transaction : tr});
            if(isEmpty(newPromotionItems)){
                statusCode = 400;
                customError.comment = "promotion not created!!";
                throw new Error();
            }

            // successfully promotion created
            statusCode = 200;
            await tr.commit();
            return res.status(statusCode).json(customSuccess);

        }
    }catch(error){
        console.log(error);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}