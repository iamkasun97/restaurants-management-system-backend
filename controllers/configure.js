const {sequelize} = require("../models");
const {hotel} = require('../models');
const {restaurant} = require('../models');
const {department} = require('../models');
const {role} = require('../models');
const {users} = require('../models');
const {owner} = require('../models');
const {category} = require('../models');
const {tax} = require('../models');

const config = require("../config");
const isEmpty = require("is-empty");
const bcrypt = require('bcryptjs');

// tested
exports.enterNewHotel = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;
    try{

       // check the request

        if(isEmpty(req.body.userId)){
            statusCode = 400;
            customError.comment = "userId is missing!!";
            throw new Error(); 
        }

        if(isEmpty(req.body.hotelName)){
            statusCode = 400;
            customError.comment = "hotel name can't be empty!!";
            throw new Error();
        }

        if(isEmpty(req.body.hotelAddress)){
            statusCode = 400;
            customError.comment = "hotel address can't be empty!!";
            throw new Error();
        }

        // check the name is already exist
        const searchRes = await hotel.findAll({
            where : {hotelName : req.body.hotelName , ownerId : req.body.userId}
        });
        if(!isEmpty(searchRes)){
            statusCode = 400;
            customError.comment = "hotel name already exist try another one!!";
            throw new Error();
        }
        
        // create a new hotel
        const newHotel = await hotel.create({
            hotelName : req.body.hotelName,
            ownerId : req.body.userId,
            hotelAddress : req.body.hotelAddress,
            hotelStatus : 1
        } , {transaction : tr});
        if(isEmpty(newHotel)){
            statusCode = 500;
            customError.comment = 'new hotel not created succuessfully!!';
            throw new Error();
        }

        // successfully created
        statusCode = 200;
        await tr.commit();
        customSuccess.data = newHotel;
        return res.status(statusCode).json(customSuccess);
    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// tested
// update hotel 
exports.updateHotel = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;
    try{

        // hotel id 
        const hotelId = req.body.hotelId;

        // check empty

        if(isEmpty(req.body.userId)){
            statusCode = 400;
            customError.comment = "userId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.hotelName)){
            statusCode = 400;
            customError.comment = "hotel name can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.hotelAddress)){
            statusCode = 400;
            customError.comment = "hotel address can't be empty!!";
            throw new Error();
        }
        if(isEmpty(hotelId)){
            statusCode = 400;
            customError.comment = "hotel id can't be empty!!";
            throw new Error();
        }

        // update hotel details
        const updatedResult = await hotel.update({
            hotelName : req.body.hotelName,
            hotelAddress: req.body.hotelAddress
        } , {where : {id : hotelId , ownerId : req.body.userId} , transaction : tr});

        if(updatedResult != 1 || updatedResult < 0){
            statusCode = 500;
            customError.comment = "system error!!";
            throw new Error();
        }

        statusCode = 200;
        await tr.commit();
        return res.status(statusCode).json(customSuccess);


    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// deactivate a hotel
exports.changeHotelStatus = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.userId)){
            statusCode = 400;
            customError.comment = "userId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(req.body.hotelStatus === ""){
            statusCode = 400;
            customError.comment = "hotel status is missing!!";
            throw new Error();
        }

        // comment is optional

        if(req.body.hotelStatus == 0 || req.body.hotelStatus == 1){}
        else {
            statusCode = 400;
            customError.comment = "invalid status!!";
            throw new Error();
        }

        // check the same status or not
        const checkStatus = await hotel.findOne({
            where : {id : req.body.hotelId , ownerId : req.body.userId}
        });

        if(checkStatus.dataValues.hotelStatus == req.body.hotelStatus){
            statusCode = 400;
            customError.comment = "bad request!!";
            throw new Error(); 
        }

        const updatedResult = await hotel.update({
            hotelStatus : req.body.hotelStatus
        },{where : {id : req.body.hotelId , ownerId : req.body.userId}, transaction : tr});

        if(updatedResult != 1 || updatedResult < 0){
            statusCode = 500;
            customError.comment = "Error!!";
            throw new Error();
        }

        statusCode = 200;
        await tr.commit();
        return res.status(statusCode).json(customSuccess);
    }catch(error){
        console.log(error);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// get all the hotels owns by 
exports.getHotels = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;
    try{
        // check empty
        if(isEmpty(req.body.userId)){
            statusCode = 400;
            customError.comment = "userId is missing!!";
            throw new Error();
        }

        const hotelList = await hotel.findAll({where : {ownerId : req.body.userId} , transaction : tr});
        if(isEmpty(hotelList)){
            statusCode = 200;
            await tr.commit();
            customSuccess.comment = 'no hotels registered!!';
            customSuccess.data = [];
            return res.status(statusCode).json(customSuccess);
        }else {
            statusCode = 200;
            await tr.commit();
            customSuccess.data = hotelList;
            return res.status(statusCode).json(customSuccess);
        }
    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// tested
// create a restaurant
exports.enterNewRestaurant = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;
    try{

        if(isEmpty(req.body.userId)){
            statusCode = 400;
            customError.comment = "userId is missing!!";
            throw new Error();
        }
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }
        if(isEmpty(req.body.restaurantName)){
            statusCode = 400;
            customError.comment = "restaurant name can't be empty!!";
            throw new Error();
        }
        // both mobile and restaurant description are optional

        // restaurant name exist
        const searchRes = await restaurant.findAll({
            where : {restaurantName : req.body.restaurantName, ownerId : req.body.userId, hotelId : req.body.hotelId}
        });
        if(!isEmpty(searchRes)){
            statusCode = 400;
            customError.comment = "restaurant name is already exist try another one!!";
            throw new Error();
        }

        // match the hotel with user id
        const searchRes1 = await hotel.findAll({
            where : {ownerId : req.body.userId,id : req.body.hotelId}
        });

        if(isEmpty(searchRes1)){
            statusCode = 400;
            customError.comment = "userId and hotelId are not mapped each other!!";
            throw new Error();
        }

        // create a new restaurant under a hotel
        const newRestaurant = await restaurant.create({
            restaurantName : req.body.restaurantName,
            hotelId : req.body.hotelId,
            ownerId : req.body.userId,
            restaurantDescription : req.body.restaurantDescription,
            mobile : req.body.mobile,
            restaurantStatus : 1
        } , {transaction : tr});

        if(isEmpty(newRestaurant)){
            statusCode = 500;
            customError.comment = "new restaurant not created!!";
            throw new Error();
        }

        // restaurant created successfully
        statusCode = 200;
        await tr.commit();
        customSuccess.data = newRestaurant;
        return res.status(statusCode).json(customSuccess);

        
    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// get restaurant under a hotel and  owner
exports.getRestaurants = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.userId)){
            statusCode = 400;
            customError.comment = "userId is missing!!";
            throw new Error();
        }

        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        // search result
        const restaurants = await restaurant.findAll({
            where : {
                ownerId : req.body.userId,
                hotelId : req.body.hotelId,
            }
        });

        if(isEmpty(restaurants)){
            statusCode = 200;
            customSuccess.comment = "no restaurant resgisterd!!";
            customSuccess.data = [];
            return res.status(statusCode).json(customSuccess);
        }

        statusCode = 200;
        await tr.commit();
        customSuccess.data = restaurants;
        return res.status(statusCode).json(customSuccess);
    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// update restaurant details
exports.updateRestaurant = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;
    
    try{
        // check empty
        if(isEmpty(req.body.userId)){
            statusCode = 400;
            customError.comment = "userId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantName)){
            statusCode = 400;
            customError.comment = "restaurantName is missing!!";
            throw new Error();
        }

        // update the restaurant
        const updatedResult = await restaurant.update({
            restaurantName : req.body.restaurantName,
            restaurantDescription : req.body.restaurantDescription,
            mobile : req.body.mobile
        }, {where : {id : req.body.restaurantId , ownerId : req.body.userId , hotelId : req.body.hotelId} , transaction : tr})

        // check the result
        if(updatedResult != 1 || updatedResult < 0){
            statusCode = 500;
            customError.comment = "system error!!";
            throw new Error();
        }

        statusCode = 200;
        await tr.commit();
        return res.status(statusCode).json(customSuccess);

    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// deactivate a restaurant 
exports.changeRestaurantStatus = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.userId)){
            statusCode = 400;
            customError.comment = "userId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(req.body.restaurantStatus === "" || req.body.restaurantStatus == undefined){
            statusCode = 400;
            customError.comment = "restaurant status is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        // status comment is optional

        if(req.body.restaurantStatus == 0 || req.body.restaurantStatus == 1){}
        else {
            statusCode = 400;
            customError.comment = "invalid status!!";
            throw new Error();
        }

        const checkStatus = await restaurant.findOne({
            where : {id : req.body.restaurantId , hotelId : req.body.hotelId , ownerId : req.body.userId}
        });

        if(checkStatus.dataValues.restaurantStatus == req.body.restaurantStatus){
            statusCode = 400;
            customError.comment = "bad request!!";
            throw new Error();
        }

        const updatedResult = await restaurant.update({
            restaurantStatus : req.body.restaurantStatus
        },{where : {id : req.body.restaurantId , hotelId : req.body.hotelId , ownerId : req.body.userId} , transaction : tr})
    
        // check the result
        if(updatedResult != 1 || updatedResult < 0){
            statusCode = 500;
            customError.comment = "system error!!";
            throw new Error();
        }

        statusCode = 200;
        await tr.commit();
        return res.status(statusCode).json(customSuccess);
    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// add a new department to the system
exports.enterNewDepartment = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;
    try{

        // check empty
        if(isEmpty(req.body.userId)){
            statusCode = 400;
            customError.comment = "userId is missing!!";
            throw new Error();
        }

        // department only map with the ownerId
        if(isEmpty(req.body.departmentName)){
            statusCode = 400;
            customError.comment = "department name can't be empty!!";
            throw new Error();
        }
        // department description is optional

        // check department name is exist
        const searchRes = await department.findAll({
            where : {departmentName : req.body.departmentName , ownerId : req.body.userId}
        });
        if(!isEmpty(searchRes)){
            statusCode = 400;
            customError.comment = "department name already exist!!";
            throw new Error();
        }

        // create a new department
        const newDepartment = await department.create({
            departmentName : req.body.departmentName,
            departmentDescription : req.body.departmentDescription,
            ownerId : req.body.userId,
            departmentStatus : 1
        } , {transaction : tr});

        if(isEmpty(newDepartment)){
            statusCode = 500;
            customError.comment = "department is not created!!";
            throw new Error();
        }

        // department created successfully
        statusCode = 200;
        await tr.commit();
        customSuccess.data = newDepartment;
        return res.status(statusCode).json(customSuccess);

    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// get departments under a owner
exports.getDepartments = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.userId)){
            statusCode = 400;
            customError.comment = "userId is missing!!";
            throw new Error();
        }

        // find all departments under owner
        const departments = await department.findAll({
            where : {ownerId : req.body.userId}
        });

        if(isEmpty(departments)){
            statusCode = 200;
            await tr.commit();
            customSuccess.comment = 'no department registered!!';
            customSuccess.data = [];
            return res.status(statusCode).json(customSuccess);
        }else {
            statusCode = 200;
            await tr.commit();
            customSuccess.data = departments;
            return res.status(statusCode).json(customSuccess);
        }


    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// update department details
exports.updateDepartment = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.userId)){
            statusCode = 400;
            customError.comment = "userId is missing!!";
            throw new Error();
        }

        // department only map with the ownerId
        if(isEmpty(req.body.departmentId)){
            statusCode = 400;
            customError.comment = "departmentId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.departmentName)){
            statusCode = 400;
            customError.comment = "department name can't be empty!!";
            throw new Error();
        }


        // update the department details
        const updatedResult = await department.update({
            departmentName : req.body.departmentName,
            departmentDescription : req.body.departmentDescription
        } , {where : {id : req.body.departmentId , ownerId : req.body.userId} , transaction : tr});

        if(updatedResult != 1 || updatedResult < 0){
            statusCode = 500;
            customError.comment = "system error!!";
            throw new Error();
        }

        // updated successfully
        statusCode = 200;
        await tr.commit();
        return res.status(statusCode).json(customSuccess);

    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// deactivate a department
exports.changeDepartmentStatus = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.userId)){
            statusCode = 400;
            customError.comment = "userId is missing!!";
            throw new Error();
        }

        // department only map with the ownerId
        if(isEmpty(req.body.departmentId)){
            statusCode = 400;
            customError.comment = "departmentId is missing!!";
            throw new Error();
        }

        if(req.body.departmentStatus === "" || req.body.departmentStatus == undefined){
            statusCode = 400;
            customError.comment = "department status is missing!!";
            throw new Error();
        }

        // comment is optional

        if(req.body.departmentStatus == 0 || req.body.departmentStatus == 1){}
        else {
            statusCode = 400;
            customError.comment = "invalid department status!!";
            throw new Error();
        }

        const statusCheck = await department.findOne({
            where : {id : req.body.departmentId , ownerId : req.body.userId}
        });

        if(statusCheck.dataValues.departmentStatus == req.body.departmentStatus){
            statusCode = 400;
            customError.comment = "bad request!!";
            throw new Error();
        }

        const updatedResult = await department.update({
            departmentStatus : req.body.departmentStatus
        },{where : {id : req.body.departmentId , ownerId : req.body.userId} , transaction : tr});

        if(updatedResult != 1 || updatedResult < 0){
            statusCode = 500;
            customError.comment = "system error!!";
            throw new Error();
        }

        // updated successfully
        statusCode = 200;
        await tr.commit();
        return res.status(statusCode).json(customSuccess);
    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// add new role to the system
exports.enterNewRole = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.userId)){
            statusCode = 400;
            customError.comment = "userId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.roleName)){
            statusCode = 400;
            customError.comment = "role name can't be empty!!";
            throw new Error();
        }

        if(isEmpty(req.body.permissionLevel)){
            statusCode = 400;
            customError.comment = "permission level can't be empty!!";
            throw new Error();
        }

        const searchResult = await role.findAll({
            where : { roleName : req.body.roleName}
        });

        if(!isEmpty(searchResult)){
            statusCode = 400;
            customError.comment = "role name already exist!!";
            throw new Error();
        }

        // validate permission levels
        const permissionLevels = ["manager" , "chef" , "security" , "waiter" , "cashier" , "reception"];

        req.body.permissionLevel.forEach(level => {
            if(permissionLevels.includes(level)){}
            else {
                statusCode = 400;
                customError.comment = "invalid permission levels!!";
                throw new Error();
            }
        })

        // create a new role
        const newRole = await role.create({
            roleName : req.body.roleName,
            permissionLevel : JSON.stringify(req.body.permissionLevel)
        },{transaction : tr});

        if(isEmpty(newRole)){
            statusCode = 500;
            customError.comment = "new role not created!!";
            throw new Error();
        }

        // create successfully
        statusCode = 200;
        await tr.commit();
        customSuccess.data = newRole;
        return res.status(statusCode).json(customSuccess);
    }catch(error){
        console.log(error);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// get all roles
exports.getRoles = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        if(isEmpty(req.body.userId)){
            statusCode = 400;
            customError.comment = "userId is missing!!";
            throw new Error();
        }

        const roles = await role.findAll({});

        if(isEmpty(roles)){
            statusCode = 200;
            await tr.commit();
            customSuccess.comment = 'no roles registered!!';
            customSuccess.data = [];
            return res.status(statusCode).json(customSuccess);
        }else {
            statusCode = 200;
            await tr.commit();
            customSuccess.data = roles;
            return res.status(statusCode).json(customSuccess);
        }

    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}


// update role
exports.updateRole = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;
    try{
        if(isEmpty(req.body.userId)){
            statusCode = 400;
            customError.comment = "userId is missing!!";
            throw new Error();
        }
        if(isEmpty(req.body.roleId)){
            statusCode = 400;
            customError.comment = "roleId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.roleName)){
            statusCode = 400;
            customError.comment = "role name can't be empty!!";
            throw new Error();
        }

        if(isEmpty(req.body.permissionLevel)){
            statusCode = 400;
            customError.comment = "permission level can't be empty!!";
            throw new Error();
        }

        // update statement
        const updatedResult = await role.update({
            roleName : req.body.roleName,
            permissionLevel : JSON.stringify(req.body.permissionLevel)
        }, {where : {id : req.body.roleId} , transaction : tr});

        if(updatedResult != 1 || updatedResult < 0){
            statusCode = 500;
            customError.comment = "system error!!";
            throw new Error();
        }

        // updated successfully
        statusCode = 200;
        await tr.commit();
        return res.status(statusCode).json(customSuccess);

    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// dropdown details to add users
exports.dropDownDetails = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        if(req.body.role.includes('owner')){
                // scenario for an owner
                if(isEmpty(req.body.userId)){
                    statusCode = 400;
                    customError.comment = "userId is missing!!";
                    throw new Error();
                }

                const hotels = await hotel.findAll({
                    where : {ownerId : req.body.userId , hotelStatus : 1}
                });
                const restaurants = await restaurant.findAll({
                    where : {ownerId : req.body.userId , restaurantStatus : 1}
                });
                const departments = await department.findAll({
                    where: {ownerId : req.body.userId , departmentStatus : 1}
                });
                const roles = await role.findAll({});

                // add departments array to the response
                let departmentArray = [];
                departments.forEach(department => {
                    departmentArray.push(department.dataValues);
                });

                // add roles array to the response
                let roleArray = [];
                roles.forEach(role => {
                    roleArray.push(role.dataValues);
                });

                // do the mapping
                let hotelAndRestaurantArray = [];
                hotels.forEach(hotel => {
                    console.log(hotel);
                    let restaurantArray = [];
                    restaurants.forEach(restaurant => {
                        if(restaurant.dataValues.hotelId == hotel.dataValues.id){
                            restaurantArray.push(restaurant.dataValues);
                        }
                    });
                    let temp = {
                        id : hotel.dataValues.id,
                        hotelName : hotel.dataValues.hotelName,
                        hotelAddress : hotel.dataValues.hotelAddress,
                        statusComment : hotel.dataValues.statusComment,
                        ownerId : hotel.dataValues.ownerId,
                        restaurants : restaurantArray,
                    }
                    hotelAndRestaurantArray.push(temp);
                });
                let response = {
                    hotels : hotelAndRestaurantArray,
                    departments : departmentArray,
                    roles : roleArray
                }
                // console.log(response);
                statusCode = 200;
                await tr.commit();
                customSuccess.data = response;
                return res.status(statusCode).json(customSuccess);

        }else if(req.body.role.includes('manager')){
            // empty check
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            // get the hotel , restaurant and department details for and employee
            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            const hotelE = await hotel.findOne({
                where : {id : searchResult.dataValues.hotelId , hotelStatus : 1}
            });
            const restaurantE = await restaurant.findOne({
                where : {id : searchResult.dataValues.restaurantId , restaurantStatus : 1}
            });
            const departmentE = await department.findOne({
                where: {id : searchResult.dataValues.departmentId , departmentStatus : 1}
            });

            let restaurantArray = [];
            let departmentArray = [];
            let hotelAndRestaurantArray = [];

            restaurantArray.push(restaurantE.dataValues);
            departmentArray.push(departmentE.dataValues);
            let temp = {
                id : hotelE.dataValues.id,
                hotelName : hotelE.dataValues.hotelName,
                hotelAddress : hotelE.dataValues.hotelAddress,
                statusComment : hotelE.dataValues.statusComment,
                ownerId : hotelE.dataValues.ownerId,
                restaurants : restaurantArray,
            }

            hotelAndRestaurantArray.push(temp);

            let response = {
                hotels : hotelAndRestaurantArray,
                departments : departmentArray
            }
            statusCode = 200;
            await tr.commit();
            customSuccess.data = response;
            return res.status(statusCode).json(customSuccess);
        }
    }catch(error){
        console.log(error);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// add new user
exports.enterNewUser = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.departmentId)){
            statusCode = 400;
            customError.comment = "departmentId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.firstName)){
            statusCode = 400;
            customError.comment = "first name can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.lastName)){
            statusCode = 400;
            customError.comment = "last name can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.userName)){
            statusCode = 400;
            customError.comment = "user name can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.mobile)){
            statusCode = 400;
            customError.comment = "mobile number can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.roleId)){
            statusCode = 400;
            customError.comment = "roleId is missing!!";
            throw new Error();
        }
        if(isEmpty(req.body.employeeNumber)){
            statusCode = 400;
            customError.comment = "employee number can't be empty!!";
            throw new Error();
        }

        // check the password
        if(isEmpty(req.body.password)){
            statusCode = 400;
            customError.comment = "password can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.confirmPassword)){
            statusCode = 400;
            customError.comment = "confirm password can't be empty!!";
            throw new Error();
        }

        // check password and confirm password
        if(req.body.password != req.body.confirmPassword){
            statusCode = 400;
            customError.comment = "confirm password and password not matched!!";
            throw new Error();
        }

        // check the user name mobile number exists or not
        const userNameResult = await owner.findAll({
            where : {userName : req.body.userName}
        });
        if(!isEmpty(userNameResult)){
            statusCode = 400;
            customError.comment = "try another user name this user name is already in use!!";
            throw new Error();
        }

        const userNameResult1 = await users.findAll({
            where : {userName : req.body.userName}
        });
        if(!isEmpty(userNameResult1)){
            statusCode = 400;
            customError.comment = "try another user name this user name is already in use!!";
            throw new Error();
        }

        const mobileResult = await owner.findAll({
            where : {mobile : req.body.mobile}
        });
        if(!isEmpty(mobileResult)){
            statusCode = 400;
            customError.comment = "this mobile number already registerd!!";
            throw new Error();
        }
        const mobileResult1 = await users.findAll({
            where : {mobile : req.body.mobile}
        });
        if(!isEmpty(mobileResult1)){
            statusCode = 400;
            customError.comment = "this mobile number already registerd!!";
            throw new Error();
        }

        // check role id
        const roleCheck = await role.findAll({
            where : {id : req.body.roleId}
        });

        if(isEmpty(roleCheck)){
            statusCode = 400;
            customError.comment = "roleId is not valid!!";
            throw new Error(); 
        }

        // scenario from an owner
        if(req.body.role.includes('owner')){
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            } 
    
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });
            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
    
            // have to check the department is available
            const departmentCheck = await department.findAll({
                where : {id : req.body.departmentId , ownerId : req.body.userId}
            });
            if(isEmpty(departmentCheck)){
                statusCode = 400;
                customError.comment = "departmentId is not valid!!";
                throw new Error(); 
            }
    
            // password hash
            let salt = bcrypt.genSaltSync(10);
            let passwordHash = bcrypt.hashSync(req.body.password, salt);
    
            // create a user for a department
            const newUser = await users.create({
                hotelId : req.body.hotelId,
                restaurantId : req.body.restaurantId,
                departmentId : req.body.departmentId,
                firstName : req.body.firstName,
                lastName : req.body.lastName,
                userName : req.body.userName,
                password : passwordHash,
                mobile : req.body.mobile,
                roleId : req.body.roleId,
                employeeNumber : req.body.employeeNumber,
                createdOwner : req.body.userId
            } , {transaction : tr});
    
            if(isEmpty(newUser)){
                statusCode = 500;
                customError.comment = "new user is not created!!";
                throw new Error();
            }

            // user created successfully
            statusCode = 200;
            await tr.commit();
            return res.status(statusCode).json(customSuccess);

        }else if(req.body.role.includes('manager')){
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
            
            // have to check the department is available
            const departmentCheck = await department.findAll({
                where : {id : req.body.departmentId , ownerId : searchResult.dataValues.createdOwner}
            });
            if(isEmpty(departmentCheck)){
                statusCode = 400;
                customError.comment = "departmentId is not valid!!";
                throw new Error(); 
            }

            // password hash
            let salt = bcrypt.genSaltSync(10);
            let passwordHash = bcrypt.hashSync(req.body.password, salt);
    
            // create a user for a department
            const newUser = await users.create({
                hotelId : req.body.hotelId,
                restaurantId : req.body.restaurantId,
                departmentId : req.body.departmentId,
                firstName : req.body.firstName,
                lastName : req.body.lastName,
                userName : req.body.userName,
                password : passwordHash,
                mobile : req.body.mobile,
                roleId : req.body.roleId,
                employeeNumber : req.body.employeeNumber,
                createdOwner : searchResult.dataValues.createdOwner
            } , {transaction : tr});
    
            if(isEmpty(newUser)){
                statusCode = 500;
                customError.comment = "new user is not created!!";
                throw new Error();
            }

            // user created successfully
            statusCode = 200;
            await tr.commit();
            return res.status(statusCode).json(customSuccess);
        }

        customError.comment = "error occurs!!";
        throw new Error();
    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// get user details
exports.getUserDetails = async (req , res , next) => {{
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty values
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }
        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }
        if(isEmpty(req.body.departmentId)){
            statusCode = 400;
            customError.comment = "departmentId is missing!!";
            throw new Error();
        }

        // add the pagination concept here
        const pageAsNumber = Number.parseInt(req.query.page);
        const sizeAsNumber = Number.parseInt(req.query.size);

        let page = 0;
        if(!Number.isNaN(pageAsNumber) && pageAsNumber > 0){
            page = pageAsNumber;
        }

        let size = 5;
        if(!Number.isNaN(sizeAsNumber) && sizeAsNumber > 0){
            size = sizeAsNumber;
        }

        // use COUNT to get count
        // use limit and offset
        const userCount = await sequelize.query(
            `
            SELECT DISTINCT COUNT(*) AS count FROM users
            WHERE hotelId = ${req.body.hotelId} AND restaurantId = ${req.body.restaurantId} AND departmentId = ${req.body.departmentId}
            `
        );

        const usersResult = await sequelize.query(
            `
            SELECT u.id AS id , u.firstName , u.lastName , u.userName , u.employeeNumber , u.mobile , u.hotelId , u.restaurantId , u.departmentId , r.id AS roleId , r.roleName , r.permissionLevel
            FROM users u
            JOIN roles r
            ON u.roleId = r.id
            WHERE u.hotelId = ${req.body.hotelId} AND u.restaurantId = ${req.body.restaurantId} AND u.departmentId = ${req.body.departmentId}
            LIMIT ${size} OFFSET ${page * size}
            `
        );

        if(isEmpty(usersResult)){
            statusCode = 200;
            customSuccess.comment = "no users resgisterd!!";
            customSuccess.data = [];
            return res.status(statusCode).json(customSuccess);
        }

        let result = {
            count : userCount[0][0].count,
            rows : usersResult[0]
        }

        statusCode = 200;
        await tr.commit();
        customSuccess.data = result;
        return res.status(statusCode).json(customSuccess);

    }catch(error){
        console.log(error);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}}


// update user details
exports.updateUserDetails = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        if(isEmpty(req.body.firstName)){
            statusCode = 400;
            customError.comment = "first name can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.lastName)){
            statusCode = 400;
            customError.comment = "last name can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.userName)){
            statusCode = 400;
            customError.comment = "user name can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.mobile)){
            statusCode = 400;
            customError.comment = "mobile number can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.roleId)){
            statusCode = 400;
            customError.comment = "roleId is missing!!";
            throw new Error();
        }
        if(isEmpty(req.body.employeeNumber)){
            statusCode = 400;
            customError.comment = "employee number can't be empty!!";
            throw new Error();
        }

        // user id empty
        if(isEmpty(req.body.updateUserId)){
            statusCode = 400;
            customError.comment = "update userId can't be empty!!";
            throw new Error();
        }

        // check the user name mobile number exists or not
        const userNameResult = await owner.findAll({
            where : {userName : req.body.userName}
        });
        if(!isEmpty(userNameResult)){
            statusCode = 400;
            customError.comment = "try another user name this user name is already in use!!";
            throw new Error();
        }

        const userNameResult1 = await users.findAll({
            where : {userName : req.body.userName}
        });
        if(!isEmpty(userNameResult1)){
            statusCode = 400;
            customError.comment = "try another user name this user name is already in use!!";
            throw new Error();
        }

        const mobileResult = await owner.findAll({
            where : {mobile : req.body.mobile}
        });
        if(!isEmpty(mobileResult)){
            statusCode = 400;
            customError.comment = "this mobile number already registerd!!";
            throw new Error();
        }
        const mobileResult1 = await users.findAll({
            where : {mobile : req.body.mobile}
        });
        if(!isEmpty(mobileResult1)){
            statusCode = 400;
            customError.comment = "this mobile number already registerd!!";
            throw new Error();
        }

        

        // update the user details not password
        const updatedResult = await users.update({
            firstName : req.body.firstName,
            lastName : req.body.lastName,
            userName : req.body.userName,
            mobile : req.body.mobile,
            roleId : req.body.roleId,
            employeeNumber : req.body.employeeNumber,
        } , {where : {id : req.body.updateUserId} , transaction : tr});

        if(updatedResult != 1 || updatedResult < 0){
            statusCode = 500;
            customError.comment = "System error!!";
            throw new Error();
        }

        // successfully updated
        statusCode = 200;
        await tr.commit();
        return res.status(statusCode).json(customSuccess);
    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}


// add category 
exports.enterNewCategory = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.userId)){
            statusCode = 400;
            customError.comment = "userId is missing!!";
            throw new Error();
        }
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }
        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }
        if(isEmpty(req.body.categoryName)){
            statusCode = 400;
            customError.comment = "categoryName can't be empty!!";
            throw new Error();
        }
        // category description is optional

        //check the mapping between hotel , restaurant and owner
        const searchResult = await restaurant.findOne({
            where : {id : req.body.restaurantId , ownerId : req.body.userId , hotelId : req.body.hotelId}
        });
        if(isEmpty(searchResult)){
            statusCode = 400;
            customError.comment = "restaurantId is not valid!!";
            throw new Error();
        }

        // create new category
        const newCategory = await category.create({
            categoryName : req.body.categoryName,
            categoryDescription : req.body.categoryDescription,
            hotelId : req.body.hotelId,
            restaurantId : req.body.restaurantId
        } , {transaction : tr});

        if(isEmpty(newCategory)){
            statusCode = 500;
            customError.comment = "categoryName is not created!!";
            throw new Error();
        }

        // successfully created
        statusCode = 200;
        await tr.commit();
        customSuccess.data = newCategory;
        return res.status(statusCode).json(customSuccess);
    }catch(error){
        console.log(error);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

//dropdown details to add category
exports.dropDownDetailsForCategory = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        if(isEmpty(req.body.userId)){
            statusCode = 400;
            customError.comment = "userId is missing!!";
            throw new Error();
        }

        const hotels = await hotel.findAll({
            where : {ownerId : req.body.userId , hotelStatus : 1}
        });
        const restaurants = await restaurant.findAll({
            where : {ownerId : req.body.userId , restaurantStatus : 1}
        });


        // do the mapping
        let response = [];
        hotels.forEach(hotel => {
            let restaurantArray = [];
            restaurants.forEach(restaurant => {
                if(restaurant.hotelId == hotel.id){
                    restaurantArray.push(restaurant.dataValues);
                }
            });
            let temp = {
                hotel : hotel.dataValues,
                restaurants : restaurantArray,
            }
            response.push(temp);
        });
        statusCode = 200;
        await tr.commit();
        customSuccess.data = response;
        return res.status(statusCode).json(customSuccess);
    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// get category details for a restaurant under a hotel
exports.getCategories = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.userId)){
            statusCode = 400;
            customError.comment = "userId is missing!!";
            throw new Error();
        }
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }
        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        const categories = await category.findAll({
            where : {hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
        });
        if(isEmpty(categories)){
            statusCode = 200;
            customSuccess.comment = "no category resgisterd!!";
            customSuccess.data = [];
            return res.status(statusCode).json(customSuccess);
        }

        statusCode = 200;
        await tr.commit();
        customSuccess.data = categories;
        return res.status(statusCode).json(customSuccess);

    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// update categories
exports.updateCategoryDetails = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.userId)){
            statusCode = 400;
            customError.comment = "userId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.categoryId)){
            statusCode = 400;
            customError.comment = "categoryId is missing!!";
            throw new Error();
        }
        if(isEmpty(req.body.categoryName)){
            statusCode = 400;
            customError.comment = "categoryName can't be empty!!";
            throw new Error();
        }
        // category description is optional

        const updatedCategory = await category.update({
            categoryName : req.body.categoryName,
            categoryDescription : req.body.categoryDescription
        }, {where : {id : req.body.categoryId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId} , transaction : tr});

        if(updatedCategory != 1 || updatedCategory < 0){
            statusCode = 500;
            customError.comment = "System error!!";
            throw new Error();
        }

        statusCode = 200;
        await tr.commit();
        return res.status(statusCode).json(customSuccess);
    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}


// set the tax rate
exports.setTaxRate = async (req, res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.userId)){
            statusCode = 400;
            customError.comment = "userId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.taxRate)){
            statusCode = 400;
            customError.comment = "tax arte can't be empty!!";
            throw new Error();
        }

        // set or update tax rate
        const searchResult = await tax.findOne({
            where : {hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , ownerId : req.body.userId}
        });

        if(isEmpty(searchResult)){
            const newTax = await tax.create({
                taxRate : req.body.taxRate,
                hotelId : req.body.hotelId,
                restaurantId : req.body.restaurantId,
                ownerId : req.body.userId
            }, {transaction : tr});

            if(isEmpty(newTax)){
                customError.comment = "new tax not created!!";
                throw new Error();
            }
        }else {
            const updatedResult = await tax.update({
                taxRate : req.body.taxRate
            }, {where : {id : searchResult.dataValues.id , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , ownerId : req.body.userId}, transaction : tr});
        
            if(updatedResult != 1 || updatedResult < 0){
                customError.comment = "System error!!";
                throw new Error();
            }
        }

        statusCode = 200;
        await tr.commit();
        return res.status(statusCode).json(customSuccess);

    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// get the tax rate
exports.getTaxRate = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.userId)){
            statusCode = 400;
            customError.comment = "userId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        let taxRate = 0;
        const searchResult = await tax.findOne({
            where : {hotelId : req.body.hotelId, restaurantId : req.body.restaurantId, ownerId : req.body.userId}
        });
        if(!isEmpty(searchResult)){
            taxRate = searchResult.dataValues.taxRate
        }

        statusCode = 200;
        await tr.commit();
        customSuccess.data = taxRate;
        return res.status(statusCode).json(customSuccess);
    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}


