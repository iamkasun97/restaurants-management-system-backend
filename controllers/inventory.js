const {sequelize} = require("../models");
const {pos_Transactions} = require("../models");
const {pos_Vendors} = require("../models");
const {pos_InventoryItems} = require("../models");
const {pos_BranchVendor} = require("../models");
const {pos_ReturnInventory} = require("../models");
const {pos_Items} = require("../models");
const {pos_Keywords} = require("../models");
const {pos_Discounts} = require("../models");
const config = require("../config");
const isEmpty = require("is-empty");
const {Op} = require("sequelize");

// create new entry in items table
//  tested
exports.enterNewItem = async (req, res, next) => {
  const tr = await sequelize.transaction();
  const customError = Object.assign({}, config.custom_error);
  const customSuccess = Object.assign({}, config.custom_success);
  let statusCode = 500;
  try{
    const branchId = req.body.branchId;
    const newItem = await pos_Items.create({
      branchId : branchId,
      itemName : req.body.itemName,
      image : req.body.image
    } , {transaction : tr});

    // use empty 
    if(isEmpty(newItem)){
      statusCode = 500;
      customError.comment = "item not created successfully";
      throw new Error();
    } else {
      statusCode = 200;
      const newKeyword = await pos_Keywords.create({
        keywords : req.body.keywords,
        itemId : newItem.id
      } , {transaction : tr});
      await tr.commit();
      customSuccess.data = newItem;
      return res.status(statusCode).json(customSuccess);
    }
  }catch(message){
    console.log(message);
    await tr.rollback();
    // add the content from the same resource
    return res.status(statusCode).json(customError);
  }
}

// find from the item from keyword
//  tested
exports.findItem = async (req , res , next) => {
  const customError = Object.assign({}, config.custom_error);
  const customSuccess = Object.assign({}, config.custom_success);
  let statusCode = 500;
  try{
    
    // add branch id to where clause
    const [result , metadata] = await sequelize.query(
      `SELECT pos_items.id , pos_items.itemName , pos_items.image FROM pos_items
      JOIN pos_keywords
      ON pos_items.id = pos_keywords.itemId
      WHERE MATCH(keywords) against('${req.body.keyword}') AND pos_items.branchId = ${req.body.branchId};
      `
    );
    if(isEmpty(result)){
      customError.comment = "search result is empty"
      statusCode = 400;
      throw new Error();
    } else {
      statusCode = 200;
      customSuccess.data = result;
    }        
    return res.status(statusCode).json(customSuccess);
  }catch(message){
    console.log(message);
    return res.status(statusCode).json(customError);
  }
}

// create a new vendor 
// tested
exports.createNewVendor = async (req , res , next) => {
  const tr = await sequelize.transaction();
  const customError = Object.assign({}, config.custom_error);
  const customSuccess = Object.assign({}, config.custom_success);
  let statusCode = 500;
  try{
    const branchId = req.body.branchId;
    const newVendor = await pos_Vendors.create({
      vendorName : req.body.vendorName,
      vendorAddress : req.body.vendorAddress,
      vendorContact : req.body.vendorContact,
      isActive : req.body.isActive
    } , {transaction : tr});

    if(isEmpty(newVendor)){
      statusCode = 500;
      customError.comment = "new vendor is not created successfully.";
      throw new Error();
    } else {
      const newBranchVendor = await pos_BranchVendor.create({
        branchId : branchId,
        vendorId : newVendor.id
      }, {transaction : tr});

      if(isEmpty(newBranchVendor)){
        statusCode = 500;
        customError.comment = "new vendor branch entry is not created successfully.";
        throw new Error();
      } else {
        statusCode = 200;
        await tr.commit();
        customSuccess.data = newVendor;
        return res.status(statusCode).json(customSuccess);
      } 
    }
  }catch(message) {
    console.log(message);
    await tr.rollback();
    return res.status(statusCode).json(customError);
  }
}

// find the vendor by mobile number
// tested
exports.findVendors = async (req , res , next) => {
  const tr = await sequelize.transaction();
  const customError = Object.assign({}, config.custom_error);
  const customSuccess = Object.assign({}, config.custom_success);
  let statusCode = 500;
  try{
    const branchId = req.body.branchId;
    let result;
    if(isEmpty(req.body.vendorName && !isEmpty(req.body.vendorContact))){
      result = await sequelize.query(
        `SELECT v.id , v.vendorName , v.vendorAddress , v.vendorContact , v.isActive FROM pos_vendors AS v
        JOIN pos_branchvendors AS b
        ON v.id = b.vendorId
        WHERE b.branchId = ${branchId} AND (v.vendorContact = ${req.body.vendorContact});`
      ); 
    } else if(isEmpty(req.body.vendorContact) && !isEmpty(req.body.vendorName)) {
      result = await sequelize.query(
        `SELECT v.id , v.vendorName , v.vendorAddress , v.vendorContact , v.isActive FROM pos_vendors AS v
        JOIN pos_branchvendors AS b
        ON v.id = b.vendorId
        WHERE b.branchId = ${branchId} AND (v.vendorName LIKE '${req.body.vendorName}');`
      );
    } else {
      result = await sequelize.query(
        `SELECT v.id , v.vendorName , v.vendorAddress , v.vendorContact , v.isActive FROM pos_vendors AS v
        JOIN pos_branchvendors AS b
        ON v.id = b.vendorId
        WHERE b.branchId = ${branchId} AND (v.vendorName LIKE '${req.body.vendorName}' OR v.vendorContact = '${req.body.vendorContact}');`
      );
    }

    if(isEmpty(result[0])) {
      statusCode = 400;
      customError.comment = "no vendor found by this mobile number";
      throw new Error();
    } else {
      statusCode = 200;
      await tr.commit();
      customSuccess.data = result[0];
      return res.status(statusCode).json(customSuccess);
    }
  }catch(message) {
    console.log(message);
    await tr.rollback();
    return res.status(statusCode).json(customError);
  }
}

// start the transaction
// tested
exports.newTransactionStart = async (req , res , next) => {
  const tr = await sequelize.transaction();
  const customError = Object.assign({}, config.custom_error);
  const customSuccess = Object.assign({}, config.custom_success);
  let statusCode = 500;
  try{
    const branchId = req.body.branchId;
    const newTransaction = await pos_Transactions.create({
      branchId : branchId,
      dateToSettle : req.body.dateToSettle,
      modeOfPayment : req.body.modeOfPayment,
      transactionType : 0,
      vendorId : req.body.vendorId,
      invoiceNumber : req.body.invoiceNumber,
      invoiceImage : req.body.invoiceImage
    } , { transaction : tr});

    if(isEmpty(newTransaction)){
      statusCode = 500;
      customError.comment = "new transaction not started successfully";
      throw new Error();
    } else {
      statusCode = 200;
      await tr.commit();
      customSuccess.data = newTransaction;
      return res.status(statusCode).json(customSuccess);
    }
  }catch(message) {
    console.log(message);
    await tr.rollback();
    return res.status(statusCode).json(customError);
  }
}




// tested
exports.createNewInventory = async (req , res , next) => {

    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;
    try{
      const branchId = req.body.branchId;

      // check here the transaction type is one
      // if one throw an error
      const transaction = await pos_Transactions.findOne({where : {id : req.body.transactionId , branchId : branchId}});
      if(transaction.transactionType === 1){
        statusCode = 400;
        customError.comment = 'this transaction already finished!';
        throw new Error();
      }

      // create new inventory item
      const newInventoryItem = await pos_InventoryItems.create({
        branchId : branchId,
        transactionId : req.body.transactionId,
        sellingPrice : req.body.sellingPrice,
        broughtPrice : req.body.broughtPrice,
        itemCode : req.body.itemCode,
        itemName : req.body.itemName,
        expireDate : req.body.expireDate,
        manufactureDate : req.body.manufactureDate,
        category : req.body.category,
        unit : req.body.unit,
        numberOfUnits : req.body.numberOfUnits,
        itemId : req.body.itemId,
      } , {transaction : tr});

      if(isEmpty(newInventoryItem)){
        statusCode = 500;
        customError.comment = "item enter to the inventory unsuccessful";
        throw new Error();
      } else {
        await tr.commit();
        statusCode = 200;
        customSuccess.data = newInventoryItem;
        return res.status(statusCode).json(customSuccess);
      }
      }catch(message){
        console.log(message);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// find the specific transaction with vendor for identify the specific inventory item set
// consider the branch id
// tested
exports.findTransactions = async (req , res , next) => {
  const tr = await sequelize.transaction();
  const customError = Object.assign({}, config.custom_error);
  const customSuccess = Object.assign({}, config.custom_success);
  let statusCode = 500;
  try{
    // query
    // const transactions = await pos_Transactions.findAll({where : {invoiceNumber : req.body.invoiceNumber , branchId : req.body.branchId} , transaction : tr});
    const [transactions , metadata] = await sequelize.query(
      `SELECT t.id AS transactionId , t.invoiceNumber , t.invoiceImage , v.id AS vendorId , v.vendorName FROM pos_transactions AS t
      JOIN pos_vendors AS v
      ON v.id = t.vendorId
      WHERE invoiceNumber LIKE '${req.body.invoiceNumber}' AND branchId = ${req.body.branchId};`,
      {transaction : tr}
    );
    if(isEmpty(transactions)){
      statusCode = 400;
      customError.comment = "no transactions for the specific invoice number";
      throw new Error();
    } else {
      statusCode = 200;
      customSuccess.data = transactions;
      await tr.commit();
      return res.status(statusCode).json(customSuccess);
    }
  }catch(message){
    console.log(message);
    await tr.rollback();
    return res.status(statusCode).json(customError);
  }
}

// find the inventory items based on the invoice
// tested
exports.findInventoryItemSet = async (req , res , next) => {
  const tr = await sequelize.transaction();
  const customError = Object.assign({}, config.custom_error);
  const customSuccess = Object.assign({}, config.custom_success);
  let statusCode = 500;
  try{
    const branchId = req.body.branchId;
    const inventoryItems = await pos_InventoryItems.findAll({where : {transactionId : req.body.transactionId , branchId : branchId} , transaction : tr});
    
    if(isEmpty(inventoryItems)){
      statusCode = 400;
      customError.comment = "empty item list for the transaction id";
      throw new Error();
    } else {
      statusCode = 200;
      customSuccess.data = inventoryItems;
      return res.status(statusCode).json(customSuccess);
    }
  }catch(message) {
    console.log(message);
    return res.status(statusCode).json(customError);
  }
} 

// update inventory items stock base on ID
// checked
exports.updateInventoryItemSellingPrice = async (req , res , next) => {

  const tr = await sequelize.transaction();
  const customError = Object.assign({}, config.custom_error);
  const customSuccess = Object.assign({}, config.custom_success); 
  let statusCode = 500;
  try{
    // do the find before updating
    const branchId = req.body.branchId;
    const inventoryItem = await pos_InventoryItems.findOne({where : {id : req.body.inventoryItemsId , branchId : branchId}});
    if(isEmpty(inventoryItem)){
      statusCode = 400;
      customError.comment = "inventory item not found";
      throw new Error();
    }else if(inventoryItem.sellingPrice == req.body.sellingPrice){
      statusCode = 400;
      customError.comment = "same value entered for selling price";
      throw new Error();
    }else {
      const sellingPriceDetails = {
        price : inventoryItem.sellingPrice,
        updatedDate : new Date()
      };
      const oldSellingPrices = JSON.parse(inventoryItem.oldSellingPrices);
      oldSellingPrices.push(sellingPriceDetails);
      await pos_InventoryItems.update(
        {
        sellingPrice : req.body.sellingPrice,
        oldSellingPrices : JSON.stringify(oldSellingPrices)
        }, 
       { where : {id : req.body.inventoryItemsId} , transaction : tr}
      );
      statusCode = 200;
      customSuccess.comment = "selling price updated successfully";
      await tr.commit();
      return res.status(statusCode).json(customSuccess);
    }
  }catch(message){
    console.log(message);
    await tr.rollback();
    return res.status(statusCode).json(customError);
  }
}

// update the transaction status by id
//  checked
exports.updateTransactionType = async (req, res, next) => {
  const customError = Object.assign({}, config.custom_error);
  const customSuccess = Object.assign({}, config.custom_success);
  let statusCode = 500;
  try{
    const branchId = req.body.branchId
    const transaction = await pos_Transactions.findOne({where : {id : req.body.transactionId , branchId : branchId}});
    if(isEmpty(transaction)){
      statusCode = 400;
      customError.comment = "empty result for the transaction id";
      throw new Error();
    }else if(transaction.transactionType == req.body.transactionType){
      statusCode = 400;
      customError.comment = "you enter same value for the transaction status";
      throw new Error();
    } else {
      let transDetails = await pos_Transactions.update(
        {
          transactionType : req.body.transactionType
        },
        {where : {id : req.body.transactionId}}
      );
      if(transDetails[0] >= 1){
        statusCode = 200;
        customSuccess.comment = "transaction type change successfully";
        return res.status(statusCode).json(customSuccess);
      } else {
        statusCode = 500;
        customError.comment = "transaction type not change error occures";
        throw new Error();
      }
    }
  }catch(message){
    console.log(message);
    return res.status(statusCode).json(customError);
  }
}

// use the find invoice id route before this
// use the same flow as update inventory
//  checked
exports.returnInventory = async (req , res , next) => {
  const tr = await sequelize.transaction();
  const customError = Object.assign({}, config.custom_error);
  const customSuccess = Object.assign({}, config.custom_success);
  let statusCode = 500;
  try{
    // use decrement function in sequelize
    const branchId = req.body.branchId;
    const itemDetails = await pos_InventoryItems.findOne({where : {id : req.body.inventoryItemsId , branchId : branchId} , transaction : tr});
    if(isEmpty(itemDetails)){
      statusCode = 400;
      customError.comment = "inventoy item does not exist for the inventory item id";
      throw new Error();
    }else {
      if(itemDetails.numberOfUnits >= req.body.returnNumberOfUnits){
        decrementResult = await itemDetails.decrement('numberOfUnits' , {by : req.body.returnNumberOfUnits , where : {numberOfUnits : {[Op.gte] : req.body.returnNumberOfUnits}}, transaction : tr});
        console.log("decrement function return result "+decrementResult.length);
        const returnInventoryItem = await pos_ReturnInventory.create({
          branchId : branchId,
          inventoryItemId : req.body.inventoryItemsId,
          returnNumberOfUnits : req.body.returnNumberOfUnits
        } , {transaction : tr});
        if(isEmpty(returnInventoryItem)){
          statusCode = 500;
          customError.comment = "return item not successful";
          throw new Error();     
        }else {
          statusCode = 200;
          await tr.commit();
          customSuccess.data = returnInventoryItem;
          return res.status(statusCode).json(customSuccess);
        }
      } else {
        statusCode = 400;
        await tr.rollback();
        customError.comment = "enter a valid number for return stock";
        throw new Error();
      }
    }
  }catch(message){
    console.log(message);
    await tr.rollback();
    return res.status(statusCode).json(customError);
  }
}

// create a new  discount for an inventory item
// tested
exports.newDiscount = async (req , res , next) => {
  const tr = await sequelize.transaction();
  const customError = Object.assign({}, config.custom_error);
  const customSuccess = Object.assign({}, config.custom_success);
  let statusCode = 500;
  try{
    const branchId = req.body.branchId;
    const discountStartedDate = new Date(req.body.discountStartedDate);
    const discountEndDate = new Date(req.body.discountEndDate);
    if(discountStartedDate.getTime() > discountEndDate.getTime()){
      statusCode = 400;
      customError.comment = "enter a valid discount period";
      throw new Error();
    }
    const discounts = await pos_Discounts.findAll({where : {inventoryItemsId : req.body.inventoryItemsId , branchId : branchId , isActive : true} , transaction : tr});
    console.log(discounts);
    if(isEmpty(discounts)){
      const newDiscount = await pos_Discounts.create({
        branchId : branchId,
        inventoryItemsId : req.body.inventoryItemsId,
        discountStartedDate : req.body.discountStartedDate,
        discountEndDate : req.body.discountEndDate,
        discountRate : req.body.discountRate,
        discountPrice : req.body.discountPrice,
        isRate : req.body.isRate
      });
      if(isEmpty(newDiscount)){
        statusCode = 500;
        customError.comment = "new discount is not entered successfully";
        throw new Error();
      }else {
        statusCode = 200;
        customSuccess.data = newDiscount;
        await tr.commit();
        return res.status(statusCode).json(customSuccess);
      }
    } else {
      let conflict = false;
      for(let i = 0; i < discounts.length; i++){
        if(discounts[i].discountStartedDate.getTime() <= discountStartedDate.getTime() && discounts[i].discountEndDate.getTime() >= discountEndDate.getTime()){
          conflict = true;
          break;
        }

        if(discounts[i].discountStartedDate.getTime() >= discountStartedDate.getTime() && discounts[i].discountStartedDate.getTime() <= discountEndDate.getTime()){
          conflict = true;
          break;
        }

        if(discounts[i].discountEndDate.getTime() >= discountStartedDate.getTime() && discounts[i].discountEndDate.getTime() <= discountEndDate.getTime()){
          conflict = true;
          break;
        }

        if(discounts[i].discountStartedDate.getTime() >= discountStartedDate.getTime() && discounts[i].discountEndDate.getTime() <= discountEndDate.getTime()){
          conflict = true;
          break;
        }
      }
      if(conflict == true){
        // discount dates conflicted 
        statusCode = 400;
        customError.comment = "you enter discount period is invalid";
        throw new Error();
      } else {
        // discount dates are not conflicted
        const newDiscount = await pos_Discounts.create({
          branchId : branchId,
          inventoryItemsId : req.body.inventoryItemsId,
          discountStartedDate : req.body.discountStartedDate,
          discountEndDate : req.body.discountEndDate,
          discountRate : req.body.discountRate,
          discountPrice : req.body.discountPrice,
          isRate : req.body.isRate
        });

        if(isEmpty(newDiscount)){
          statusCode = 500;
          customError.comment = "discount is not entered successfully";
          throw new Error();
        } else {
          statusCode = 200;
          customSuccess.data = newDiscount;
          await tr.commit();
          return res.status(statusCode).json(customSuccess);
        }
      }
    }
  }catch(message){
    console.log(message);
    await tr.rollback();
    return res.status(statusCode).json(customError);
  }
}

// find all discounts
// checked
exports.findDiscounts = async (req , res , next) => {
  const customError = Object.assign({}, config.custom_error);
  const customSuccess = Object.assign({}, config.custom_success);
  let statusCode = 500;
  try{
    const branchId = req.body.branchId;
    const discounts = await pos_Discounts.findAll({where : {inventoryItemsId : req.body.inventoryItemsId , branchId : branchId}});
    if(isEmpty(discounts)){
      statusCode = 400;
      customError.comment = "no discounts available for this inventory item.";
      throw new Error();
    } else {
      statusCode = 200;
      customSuccess.data = discounts;
      return res.status(statusCode).json(customSuccess);
    }
  }catch(message){
    console.log(message);
    return res.status(400).json(customError);
  }
}

// update the discount rate
// checked
exports.updateDiscount = async (req , res , next) => {
  const tr = await sequelize.transaction();
  const customError = Object.assign({}, config.custom_error);
  const customSuccess = Object.assign({}, config.custom_success);
  let statusCode = 500;
  try{ 
    const branchId = req.body.branchId;
    const discount = await pos_Discounts.findOne({where : {id : req.body.discountId , branchId : branchId} , transaction : tr});
    if(isEmpty(discount)){
      statusCode = 400;
      customError.comment = "discount id is not valid";
      throw new Error();
    }else {
      const startedDate = new Date(req.body.discountStartedDate);
      const endDate = new Date(req.body.discountEndDate);

      const otherDiscounts = await pos_Discounts.findAll({where : {inventoryItemsId : req.body.inventoryItemsId , id : {[Op.ne] : req.body.discountId} , isActive : true} , transaction : tr});
      if(isEmpty(otherDiscounts)){
        const updateDiscount = await pos_Discounts.update(
          {
            discountStartedDate : req.body.discountStartedDate,
            discountEndDate : req.body.discountEndDate,
            discountRate : req.body.discountRate,
            discountPrice : req.body.discountPrice,
            isRate : req.body.isRate,
          },
          {where : {id : req.body.discountId} , transaction : tr}
        );

        if(updateDiscount[0] >= 1){
          statusCode = 200;
          await tr.commit();
          return res.status(statusCode).json(customSuccess); 
        } else {
          statusCode = 500;
          customError.comment = "record update unsuccessful";
          throw new Error();
        }
      } else {
        let conflict = false;
        for(let i=0;i<otherDiscounts.length; i++){
          if(otherDiscounts[i].discountStartedDate.getTime() <= startedDate.getTime() && otherDiscounts[i].discountEndDate.getTime() >= endDate.getTime()){
            conflict = true;
            break;
          }
  
          if(otherDiscounts[i].discountStartedDate.getTime() >= startedDate.getTime() && otherDiscounts[i].discountStartedDate.getTime() <= endDate.getTime()){
            conflict = true;
            break;
          }
  
          if(otherDiscounts[i].discountEndDate.getTime() >= startedDate.getTime() && otherDiscounts[i].discountEndDate.getTime() <= endDate.getTime()){
            conflict = true;
            break;
          }
  
          if(otherDiscounts[i].discountStartedDate.getTime() >= startedDate.getTime() && otherDiscounts[i].discountEndDate.getTime() <= endDate.getTime()){
            conflict = true;
            break;
          }
        }

        if(conflict == true){
          statusCode = 400;
          customError.comment = "discount conflict with another discount.";
          throw new Error();
        } else {
          const updateDiscount = await pos_Discounts.update(
            {
              discountStartedDate : req.body.discountStartedDate,
              discountEndDate : req.body.discountEndDate,
              discountRate : req.body.discountRate,
              discountPrice : req.body.discountPrice,
              isRate : req.body.isRate
            },
            {where : {id : req.body.discountId} , transaction : tr}
          );

          if(updateDiscount[0] >= 1){
            statusCode = 200;
            await tr.commit();
            return res.status(statusCode).json(customSuccess);
          }else {
            statusCode = 500;
            customError.comment = "record is not updated successfully";
            throw new Error();
          }
        }
      }
    }
  }catch(message){
    console.log(message);
    await tr.rollback();
    return res.status(statusCode).json(customError);
  }
}

// delete the discount
// checked
exports.deActivateDiscount = async (req , res , next) => {
  const tr = await sequelize.transaction();
  const customError = Object.assign({}, config.custom_error);
  const customSuccess = Object.assign({}, config.custom_success);
  let statusCode = 500;
  try{
    const branchId = req.body.branchId;
    const discountDetails = await pos_Discounts.update(
      {
        isActive : false
      },
      {where : {id : req.body.discountId , branchId : branchId} , transaction : tr}
    );

    if(discountDetails[0] >= 1){
      statusCode = 200;
      await tr.commit();
      return res.status(statusCode).json(customSuccess);
    } else {
      statusCode = 500;
      customError.comment = "discount is not deactivated.";
      throw new Error();
    }
  }catch(message){
    console.log(message);
    await tr.rollback();
    return res.status(statusCode).json(customError);
  }
}

exports.activateDiscount = async (req , res , next) => {
  const tr = await sequelize.transaction();
  const customError = Object.assign({}, config.custom_error);
  const customSuccess = Object.assign({}, config.custom_success);
  let statusCode = 500;

  try{
    const branchId = req.body.branchId;
    const discountDetails = await pos_Discounts.findOne({where : {id : req.body.discountId , branchId : branchId} , transaction : tr});
    if(isEmpty(discountDetails)){
      statusCode = 400;
      customError.comment = "no discount found for the discount id";
      throw new Error();
    } else {
      if(discountDetails.isActive == true){
        statusCode = 400;
        customError.comment = "discount is already active";
        throw new Error();
      } else {
        const otherDiscounts = await pos_Discounts.findAll({where : {inventoryItemsId : req.body.inventoryItemsId , branchId : branchId , id : {[Op.ne] : req.body.discountId} , isActive : true}});
        if(isEmpty(otherDiscounts)) {
          const activateDiscount = pos_Discounts.update(
            {
              isActive : true,
            },
            {where : {id : req.body.discountId , branchId : branchId}}
          );
          if(activateDiscount[0] >= 1){
            statusCode = 200;
            await tr.commit();
            return res.status(statusCode).json(customSuccess);
          } else {
            statusCode = 500;
            customError.comment = "discount is not activated successfully";
            throw new Error();
          }
        } else {
          let conflict = false;
          for(let i = 0; i < otherDiscounts.length; i++){
            if(otherDiscounts[i].discountStartedDate.getTime() <= discountDetails.discountStartedDate.getTime() && otherDiscounts[i].discountEndDate.getTime() >= discountDetails.discountEndDate.getTime()){
              conflict = true;
              break;
            }
    
            if(otherDiscounts[i].discountStartedDate.getTime() >= discountDetails.discountStartedDate.getTime() && otherDiscounts[i].discountStartedDate.getTime() <= discountDetails.discountEndDate.getTime()){
              conflict = true;
              break;
            }
    
            if(otherDiscounts[i].discountEndDate.getTime() >= discountDetails.discountStartedDate.getTime() && otherDiscounts[i].discountEndDate.getTime() <= discountDetails.discountEndDate.getTime()){
              conflict = true;
              break;
            }
    
            if(otherDiscounts[i].discountStartedDate.getTime() >= discountDetails.discountStartedDate.getTime() && otherDiscounts[i].discountEndDate.getTime() <= discountDetails.discountEndDate.getTime()){
              conflict = true;
              break;
            }
          }

          if(conflict == true){
            statusCode = 400;
            customError.comment = "can't active the discount because of a conflict";
            throw new Error();
          } else {
            const activeDiscount = await pos_Discounts.update(
              {
                isActive : true,
              },
              {where : {id : req.body.discountId , branchId : branchId}}
            );

            if(activeDiscount[0] >= 1){
              statusCode = 200;
              await tr.commit();
              return res.status(statusCode).json(customSuccess);
            } else {
              statusCode = 500;
              customError.comment = "discount activate is not successful";
              throw new Error();
            }
          }
        }
      }
    }
  }catch(message){
    console.log(message);
    await tr.rollback();
    return res.status(statusCode).json(customError);
  }
}









