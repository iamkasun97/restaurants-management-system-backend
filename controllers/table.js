const {sequelize} = require("../models");
const {owner} = require('../models');
const {users} = require('../models');
const {role} = require('../models');
const {hotel} = require('../models');
const {restaurant} = require('../models');
const {department} = require('../models');
const {item} = require('../models');
const {category_item} = require('../models');
const {promotion} = require('../models');
const {promotion_item} = require('../models');
const {customer} = require('../models');
const {order} = require('../models');
const {table} = require('../models');


const config = require("../config");
const isEmpty = require("is-empty");
const bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const moment = require('moment');
require("dotenv").config();

exports.dropdownDetailsForAddTable = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // owners enter a item scenario
        if(req.body.role.includes('owner')){
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }

            const hotels = await hotel.findAll({
                where : {ownerId : req.body.userId , hotelStatus : 1}
            });
            const restaurants = await restaurant.findAll({
                where : {ownerId : req.body.userId , restaurantStatus : 1}
            });
            const departments = await department.findAll({
                where: {ownerId : req.body.userId , departmentStatus : 1}
            });

            // add departments array to the response
            let departmentArray = [];
            departments.forEach(department => {
                departmentArray.push(department.dataValues);
            });

            // do the mapping
            let hotelAndRestaurantArray = [];
            hotels.forEach(hotel => {
                let restaurantArray = [];
                restaurants.forEach(restaurant => {
                    if(restaurant.dataValues.hotelId == hotel.dataValues.id){
                        restaurantArray.push(restaurant.dataValues);
                    }
                });
                let temp = {
                    id : hotel.dataValues.id,
                    hotelName : hotel.dataValues.hotelName,
                    hotelAddress : hotel.dataValues.hotelAddress,
                    statusComment : hotel.dataValues.statusComment,
                    ownerId : hotel.dataValues.ownerId,
                    restaurants : restaurantArray,
                }
                hotelAndRestaurantArray.push(temp);
            });
            let response = {
                hotels : hotelAndRestaurantArray,
                departments : departmentArray
            }
            // console.log(response);
            statusCode = 200;
            await tr.commit();
            customSuccess.data = response;
            return res.status(statusCode).json(customSuccess);

        }else if(req.body.role.includes('manager')){
            // manager employee scenario
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            // get the hotel , restaurant and department details for and employee
            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            const hotelE = await hotel.findOne({
                where : {id : searchResult.dataValues.hotelId , hotelStatus : 1}
            });
            const restaurantE = await restaurant.findOne({
                where : {id : searchResult.dataValues.restaurantId , restaurantStatus : 1}
            });
            const departmentE = await department.findOne({
                where: {id : searchResult.dataValues.departmentId , departmentStatus : 1}
            });

            let restaurantArray = [];
            let departmentArray = [];
            let hotelAndRestaurantArray = [];

            restaurantArray.push(restaurantE.dataValues);
            departmentArray.push(departmentE.dataValues);
            let temp = {
                id : hotelE.dataValues.id,
                hotelName : hotelE.dataValues.hotelName,
                hotelAddress : hotelE.dataValues.hotelAddress,
                statusComment : hotelE.dataValues.statusComment,
                ownerId : hotelE.dataValues.ownerId,
                restaurants : restaurantArray,
            }
            hotelAndRestaurantArray.push(temp);

            let response = {
                hotels : hotelAndRestaurantArray,
                departments : departmentArray
            }
            statusCode = 200;
            await tr.commit();
            customSuccess.data = response;
            return res.status(statusCode).json(customSuccess);
        }

        customError.comment = "error occurs!!";
        throw new Error();

    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// create a table
exports.addNewTable = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{

        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.tableName)){
            statusCode = 400;
            customError.comment = "table name can't be empty!!";
            throw new Error();
        }

        if(isEmpty(req.body.numberOfChairs)){
            statusCode = 400;
            customError.comment = "number of chairs can't be empty!!";
            throw new Error();
        }

        // mapping validation
        if(req.body.role.includes('owner')){
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });
            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
        }else if(req.body.role.includes('manager')){
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId || req.body.departmentId != searchResult.dataValues.departmentId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
        }

        // create a new table
        const newTable = await table.create({
            tableName : req.body.tableName,
            numberOfChairs : req.body.numberOfChairs,
            hotelId : req.body.hotelId,
            restaurantId : req.body.restaurantId,
            tableStatus : 1,
            enableStatus : 1
        },{transaction : tr});

        if(isEmpty(newTable)){
            customError.comment = "new table is not create properly!!";
            throw new Error(); 
        }

        // successfully created
        statusCode = 200;
        await tr.commit();
        customSuccess.data = newTable;
        return res.status(statusCode).json(customSuccess);

    }catch(error){
        console.log(error);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// get all the tables
exports.getTables = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{

        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        if(req.body.role.includes('owner')){
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });
            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

        }else if(req.body.role.includes('manager')){
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
        }

        // add the pagination concept here
        const pageAsNumber = Number.parseInt(req.query.page);
        const sizeAsNumber = Number.parseInt(req.query.size);

        let page = 0;
        if(!Number.isNaN(pageAsNumber) && pageAsNumber > 0){
            page = pageAsNumber;
        }

        let size = 5;
        if(!Number.isNaN(sizeAsNumber) && sizeAsNumber > 0){
            size = sizeAsNumber;
        }

        // get the search result
        const restaurantTables = await table.findAndCountAll({
            where : {hotelId : req.body.hotelId , restaurantId : req.body.restaurantId},
            limit : size,
            offset : page * size
        });

        statusCode = 200;
        await tr.commit();
        customSuccess.data = restaurantTables;
        return res.status(statusCode).json(customSuccess);

    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError); 
    }
}

// get only available tables in the restaurant
exports.getAvailableTables = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        if(req.body.role.includes('owner')){
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });
            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

        }else if(req.body.role.includes('manager')){
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
        }

        // get the search result
        const restaurantTables = await table.findAll({
            where : {hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , enableStatus : 1 , tableStatus : 1}
        });

        statusCode = 200;
        await tr.commit();
        customSuccess.data = restaurantTables;
        return res.status(statusCode).json(customSuccess);

    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError); 
    }
}



// update the table details
exports.updateTableDetails = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.tableId)){
            statusCode = 400;
            customError.comment = "table id is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.tableName)){
            statusCode = 400;
            customError.comment = "table name can't be empty!!";
            throw new Error();
        }

        if(isEmpty(req.body.numberOfChairs)){
            statusCode = 400;
            customError.comment = "number of chairs can't be empty!!";
            throw new Error();
        }

        if(req.body.role.includes('owner')){
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });
            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

        }else if(req.body.role.includes('manager')){
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
        }

        // update the table content
        const updatedResult = await table.update({
            tableName : req.body.tableName,
            numberOfChairs : req.body.numberOfChairs            
        } , {where : {id : req.body.tableId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId} , transaction : tr});

        if(updatedResult != 1 || updatedResult < 0){
            customError.comment = "System error!!";
            throw new Error();
        }

        // table details successfully updated
        statusCode = 200;
        await tr.commit();
        return res.status(statusCode).json(customSuccess);

    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError); 
    }
}


// change the status of the table
// disable and enable
exports.changeTableStatus = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.tableId)){
            statusCode = 400;
            customError.comment = "tableId is missing!!";
            throw new Error();
        }

        if(req.body.enableStatus === "" || req.body.enableStatus == undefined){
            statusCode = 400;
            customError.comment = "enable status is missing!!";
            throw new Error();
        }

        if(req.body.role.includes('owner')){
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });
            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

        }else if(req.body.role.includes('manager')){
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
        }
        // check there is a change or not
        const searchResult = await table.findOne({
            where : {id : req.body.tableId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
        });
        if(searchResult.dataValues.enableStatus == req.body.enableStatus){
            statusCode = 400;
                customError.comment = "bad request!!";
                throw new Error();
        }

        const updatedResult = await table.update({
            enableStatus : req.body.enableStatus
        },{where : {id : req.body.tableId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId} , transaction : tr});

        if(updatedResult != 1 || updatedResult < 0){
            customError.comment = "System error!!";
            throw new Error();
        }

        // table details successfully updated
        statusCode = 200;
        await tr.commit();
        return res.status(statusCode).json(customSuccess);
        
    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError); 
    }
}
