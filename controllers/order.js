const {sequelize} = require("../models");
const { Op } = require('sequelize');
const {owner} = require('../models');
const {users} = require('../models');
const {role} = require('../models');
const {hotel} = require('../models');
const {restaurant} = require('../models');
const {department} = require('../models');
const {item} = require('../models');
const {category_item} = require('../models');
const {promotion} = require('../models');
const {promotion_item} = require('../models');
const {customer} = require('../models');
const {order} = require('../models');
const {order_item} = require("../models");
const {price_cost} = require('../models');
const {tax} = require("../models");
const {table} = require("../models");
const {category} = require("../models");

const config = require("../config");
const isEmpty = require("is-empty");
const bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const moment = require('moment');
require("dotenv").config();


const imagePath = Object.assign({} , config.imagePath);


exports.dropdownDetailsForViewOrders = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // owners enter a item scenario
        if(req.body.role.includes('owner')){
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }

            const hotels = await hotel.findAll({
                where : {ownerId : req.body.userId , hotelStatus : 1}
            });
            const restaurants = await restaurant.findAll({
                where : {ownerId : req.body.userId , restaurantStatus : 1}
            });
            const departments = await department.findAll({
                where: {ownerId : req.body.userId , departmentStatus : 1}
            });
            const categories = await category.findAll({});

            // add departments array to the response
            let departmentArray = [];
            departments.forEach(department => {
                departmentArray.push(department.dataValues);
            });

            // do the mapping
            let hotelAndRestaurantArray = [];
            hotels.forEach(hotel => {
                let restaurantArray = [];
                restaurants.forEach(restaurant => {
                    let categoryArray = [];
                    categories.forEach(category => {
                        if(hotel.dataValues.id == category.dataValues.hotelId && restaurant.dataValues.id == category.dataValues.restaurantId){
                            categoryArray.push(category.dataValues);
                        }
                    });
                    if(restaurant.dataValues.hotelId == hotel.dataValues.id){
                        restaurantArray.push({
                            id : restaurant.dataValues.id,
                            restaurantName : restaurant.dataValues.restaurantName,
                            ownerId : restaurant.dataValues.ownerId,
                            hotelId : restaurant.dataValues.hotelId,
                            categories : categoryArray
                        });
                    }
                });
                let temp = {
                    id : hotel.dataValues.id,
                    hotelName : hotel.dataValues.hotelName,
                    hotelAddress : hotel.dataValues.hotelAddress,
                    statusComment : hotel.dataValues.statusComment,
                    ownerId : hotel.dataValues.ownerId,
                    restaurants : restaurantArray,
                }
                hotelAndRestaurantArray.push(temp);
            });
            let response = {
                hotels : hotelAndRestaurantArray,
                departments : departmentArray
            }
            // console.log(response);
            statusCode = 200;
            await tr.commit();
            customSuccess.data = response;
            return res.status(statusCode).json(customSuccess);

        }else if(req.body.role.includes('manager')){
            // manager employee scenario
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            // get the hotel , restaurant and department details for and employee
            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            const hotelE = await hotel.findOne({
                where : {id : searchResult.dataValues.hotelId , hotelStatus : 1}
            });
            const restaurantE = await restaurant.findOne({
                where : {id : searchResult.dataValues.restaurantId , restaurantStatus : 1}
            });
            const departmentE = await department.findOne({
                where: {id : searchResult.dataValues.departmentId , departmentStatus : 1}
            });
            const categoryE = await category.findAll({
                where : {hotelId : searchResult.dataValues.hotelId , restaurantId : searchResult.dataValues.restaurantId}
            });

            let restaurantArray = [];
            let departmentArray = [];
            let categoryArray = [];
            let hotelAndRestaurantArray = [];
            
            categoryE.forEach(e => {
                categoryArray.push(e.dataValues);
            });

            let restaurantAndCategories = {
                id : restaurantE.dataValues.id,
                restaurantName : restaurantE.dataValues.restaurantName,
                ownerId : restaurantE.dataValues.ownerId,
                hotelId : restaurantE.dataValues.hotelId,
                categories : categoryArray
            }

            departmentArray.push(departmentE.dataValues);
            restaurantArray.push(restaurantAndCategories);

            let temp = {
                id : hotelE.dataValues.id,
                hotelName : hotelE.dataValues.hotelName,
                hotelAddress : hotelE.dataValues.hotelAddress,
                statusComment : hotelE.dataValues.statusComment,
                ownerId : hotelE.dataValues.ownerId,
                restaurants : restaurantArray,
            }
            hotelAndRestaurantArray.push(temp);

            let response = {
                hotels : hotelAndRestaurantArray,
                departments : departmentArray
            }
            statusCode = 200;
            await tr.commit();
            customSuccess.data = response;
            return res.status(statusCode).json(customSuccess);
        }

        customError.comment = "error occurs!!";
        throw new Error();
    }catch(error){
        console.log(error);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

exports.getItemsAndPromotionsUnderCategory = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }
        if(isEmpty(req.body.categoryId)){
            statusCode = 400;
            customError.comment = "categoryId is missing!!";
            throw new Error();
        }

        if(req.body.role.includes('owner')){
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });
            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

        }else if(req.body.role.includes('manager') || req.body.role.includes('waiter')){
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
        }
        // get tax rate 
        let taxDetails = 0
        let taxUpdatedAt = '2023-04-25 07:49:22';
        const searchResult = await tax.findOne({
            where : {hotelId : req.body.hotelId, restaurantId : req.body.restaurantId, ownerId : req.body.userId}
        });
        if(!isEmpty(searchResult)){
            taxDetails = searchResult.dataValues.taxRate;
            taxUpdatedAt = searchResult.dataValues.updatedAt;
        }

        let allKeywords = [];

        // check the category id in category_item table
        const [result , metadata] = await sequelize.query(
            `SELECT * FROM category_items ci
            JOIN items i
            ON i.id = ci.itemId
            WHERE ci.categoryId = ${req.body.categoryId} AND i.hotelId = ${req.body.hotelId} AND i.restaurantId = ${req.body.restaurantId} AND i.availability = 1
            `
        );
        // get updated time for refresh for the latest prices
        let lastUpdatedTime = result[0].updatedAt;

        let responseItems = [];
        let time = new Date();
        let indiaTime = Number(time.getTime())+19800000;
        result.forEach(e => {
            JSON.parse(e.serveTime).forEach(t => {
                let start = new Date(`${moment().format('YYYY-MM-DD').toString()}T${t.startTime}.0Z`).getTime();
                let end = new Date(`${moment().format('YYYY-MM-DD').toString()}T${t.endTime}.0Z`).getTime();
                if(Number(start)<Number(indiaTime) && Number(end)>Number(indiaTime)){
                    // check responseItems already hold the item or not
                    if(responseItems.find(it => it.id == e.id)){}else {
                        allKeywords = [...allKeywords , ...JSON.parse(e.keywords)];
                        let temp = {
                            id : e.id,
                            itemName : e.itemName,
                            itemDescription : e.itemDescription,
                            keywords : JSON.parse(e.keywords),
                            unit : e.unit,
                            unitAmount : e.unitAmount,
                            unitCost : e.unitCost,
                            unitPrice : e.unitPrice,
                            serviceCharge : e.serviceCharge,
                            serviceChargeType : e.serviceChargeType,
                            avgPrepareTime : e.avgPrepareTime,
                            serveTime : JSON.parse(e.serveTime),
                            availability : e.availability,
                            image : `${imagePath.path}${e.image}`,
                            hotelId : e.hotelId,
                            restaurantId : e.restaurantId,
                            departmentId : e.departmentId
                        }
                        responseItems.push(temp);
                        if(Number(new Date(lastUpdatedTime).getTime()) < Number(new Date(e.updatedAt).getTime())){
                            lastUpdatedTime = e.updatedAt;
                        } 
                        
                    }
                }
            });
        });
        

        // check the category id in category_item table
        const [result1 , metadata1] = await sequelize.query(
            `SELECT DISTINCT i.id FROM category_items ci
            JOIN items i
            ON i.id = ci.itemId
            WHERE ci.categoryId = ${req.body.categoryId} AND i.hotelId = ${req.body.hotelId} AND i.restaurantId = ${req.body.restaurantId} AND i.availability = 1
            `
        );

        // get the item id array
        let itemIdArray = result1.map(e => {
            return e.id
        });

        let promotionResult = await promotion_item.findAll({
            where : {itemId : itemIdArray}
        });

        let promotionIdArray = promotionResult.map(e => {
            return e.dataValues.promotionId
        });

        // distict the array
        let distictPromotionIdArray = [...new Set(promotionIdArray)];
        
        // get all promotions details
        const categoryPromotions = await promotion.findAll({
            where : {id : distictPromotionIdArray , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
        });
        
        // have to do the date and time validation
        let responsePromotions = [];
        // let time = new Date();
        // let indiaTime = Number(time.getTime())+19800000;
        categoryPromotions.forEach(e => {
            let startDate = new Date(`${moment(e.dataValues.fromDate).format('YYYY-MM-DD').toString()}T00:00:00.0Z`).getTime();
            let finishDate = new Date(`${moment(e.dataValues.toDate).format('YYYY-MM-DD').toString()}T00:00:00.0Z`).getTime();

            if(Number(startDate) < Number(indiaTime) && Number(indiaTime) < Number(finishDate)){
                JSON.parse(e.dataValues.serveTime).forEach(t => {
                    let start = new Date(`${moment().format('YYYY-MM-DD').toString()}T${t.startTime}.0Z`).getTime();
                    let end = new Date(`${moment().format('YYYY-MM-DD').toString()}T${t.endTime}.0Z`).getTime();
                        if(Number(start)<Number(indiaTime) && Number(end)>Number(indiaTime)){
                            // check responsePromotions already hold the item or not
                            if(responsePromotions.find(it => it.id == e.dataValues.id)){}else {
                                allKeywords = [...allKeywords , ...JSON.parse(e.dataValues.keywords)];
                                let temp = {
                                    id : e.dataValues.id,
                                    promotionName : e.dataValues.promotionName,
                                    promotionDescription : e.dataValues.promotionDescription,
                                    keywords : JSON.parse(e.dataValues.keywords),
                                    unitCost : e.dataValues.unitCost,
                                    unitPrice : e.dataValues.unitPrice,
                                    serviceCharge : e.dataValues.serviceCharge,
                                    serviceChargeType : e.dataValues.serviceChargeType,
                                    avgPrepareTime : e.dataValues.avgPrepareTime,
                                    serveTime : JSON.parse(e.dataValues.serveTime),
                                    availability : e.dataValues.availability,
                                    image : `${imagePath.path}${e.dataValues.image}`,
                                    hotelId : e.dataValues.hotelId,
                                    restaurantId : e.dataValues.restaurantId,
                                    fromDate : e.dataValues.fromDate,
                                    toDate : e.dataValues.toDate
                                }
                                responsePromotions.push(temp);
                                if(Number(new Date(lastUpdatedTime).getTime()) < Number(new Date(e.dataValues.updatedAt).getTime())){                   
                                    lastUpdatedTime = e.dataValues.updatedAt;
                                }
                            }
                        }     
                });
            }
        });

        // distinct an array
        allKeywords = [...new Set(allKeywords)];

        let finalResponse = {
            items : responseItems,
            promotions : responsePromotions,
            allKeywords : allKeywords,
            tax : taxDetails,
            updatedAt : lastUpdatedTime,
            taxUpdatedAt : taxUpdatedAt
        };

        statusCode = 200;
        await tr.commit();
        customSuccess.data = finalResponse;
        return res.status(statusCode).json(customSuccess);

    }catch(error){
        console.log(error);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// place and order
exports.placeAnOrder = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }
        
        let userType = '';
        let userId = '';
        // authorization
        if(req.body.role.includes('owner')){
            userType = 'owner';
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }
            userId = req.body.userId;

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });
            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

        }else if(req.body.role.includes('manager') || req.body.role.includes('waiter')){
            userType = 'employee';
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }
            userId = req.body.employeeId;

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
        }

        // email validation if its not empty

        if(isEmpty(req.body.mobile)){
            statusCode = 400;
            customError.comment = "customer mobile number can't be empty!!";
            throw new Error();
        }        

        // register a customer
        const searchCustomer = await customer.findOne({
            where : {mobile : req.body.mobile , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
        });

        let customerDetails = '';

        if(isEmpty(searchCustomer)){
            // create a new user under a restaurant and a hotel
            const newCustomer = await customer.create({
                customerName : req.body.customerName,
                mobile : req.body.mobile,
                email : req.body.email,
                hotelId : req.body.hotelId,
                restaurantId : req.body.restaurantId
            },{transaction : tr});

            if(isEmpty(newCustomer)){
                customError.comment = "new customer is not created!!";
                throw new Error();
            }

            customerDetails = newCustomer;
        }else {
            customerDetails = searchCustomer;
        }

        // place an order
        const newOrder = await order.create({
            orderDateTime : new Date(),
            paymentStatus : 0,
            customerId : customerDetails.dataValues.id,
            userId : userId,
            userType : userType,
            hotelId : req.body.hotelId,
            restaurantId : req.body.restaurantId
        }, {transaction : tr});

        if(isEmpty(newOrder)){
            customError.comment = "new order is not created!!";
            throw new Error();
        }else {
            // create a entry in order_table 
            // for future 


        }

        let sortedItemsArray = req.body.itemsList.items?.sort((a , b) => a.itemId - b.itemId)
        let sortedPromotionsArray = req.body.itemsList.promotions?.sort((a , b) => a.promotionId - b.promotionId);

        let promotionsIdArray = sortedPromotionsArray.map(e => e.promotionId);

        // get item ids relavent to promotions
        let promotionItemsIds = await promotion_item.findAll({
            where : {promotionId : promotionsIdArray}
        });

        let promotionItemDetails = [];
        promotionItemsIds.forEach(pi => {
            sortedPromotionsArray.forEach(p => {
                if(pi.dataValues.promotionId == p.promotionId){
                    promotionItemDetails.push({
                        itemId : pi.dataValues.itemId,
                        promotionId : pi.dataValues.promotionId,
                        quantity : Number(pi.dataValues.quantity)*Number(p.quantity),
                        diningStatus : p.diningStatus
                    });
                }
            });
        });
        
        let allItemsArray = [...sortedItemsArray , ...promotionItemDetails];
        let allItemIds = allItemsArray.map(e => e.itemId);

        // get item details
        let itemDetails = await item.findAll({
            where : {id : allItemIds , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
        });

        let finalItemDetailsArray = [];
        let itemsOnlyTotal = 0;
        let itemsOnlyCost = 0;
        let itemsOnlyServiceCharge = 0;
        itemDetails.forEach(it => {
            allItemsArray.forEach(i => {
                if(i.itemId == it.dataValues.id){
                    finalItemDetailsArray.push({
                        itemId : i.itemId,
                        promotionId : i.promotionId ? i.promotionId : null,
                        itemName : it.dataValues.itemName,
                        unit : it.dataValues.unit,
                        unitAmount : it.dataValues.unitAmount,
                        avgPrepareTime : it.dataValues.avgPrepareTime,
                        diningStatus : i.diningStatus,
                        unitCost : i.promotionId ? 0 : it.dataValues.unitCost,
                        unitPrice : i.promotionId ? 0 : it.dataValues.unitPrice,
                        quantity : i.quantity,
                        processingStatus : 0,
                        serviceCharge : it.dataValues.serviceCharge,
                        serviceChargeType : it.dataValues.serviceChargeType,
                        hotelId : it.dataValues.hotelId,
                        restaurantId : it.dataValues.restaurantId,
                        departmentId : it.dataValues.departmentId,
                        orderId : newOrder.id
                    });

                    // change the results
                    if(isEmpty(i.promotionId)){
                        itemsOnlyTotal += Number(it.dataValues.unitPrice);
                        itemsOnlyCost += Number(it.dataValues.unitCost);
                        if(it.dataValues.serviceChargeType == 0){
                            itemsOnlyServiceCharge += it.dataValues.serviceCharge;
                        }else if(it.dataValues.serviceChargeType == 1){
                            itemsOnlyServiceCharge = itemsOnlyServiceCharge + (it.dataValues.unitPrice*it.dataValues.serviceCharge)/100;
                        }
                    }
                }
            })
        });

        // get promotion details
        let promotionDetails = await promotion.findAll({
            where : {id : promotionsIdArray , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
        });

        // change the results
        let promotionOnlyTotal = 0;
        let promotionOnlyCost = 0;
        let promotionOnlyServiceCharge = 0;
        promotionDetails.forEach(p => {
            sortedPromotionsArray.forEach(sp => {
                if(p.dataValues.id == sp.promotionId){
                    promotionOnlyTotal += Number(p.dataValues.unitPrice);
                    promotionOnlyCost += Number(p.dataValues.unitCost);
                    if(p.dataValues.serviceChargeType == 0){
                        promotionOnlyServiceCharge += p.dataValues.serviceCharge;
                    }else if(p.dataValues.serviceChargeType == 1){
                        promotionOnlyServiceCharge = promotionOnlyServiceCharge + (p.dataValues.unitPrice*p.dataValues.serviceCharge)/100;
                    }
                }
            });
        });

        // get total price and total service charge
        let totalPrice = itemsOnlyTotal + promotionOnlyTotal;
        let totalCost = itemsOnlyCost + promotionOnlyCost;
        let totalServiceCharge = itemsOnlyServiceCharge + promotionOnlyServiceCharge;
        let profit = totalPrice + totalServiceCharge - totalCost;

        // put a database entry in a new table
        const newPriceCostEntry = await price_cost.create({
            orderId : newOrder.id,
            totalCost : totalCost,
            totalPrice : totalPrice,
            totalServiceCharge : totalServiceCharge,
            profit : profit,
            hotelId : req.body.hotelId,
            restaurantId : req.body.restaurantId
        }, {transaction : tr});

        if(isEmpty(newPriceCostEntry)){
            customError.comment = "price and cost entry is not created!!";
            throw new Error();
        }


        // create a bulk entry for the order_item_list
        const newOrderItemListEntry = await order_item.bulkCreate(finalItemDetailsArray , {transaction : tr});

        if(isEmpty(newOrderItemListEntry)){
            customError.comment = "new order item entry is not created!!";
            throw new Error();
        }

        statusCode = 200;
        await tr.commit();
        return res.status(statusCode).json(customSuccess);

    }catch(error){
        console.log(error);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// new place an order
exports.placeAnOrderNew = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;
    try{
        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }
        
        let userType = '';
        let userId = '';
        // authorization
        if(req.body.role.includes('owner')){
            userType = 'owner';
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }
            userId = req.body.userId;

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });
            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

        }else if(req.body.role.includes('manager') || req.body.role.includes('waiter')){
            userType = 'employee';
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }
            userId = req.body.employeeId;

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
        }

        // email validation if its not empty

        if(isEmpty(req.body.mobile)){
            statusCode = 400;
            customError.comment = "customer mobile number can't be empty!!";
            throw new Error();
        }        

        // register a customer
        const searchCustomer = await customer.findOne({
            where : {mobile : req.body.mobile , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
        });

        let customerDetails = '';

        if(isEmpty(searchCustomer)){
            // create a new user under a restaurant and a hotel
            const newCustomer = await customer.create({
                customerName : req.body.customerName,
                mobile : req.body.mobile,
                email : req.body.email,
                hotelId : req.body.hotelId,
                restaurantId : req.body.restaurantId
            },{transaction : tr});

            if(isEmpty(newCustomer)){
                customError.comment = "new customer is not created!!";
                throw new Error();
            }

            customerDetails = newCustomer;
        }else {
            customerDetails = searchCustomer;
        }

        // check the table status if 0 or 1
        // and also check the table enabled status 0 or 1
        // do the validation if its only order has dine in items
        let isDiningItems = req.body.itemsList.items?.find(e => e.diningStatus == 1);
        let isDiningPromotions = req.body.itemsList.promotions?.find(e => e.diningStatus == 1);

        if(!isEmpty(isDiningItems) || !isEmpty(isDiningPromotions)){
            if(isEmpty(req.body.tableId)){
                statusCode = 400;
                customError.comment = "order has dine in items tableId can't be empty!!";
                throw new Error();
            }

            const tableStatusSearch = await table.findOne({
                where : {id : req.body.tableId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
            });
            if(tableStatusSearch.dataValues.tableStatus == 0 || tableStatusSearch.dataValues.enableStatus == 0){
                statusCode = 400;
                customError.comment = "table is not available!!";
                throw new Error();
            } 

            // change the table status to zero
            // table status zero means table is booked
            const updatedResult =  await table.update({
                tableStatus : 0
            },{where : {id : req.body.tableId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId} , transaction : tr});

            if(updatedResult != 1 || updatedResult < 0){
                statusCode = 500;
                customError.comment = "System error!!";
                throw new Error();
            }
        }
        
        // get tax rate
       const taxRate = await tax.findOne({
            where : {hotelId : req.body.hotelId, restaurantId : req.body.restaurantId}
       });

        // place an order
        // tableId can be null
        const newOrder = await order.create({
            orderDateTime : new Date(),
            paymentStatus : 0,
            customerId : customerDetails.dataValues.id,
            userId : userId,
            userType : userType,
            serviceCharge : req.body.serviceCharge,
            taxRate : isEmpty(taxRate) ? 0 : taxRate.dataValues.taxRate,
            cancelStatus : 0,
            tableId : req.body.tableId == "" ? null : req.body.tableId,
            hotelId : req.body.hotelId,
            restaurantId : req.body.restaurantId
        }, {transaction : tr});

        if(isEmpty(newOrder)){
            customError.comment = "new order is not created!!";
            throw new Error();
        }
 
        // one order assign to one table
        // change the table status to zero
        let sortedItemsArray = req.body.itemsList.items?.sort((a , b) => a.itemId - b.itemId);
        let sortedPromotionsArray = req.body.itemsList.promotions?.sort((a , b) => a.promotionId - b.promotionId);

        // get item ids
        let itemIdArray = sortedItemsArray.map(e => e.itemId);
        // get promotion ids
        let promotionIdArray = sortedPromotionsArray.map(e => e.promotionId);

        // get item and promotion details
        let itemDetailsArray = await item.findAll({
            where : {id : itemIdArray , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
        });

        let promotionDetailsArray = await promotion.findAll({
            where : {id : promotionIdArray , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
        });

        let results = [];
        if(!isEmpty(sortedPromotionsArray)){
            // get promotion item details
        let query = `
            SELECT * FROM promotion_items pi
            JOIN items i
            ON pi.itemId = i.id
            WHERE pi.promotionId IN (${promotionIdArray.map(() => '?').join(',')});
            `;

            results = await sequelize.query(query, {
                replacements: promotionIdArray,
                type: sequelize.QueryTypes.SELECT
            });
        }

        let promotionList = [];
        sortedPromotionsArray.forEach(p => {
            let promotionItemList = [];
            results.forEach(pi => {
                if(p.promotionId == pi.promotionId) {
                    promotionItemList.push({
                        itemId : pi.itemId,
                        promotionId : pi.promotionId,
                        itemName : pi.itemName,
                        quantity : pi.quantity,
                        unit : pi.unit,
                        unitAmount : pi.unitAmount,
                        departmentId : pi.departmentId,
                        restaurantId : pi.restaurantId,
                        hotelId : pi.hotelId
                    });
                }
            });

            promotionList.push({
                promotionId : p.promotionId,
                itemList : promotionItemList,
                quantity : p.quantity,
                diningStatus : p.diningStatus,
                processingStatus : p.processingStatus
            });
        });

        let totalTax = 0;
        let totalPayable = 0;
        
        let promotionDetailList = [];
       // pormotion details before enter to the database
       promotionDetailsArray.forEach(pd => {
            promotionList.forEach(p => {  
                if(pd.id == p.promotionId){   
                    totalPayable += (Number(pd.unitPrice))*(Number(p.quantity));
                    promotionDetailList.push({              
                        promotionId : p.promotionId,
                        promotionName : pd.promotionName,
                        itemList : JSON.stringify(p.itemList),
                        quantity : p.quantity,
                        unitCost : pd.unitCost,
                        unitPrice : pd.unitPrice,
                        serviceCharge : pd.serviceCharge,
                        serviceChargeType : pd.serviceChargeType,
                        avgPrepareTime : pd.avgPrepareTime,
                        diningStatus : p.diningStatus,
                        processingStatus : p.processingStatus,
                        orderId : newOrder.id,
                        hotelId : pd.hotelId,
                        restaurantId : pd.restaurantId
                    })
                }
            })
       }); 
       
       
       // do the induvidual items part
       let itemDetailList = [];
       itemDetailsArray.forEach(id => {
            sortedItemsArray.forEach(i => {
                if(i.itemId == id.id){
                    totalPayable += (Number(id.unitPrice))*(Number(i.quantity));
                    itemDetailList.push({
                        itemId : id.id,
                        itemName : id.itemName,
                        quantity : i.quantity,
                        unit : id.unit,
                        unitAmount : id.unitAmount,
                        unitCost : id.unitCost,
                        unitPrice : id.unitPrice,
                        serviceCharge : id.serviceCharge,
                        serviceChargeType : id.serviceChargeType,
                        avgPrepareTime : id.avgPrepareTime,
                        diningStatus : i.diningStatus,
                        processingStatus : i.processingStatus,
                        orderId : newOrder.id,
                        departmentId : id.departmentId,
                        restaurantId : id.restaurantId,
                        hotelId : id.hotelId
                    });
                }
            });
       });

        if(!isEmpty(taxRate)){
            totalTax = totalPayable * (taxRate.dataValues.taxRate / 100);
        }

       // check tax price also in here
       // total payable add the service charge
       totalPayable += (req.body.serviceCharge+totalTax);
       
       // total tax prices same or not in the front and back
       if(parseFloat(totalTax.toFixed(2)) != parseFloat(req.body.totalTax.toFixed(2))){
            statusCode = 400;
            customError.comment = "totalTax is not same please refresh the page to get latest tax rates!!";
            throw new Error();
       }

       // check the totalPayable from front end and back end are similar or not
       if(parseFloat(totalPayable.toFixed(2)) != parseFloat(req.body.totalPayable.toFixed(2))){
            statusCode = 400;
            customError.comment = "totalPayable is not same plese refresh the page to get latest prices!!";
            throw new Error();
       }
       
       let finalArray = [...itemDetailList , ...promotionDetailList];

       // do the bulk insertion 
       const newOrderDetails = await order_item.bulkCreate(finalArray , {transaction : tr});
       if(isEmpty(newOrderDetails)){
        customError.comment = "new category_item not created!!";
        throw new Error();
        }else {

            statusCode = 200;
            await tr.commit();
            customSuccess.data = newOrder;
            return res.status(statusCode).json(customSuccess);
        }

    }catch(error){
        console.log(error);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// view orders for a specific user
exports.viewOrders = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        let userType = '';
        let userId = '';
        // authorization
        if(req.body.role.includes('owner')){
            userType = 'owner';
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }
            userId = req.body.userId;

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });
            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

        }else if(req.body.role.includes('manager') || req.body.role.includes('waiter')){
            userType = 'employee';
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }
            userId = req.body.employeeId;

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });

            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
        }



        // get number of orders first
        // then create the response seperately
        const orderList = await order.findAll({
            where : {userId : userId , userType : userType , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , paymentStatus : 0 , cancelStatus : 0}
        });    

        // get customer details also
        const customerIds = orderList.map(e => e.customerId);
        console.log(customerIds);

        // get customer Details 
        const customerDetails = await customer.findAll({
            where : {id : customerIds , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
        });

        let response = [];
        orderList.forEach(e => {
            let tempCustomerDetails = customerDetails.find(c => c.id == e.customerId);
            response.push({
                customer : tempCustomerDetails,
                orderDetails : e
            });
        });

        statusCode = 200;
        await tr.commit();
        customSuccess.data = response;
        return res.status(statusCode).json(customSuccess);
    }catch(error){
        console.log(error);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// filter orders by date
exports.getOrderDateWise = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.fromDate)){
            statusCode = 400;
            customError.comment = "from date is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.toDate)){
            statusCode = 400;
            customError.comment = "to date is missing!!";
            throw new Error();
        }

        let userType = '';
        let userId = '';
        // authorization
        if(req.body.role.includes('owner')){
            userType = 'owner';
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }
            userId = req.body.userId;

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });
            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

        }else if(req.body.role.includes('manager') || req.body.role.includes('waiter')){
            userType = 'employee';
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }
            userId = req.body.employeeId;

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            
            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
        }

        let orderList = []
        // if mobile is not empty then filter from mobile number also
        if(isEmpty(req.body.mobile)){
            orderList = await order.findAll({
                where : {userId : userId , userType : userType , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , paymentStatus : 0 , cancelStatus : 0 , orderDateTime : {[Op.between]: [new Date(req.body.fromDate) , new Date(req.body.toDate)]}}
            }); 
        }else{
            const customerDetails = await customer.findOne({
                where : {mobile : req.body.mobile , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
            });
            if(isEmpty(customerDetails)){
                statusCode = 400;
                customError.comment = "no customer found for the mobie number!!";
                throw new Error();
            }
            orderList = await order.findAll({
                where : {customerId : customerDetails.dataValues.id, userId : userId , userType : userType , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , paymentStatus : 0 , cancelStatus : 0 , orderDateTime : {[Op.between]: [new Date(req.body.fromDate) , new Date(req.body.toDate)]}}
            });
        }

        statusCode = 200;
        await tr.commit();
        customSuccess.data = orderList;
        return res.status(statusCode).json(customSuccess);

    }catch(error){
        console.log(error);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// view order items for an order id
exports.viewOrderItems = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.orderId)){
            statusCode = 400;
            customError.comment = "orderId is missing!!";
            throw new Error();
        }

        // get the tax details
        const taxDetails = await tax.findOne({
            where : {hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
        })
        if(!isEmpty(taxDetails)){
            taxRate = taxDetails.dataValues.taxRate
        }

        const orderDetails = await order.findOne({
            where : {id : req.body.orderId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
        });

        if(isEmpty(orderDetails)){
            statusCode = 400;
            customError.comment = "order details not found!!";
            throw new Error();
        }

        // get the customer details also
        const customerDetails = await customer.findOne({
            where : {id : orderDetails.dataValues.customerId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
        });
        if(isEmpty(customerDetails)){
            statusCode = 400;
            customError.comment = "customer details not found!!";
            throw new Error();
        }

        // get order items for an specific orderId
        const orderItemsList = await order_item.findAll({
            where : {orderId : req.body.orderId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
        });

        let orderItemsListWithType = [];

        orderItemsList.forEach(e => {
            let temp = {
                id : e.dataValues.id,
                type : e.dataValues.itemId ? 0 : 1,
                itemId : e.dataValues.itemId,
                itemName : e.dataValues.itemName,
                promotionId : e.dataValues.promotionId,
                promotionName : e.dataValues.promotionName,
                itemList : e.dataValues.itemList,
                unit : e.dataValues.unit,
                unitAmount : e.dataValues.unitAmount,
                avgPrepareTime : e.dataValues.avgPrepareTime,
                diningStatus : e.dataValues.diningStatus,
                unitCost : e.dataValues.unitCost,
                unitPrice : e.dataValues.unitPrice,
                quantity : e.dataValues.quantity,
                processingStatus : e.dataValues.processingStatus,
                serviceCharge : e.dataValues.serviceCharge,
                serviceChargeType : e.dataValues.serviceChargeType,
                orderId : e.dataValues.orderId,
                hotelId : e.dataValues.hotelId,
                restaurantId : e.dataValues.restaurantId,
                departmentId : e.dataValues.departmentId
            }
            orderItemsListWithType.push(temp);
        });

        let response = {
            tax : orderDetails.dataValues.taxRate,
            customer : customerDetails,
            orderDetails : orderDetails,
            orders : orderItemsListWithType
        }

        statusCode = 200;
        await tr.commit();
        customSuccess.data = response;
        return res.status(statusCode).json(customSuccess);
    }catch(error){
        console.log(error);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// edit an order 
exports.editAnOrder = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.orderId)){
            statusCode = 400;
            customError.comment = "orderId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.mobile)){
            statusCode = 400;
            customError.comment = "mobile number is missing!!";
            throw new Error();
        }

        let userType = '';
        let userId = '';
        let tableId = '';
        // authorization
        if(req.body.role.includes('owner')){
            userType = 'owner';
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }
            userId = req.body.userId;

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });
            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

        }else if(req.body.role.includes('manager') || req.body.role.includes('waiter')){
            userType = 'employee';
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }
            userId = req.body.employeeId;

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
        }

        // get the current order details
        const orderDetails = await order.findOne({
            where : {id : req.body.orderId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
        });

        // check the order id with the customer details
        const customerDetals = await customer.findOne({
            where : {id : orderDetails.dataValues.customerId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
        });

        if(isEmpty(customerDetals)){
            statusCode = 400;
            customError.comment = "customer not found!!";
            throw new Error();
        }

        if(customerDetals.dataValues.mobile != req.body.mobile){
            statusCode = 400;
            customError.comment = "customer and order id is not map each other!!";
            throw new Error();
        }

        // update the customer details
        const updatedResult = await customer.update({
            customerName : req.body.customerName,
            email : req.body.email,
        },{where : {mobile : req.body.mobile , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId} , transaction : tr});

        if(updatedResult != 1 || updatedResult < 0){
            statusCode = 500;
            customError.comment = "System error!!";
            throw new Error();
        }

        // check the table status if 0 or 1
        // and also check the table enabled status 0 or 1
        // do the validation if its only order has dine in items

        let isDiningItems = req.body.itemsList.items?.find(e => e.diningStatus == 1);
        let isDiningPromotions = req.body.itemsList.promotions?.find(e => e.diningStatus == 1);

        if(!isEmpty(isDiningItems) || !isEmpty(isDiningPromotions)){
            if(isEmpty(req.body.tableId)){
                statusCode = 400;
                customError.comment = "order has dine in items tableId can't be empty!!";
                throw new Error();
            }

            // check the table is same or not 
            if(orderDetails.dataValues.tableId != request.body.tableId){
                if(isEmpty(orderDetails.dataValues.tableId) && isEmpty(req.body.tableId)){}
                else {
                    if(!isEmpty(orderDetails.dataValues.tableId)){
                        // release the table
                        const updateTable = await table.update({
                            tableStatus : 1
                        },{where : {id : orderDetails.dataValues.tableId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId} , transaction : tr});

                        if(updateTable != 1 || updateTable < 0){
                            statusCode = 500;
                            customError.comment = "System error!!";
                            throw new Error();
                        }
                    }
                    
                    if(!isEmpty(req.body.tableId)){
                        // get the new table details 
                        const tableDetails = await table.findOne({
                            where : {id : req.body.tableId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
                        });
                        // if table is not available throw an error
                        // if available assign value to tableId
                        if(isEmpty(tableDetails) || tableDetails.dataValues.tableStatus == 0 || tableDetails.dataValues.enableStatus == 0){
                            statusCode = 400;
                            customError.comment = "table is not available!!";
                            throw new Error();
                        }

                        // change the table status
                        // booked the new table id
                        const updateTable1 = await table.update({
                            tableStatus : 0
                        },{where : {id : req.body.tabelId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId} , transaction : tr});

                        if(updateTable1 != 1 || updateTable1 < 0){
                            statusCode = 500;
                            customError.comment = "System error!!";
                            throw new Error();
                        }

                        tableId = req.body.tabelId;
                    }
                }
            }
        }
        
        // update the order details
        const updateOrder = await order.update({
            serviceCharge : req.body.serviceCharge
        },{where : {id : req.body.orderId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId} , transaction : tr});

        if(updateOrder != 1 || updateOrder < 0){
            statusCode = 500;
            customError.comment = "System error!!";
            throw new Error();
        }
        let sortedItemsArray = req.body.itemsList.items.sort((a , b) => a.itemId - b.itemId);
        let sortedPromotionsArray = req.body.itemsList.promotions.sort((a , b) => a.promotionId - b.promotionId);
        
        // get the current order items
        const allOrderItems = await order_item.findAll({
            where : {orderId : req.body.orderId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
        });
        let orderItems = allOrderItems.filter(e => e.itemId != null);
        let orderPromotions = allOrderItems.filter(e => e.promotionId != null);

        let totalPayable = 0;
        let totalTax = 0;
        let taxRate = orderDetails.dataValues.taxRate;

        // update the order items that are already exist in the list
        for(let i=0;i<sortedItemsArray.length;i++){
            let temp = orderItems.find(e => e.dataValues.id == sortedItemsArray[i].id);
            if(!isEmpty(temp)) {
                // update the order data in an existing item
                // also have to check items processing status
                if(temp.dataValues.processingStatus == 1 || temp.dataValues.processingStatus == 2){
                    statusCode = 400;
                    customError.comment = "please refresh the page to get latest status!!";
                    throw new Error();
                }
                let updateOrderItem = await order_item.update({
                    quantity : sortedItemsArray[i].quantity,
                    diningStatus : sortedItemsArray[i].diningStatus
                },{where : {id : temp.dataValues.id , orderId : req.body.orderId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId},transaction : tr});
                
                if(updateOrderItem != 1 || updateOrderItem < 0){
                    statusCode = 500;
                    customError.comment = "System error!!";
                    throw new Error();
                }
                totalPayable += (Number(temp.unitPrice)*Number(sortedItemsArray[i].quantity));

            }else {
                // new item added to the list
                // get item details
                let newItemDetails = await item.findOne({
                    where : {id : sortedItemsArray[i].itemId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , availability : 1}
                });

                if(isEmpty(newItemDetails)){
                    statusCode = 400;
                    customError.comment = "enter a invalid item!!";
                    throw new Error();
                }

                // enter a new order item record
                let newOrderItemRecord = await order_item.create({
                    itemId : sortedItemsArray[i].itemId,
                    itemName : newItemDetails.dataValues.itemName,
                    unit : newItemDetails.dataValues.unit,
                    unitAmount : newItemDetails.dataValues.unitAmount,
                    avgPrepareTime : newItemDetails.dataValues.avgPrepareTime,
                    diningStatus : sortedItemsArray[i].diningStatus,
                    unitCost : newItemDetails.dataValues.unitCost,
                    unitPrice : newItemDetails.dataValues.unitPrice,
                    quantity : sortedItemsArray[i].quantity,
                    processingStatus : 0,
                    serviceCharge : newItemDetails.dataValues.serviceCharge,
                    serviceChargeType : newItemDetails.dataValues.serviceChargeType,
                    orderId : req.body.orderId,
                    hotelId : req.body.hotelId,
                    restaurantId : req.body.restaurantId,
                    departmentId : newItemDetails.dataValues.departmentId
                },{transaction : tr});

                if(isEmpty(newOrderItemRecord)){
                    statusCode = 400;
                    customError.comment = "new order item not enter correctly!!";
                    throw new Error();
                }
                totalPayable += (Number(newItemDetails.dataValues.unitPrice)*Number(sortedItemsArray[i].quantity));
            }
        }
        console.log(totalPayable);

        // get the newly entered promotion ids 
        let newPromotions = sortedPromotionsArray.filter(e => e.id == null);
        let newPromotionsIdArray = newPromotions.map(e => e.promotionId);

        let results = [];
        if(!isEmpty(sortedPromotionsArray)){
            // get promotion item details
            let query = `
                SELECT * FROM promotion_items pi
                JOIN items i
                ON pi.itemId = i.id
                WHERE pi.promotionId IN (${newPromotionsIdArray.map(() => '?').join(',')});
            `;

            results = await sequelize.query(query, {
                replacements: newPromotionsIdArray,
                type: sequelize.QueryTypes.SELECT
            });
        }

        for(let i=0;i<sortedPromotionsArray.length;i++){
            let temp = orderPromotions.find(e => e.dataValues.id == sortedPromotionsArray[i].id);
            if(!isEmpty(temp)){
                if(temp.dataValues.processingStatus == 1 || temp.dataValues.processingStatus == 2){
                    statusCode = 400;
                    customError.comment = "please refresh the page to get latest status!!";
                    throw new Error();
                }

                let updateOrderPromotion = await order_item.update({
                    quantity : sortedPromotionsArray[i].quantity,
                    diningStatus : sortedPromotionsArray[i].diningStatus
                },{where : {id : temp.dataValues.id , orderId : req.body.orderId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId},transaction : tr})
                
                if(updateOrderPromotion != 1 || updateOrderPromotion < 0){
                    statusCode = 500;
                    customError.comment = "System error!!";
                    throw new Error();
                }  
                
                totalPayable += (Number(temp.unitPrice)*Number(sortedPromotionsArray[i].quantity));
            }else {
                if(isEmpty(sortedPromotionsArray[i].id)){
                    let newPromotionDetails = await promotion.findOne({
                        where : {id : sortedPromotionsArray[i].promotionId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , availability : 1 }
                    });

                    if(isEmpty(newPromotionDetails)){
                        statusCode = 400;
                        customError.comment = "enter a invalid promotion!!";
                        throw new Error();
                    }

                    // create the itemlist for the relavent promotion
                    let itemList = results.filter(e => e.promotionId == sortedPromotionsArray[i].promotionId);

                    let newOrderPromotionRecord = await order_item.create({
                        promotionId : promotion.promotionId,
                        promotionName : newPromotionDetails.dataValues.promotionName,
                        itemList : JSON.stringify(itemList),
                        avgPrepareTime : newPromotionDetails.dataValues.avgPrepareTime,
                        diningStatus : sortedPromotionsArray[i].diningStatus,
                        unitCost : newPromotionDetails.dataValues.unitCost,
                        unitPrice : newPromotionDetails.dataValues.unitPrice,
                        quantity : sortedPromotionsArray[i].quantity,
                        processingStatus : 0,
                        serviceCharge : newPromotionDetails.dataValues.serviceCharge,
                        serviceChargeType : newPromotionDetails.dataValues.serviceChargeType,
                        orderId : req.body.orderId,
                        hotelId : req.body.hotelId,
                        restaurantId : req.body.restaurantId
                    },{transaction : tr});

                    if(isEmpty(newOrderPromotionRecord)){
                        statusCode = 400;
                        customError.comment = "new promotion item not enterd correctly!!";
                        throw new Error();
                    }

                    totalPayable += (Number(newPromotionDetails.dataValues.unitPrice)*Number(sortedPromotionsArray[i].quantity));
                } 
            }
        }

        // check the totalPayable values
        // check the total tax all for two decimal point
        totalTax = totalPayable * Number(taxRate)/100;
        totalPayable += (Number(req.body.serviceCharge)+Number(totalTax));

        // console.log(totalPayable);
        // console.log(totalTax);

        // into 2 decimal points
        if(parseFloat(totalPayable.toFixed(2)) != req.body.totalPayable){
            statusCode = 400;
            customError.comment = "please refresh the page to get the latest prices!!";
            throw new Error();
        }
        if(parseFloat(totalTax.toFixed(2)) != req.body.totalTax){
            statusCode = 400;
            customError.comment = "total tax is not same please refresh the page!!";
            throw new Error();
        }
        
        statusCode = 200;
        await tr.commit();
        return res.status(statusCode).json(customSuccess);

    }catch(error){
        console.log(error);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// cancel an order
exports.cancelAnOrder = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;


    try{
        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.orderId)){
            statusCode = 400;
            customError.comment = "orderId is missing!!";
            throw new Error();
        }

        let userType = '';
        let userId = '';
        let tableId = '';
        // authorization
        if(req.body.role.includes('owner')){
            userType = 'owner';
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }
            userId = req.body.userId;

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });
            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

        }else if(req.body.role.includes('manager') || req.body.role.includes('waiter')){
            userType = 'employee';
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }
            userId = req.body.employeeId;

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });

            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

            // mapping should check if a user is an employee
            if(req.body.role.includes('manager')){}
            else if(req.body.role.includes('waiter')){
                const orderMappingCheck = await order.findOne({
                    where : {id : req.body.orderId , userType : userType , userId : userId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
                });
                if(isEmpty(orderMappingCheck)){
                    statusCode = 400;
                    customError.comment = "order id is not map with the user id!!";
                    throw new Error();
                }
            }

        }
        // get the table data from the order 
        const orderDetails = await order.findOne({
            where : {id : req.body.orderId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
        });
        if(isEmpty(orderDetails)){
            statusCode = 400;
                customError.comment = "order is not valid!!";
                throw new Error();
        }

        // before change the status check whether the items in the queue status 0 status
        const orderItemList = await order_item.findAll({
            where : {orderId : req.body.orderId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
        });

        // check if the user is a owner or manager then order is available for cancel at any state
        if(req.body.role.includes('manager') || req.body.role.includes('owner')){}
        else{
            orderItemList.forEach(e => {
                if(e.processingStatus == 1 || e.processingStatus == 2){
                    statusCode = 400;
                    customError.comment = "can't cancel the order because order items are in processing!!";
                    throw new Error();
                }
            })
        }

        // change the status of the order and change the status of the table
        const updatedResult1 = await order.update({
            cancelStatus : 1
        }, {where : {id : req.body.orderId , userId : userId , userType : userType , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , cancelStatus : 0 , paymentStatus : 0} , transaction : tr});
        if(updatedResult1 != 1 || updatedResult1 < 0){
            statusCode = 500;
            customError.comment = "System error!!";
            throw new Error();
        }

        // table status 1 means table is available
        // if tableId is empty then no status change for the table
        if(!isEmpty(orderDetails.dataValues.tableId)){
            const updatedResult2 = await table.update({
                tableStatus : 1
            } , {where : {id : orderDetails.dataValues.tableId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId} , transaction : tr});
            if(updatedResult2!= 1 || updatedResult2 < 0){
                statusCode = 500;
                customError.comment = "System error!!";
                throw new Error();
            }
        }

        statusCode = 200;
        await tr.commit();
        return res.status(statusCode).json(customSuccess);

    }catch(error){
        console.log(error);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

