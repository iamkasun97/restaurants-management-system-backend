const {sequelize} = require("../models");
const {owner} = require('../models');
const {users} = require('../models');
const {role} = require('../models');
const {hotel} = require('../models');
const {restaurant} = require('../models');
const {department} = require('../models');
const {item} = require('../models');
const {category_item} = require('../models');
const {category} = require('../models');
const {order_item} = require('../models');
const {promotion} = require('../models');
const {price_cost} = require('../models');
const {order} = require('../models');
const {transaction_detail} = require('../models');
const {table} = require('../models');
const {customer} = require('../models');
const {tax} = require('../models');

const config = require("../config");
const isEmpty = require("is-empty");
const bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const moment = require('moment');
require("dotenv").config();

exports.addPaymentOrder = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.orderId)){
            statusCode = 400;
            customError.comment = "orderId is missing!!";
            throw new Error();
        }

        let userType = '';
        // authorization
        if(req.body.role.includes('owner')){
            userType = 'owner';
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

            // check userId mapping
            const userIdMapping = await order.findOne({
                where : {id : req.body.orderId , userId : req.body.userId , userType : userType , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
            });

            if(isEmpty(userIdMapping)){
                statusCode = 400;
                customError.comment = "userId is not map with the order!!";
                throw new Error();
            }

        }else if(req.body.role.includes('manager') || req.body.role.includes('waiter')){
            userType = 'employee'
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

            // check userId mapping
            const userIdMapping = await order.findOne({
                where : {id : req.body.orderId , userId : req.body.userId , userType : userType , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
            });

            if(isEmpty(userIdMapping)){
                statusCode = 400;
                customError.comment = "userId is not map with the order!!";
                throw new Error();
            }
        }

        // do the payment calculation here
        // caculate the profit also in here
        const orderItems = await order_item.findAll({
            where : {orderId : req.body.orderId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
        });

        // change the status of the order payment status
        // and check the order already completed or not
        const searchResult = await order.findOne({
            where : {id : req.body.orderId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , paymentStatus : 0 , cancelStatus : 0}
        });

        if(isEmpty(searchResult)){
            statusCode = 400;
            customError.comment = "no order result found!!";
            throw new Error();
        }

        // calculate the total cost for user
        // calculate the total cost for the customer
        let totalCost = 0;
        let totalPrice = 0;
        let profit = 0;

        orderItems.forEach(e => {
            totalCost += (Number(e.dataValues.unitCost)*Number(e.dataValues.quantity));
            totalPrice += (Number(e.dataValues.unitPrice)*Number(e.dataValues.quantity));
        });
        // total price is the tax+servicecharge+totalprice
        let totalTax = Number(totalPrice)*(Number(searchResult.dataValues.taxRate)/100);
        let serviceCharge = Number(searchResult.dataValues.serviceCharge);
        totalPrice += (serviceCharge+totalTax);

        // calculate the profit here
        profit = totalPrice - totalCost;

        // check the payment is valid
        // payment validation
        if(isEmpty(req.body.payment)){
            statusCode = 400;
            customError.comment = "payment details can't be empty!!";
            throw new Error();
        }

        let balance = 0;
        let totalPayment = 0;
        // calculate the total payment here
        if(!isEmpty(req.body.payment?.card?.amount)){
            if(isNaN(req.body.payment.card.amount)){
                statusCode = 400;
                customError.comment = "enter a valid value for card amount!!";
                throw new Error();
            }
            totalPayment += Number(req.body.payment.card.amount);
        }
        if(!isEmpty(req.body.payment?.cash?.amount)){
            if(isNaN(req.body.payment.cash.amount)){
                statusCode = 400;
                customError.comment = "enter a valid value for cash amount!!";
                throw new Error();
            }
            totalPayment += Number(req.body.payment.cash.amount);
        }

        // calculate the balance here
        balance = totalPayment - (Number(totalPrice));
        if(balance < 0){
            statusCode = 400;
            customError.comment = "not enough payment for purchase!!";
            throw new Error();
        }

        let paymentKeyvalues = Object.keys(req.body.payment);
        console.log(paymentKeyvalues);

        if(paymentKeyvalues.includes('card')){}
        else if(paymentKeyvalues.includes('cash')){}
        else {
            statusCode = 400;
            customError.comment = "invalid type for payment!!";
            throw new Error();
        }

        if(isEmpty(req.body.payment.cash) && !isEmpty(req.body.payment.card)){
            balance = 0;
        }

        const updatedResult = await order.update({
            paymentStatus : 1
        },{where : {id : req.body.orderId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId} , transaction : tr});


        if(updatedResult != 1 || updatedResult < 0){
            statusCode = 500;
            customError.comment = "System error!!";
            throw new Error();
        }

        // change the relavent table status to 1 again
        // if the tableId is empty for the order then no need to update the table status
        if(!isEmpty(searchResult.dataValues.tableId)){
            const updatedResult1 = await table.update({
                tableStatus : 1
            },{where : {id : searchResult.dataValues.tableId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId} , transaction : tr})
    
            if(updatedResult1 != 1 || updatedResult1 < 0){
                statusCode = 500;
                customError.comment = "System error!!";
                throw new Error();
            }
        }

        // put the transaction details into the database
        const newTransactionDetail = await transaction_detail.create({
            orderId : req.body.orderId ,
            totalCost : totalCost,
            totalPrice : totalPrice,
            totalServiceCharge : searchResult.dataValues.serviceCharge,
            profit : profit,
            hotelId : req.body.hotelId,
            restaurantId : req.body.restaurantId,
            balance : balance,
            cardNumber : req.body.payment?.card?.cardNumber,
            paymentDetails : JSON.stringify(req.body.payment)
        } , {transaction : tr});

        if(isEmpty(newTransactionDetail)){
            customError.comment = "transaction details not enter properly!!";
            throw new Error();
        }

        statusCode = 200;
        await tr.commit();
        customSuccess.data = newTransactionDetail;
        return res.status(statusCode).json(customSuccess);
    }catch(error){
        console.log(error);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

exports.addDirectPaymentOrder = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        let userType = '';
        let userId = '';
        // authorization
        if(req.body.role.includes('owner')){
            userType = 'owner';
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }
            userId = req.body.userId;

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });
            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

        }else if(req.body.role.includes('manager') || req.body.role.includes('waiter')){
            userType = 'employee';
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }
            userId = req.body.employeeId;

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
        }

        // email validation if its not empty

        if(isEmpty(req.body.mobile)){
            statusCode = 400;
            customError.comment = "customer mobile number can't be empty!!";
            throw new Error();
        }        

        // register a customer
        const searchCustomer = await customer.findOne({
            where : {mobile : req.body.mobile , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
        });

        let customerDetails = '';

        if(isEmpty(searchCustomer)){
            // create a new user under a restaurant and a hotel
            const newCustomer = await customer.create({
                customerName : req.body.customerName,
                mobile : req.body.mobile,
                email : req.body.email,
                hotelId : req.body.hotelId,
                restaurantId : req.body.restaurantId
            },{transaction : tr});

            if(isEmpty(newCustomer)){
                customError.comment = "new customer is not created!!";
                throw new Error();
            }

            customerDetails = newCustomer;
        }else {
            customerDetails = searchCustomer;
        }

        // check the table status if 0 or 1
        // and also check the table enabled status 0 or 1
        // do the validation if its only order has dine in items
        let isDiningItems = req.body.itemsList.items?.find(e => e.diningStatus == 1);
        let isDiningPromotions = req.body.itemsList.promotions?.find(e => e.diningStatus == 1);

        if(!isEmpty(isDiningItems) || !isEmpty(isDiningPromotions)){
            if(isEmpty(req.body.tableId)){
                statusCode = 400;
                customError.comment = "order has dine in items tableId can't be empty!!";
                throw new Error();
            }

            const tableStatusSearch = await table.findOne({
                where : {id : req.body.tableId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
            });
            if(tableStatusSearch.dataValues.tableStatus == 0 || tableStatusSearch.dataValues.enableStatus == 0){
                statusCode = 400;
                customError.comment = "table is not available!!";
                throw new Error();
            } 

            // change the table status to zero
            // table status zero means table is booked
            const updatedResult =  await table.update({
                tableStatus : 0
            },{where : {id : req.body.tableId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId} , transaction : tr});

            if(updatedResult != 1 || updatedResult < 0){
                statusCode = 500;
                customError.comment = "System error!!";
                throw new Error();
            }
        }
        // get tax rate
        const taxRate = await tax.findOne({
            where : {hotelId : req.body.hotelId, restaurantId : req.body.restaurantId}
        });

        // place an order
        // tableId can be null
        const newOrder = await order.create({
            orderDateTime : new Date(),
            paymentStatus : 1,
            customerId : customerDetails.dataValues.id,
            userId : userId,
            userType : userType,
            serviceCharge : req.body.serviceCharge,
            taxRate : isEmpty(taxRate) ? 0 : taxRate.dataValues.taxRate,
            cancelStatus : 0,
            tableId : req.body.tableId == "" ? null : req.body.tableId,
            hotelId : req.body.hotelId,
            restaurantId : req.body.restaurantId
        }, {transaction : tr});

        if(isEmpty(newOrder)){
            customError.comment = "new order is not created!!";
            throw new Error();
        }

        // one order assign to one table
        // change the table status to zero
        let sortedItemsArray = req.body.itemsList.items?.sort((a , b) => a.itemId - b.itemId);
        let sortedPromotionsArray = req.body.itemsList.promotions?.sort((a , b) => a.promotionId - b.promotionId);

        // get item ids
        let itemIdArray = sortedItemsArray.map(e => e.itemId);
        // get promotion ids
        let promotionIdArray = sortedPromotionsArray.map(e => e.promotionId);

        // get item and promotion details
        let itemDetailsArray = await item.findAll({
            where : {id : itemIdArray , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
        });

        let promotionDetailsArray = await promotion.findAll({
            where : {id : promotionIdArray , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
        });

        let results = []
        if(!isEmpty(sortedPromotionsArray)){
            // get promotion item details
            let query = `
            SELECT * FROM promotion_items pi
            JOIN items i
            ON pi.itemId = i.id
            WHERE pi.promotionId IN (${promotionIdArray.map(() => '?').join(',')});
            `;

            results = await sequelize.query(query, {
                replacements: promotionIdArray,
                type: sequelize.QueryTypes.SELECT
            });
        }

        let promotionList = [];
        sortedPromotionsArray.forEach(p => {
            let promotionItemList = [];
            results.forEach(pi => {
                if(p.promotionId == pi.promotionId) {
                    promotionItemList.push({
                        itemId : pi.itemId,
                        promotionId : pi.promotionId,
                        itemName : pi.itemName,
                        quantity : pi.quantity,
                        unit : pi.unit,
                        unitAmount : pi.unitAmount,
                        departmentId : pi.departmentId,
                        restaurantId : pi.restaurantId,
                        hotelId : pi.hotelId
                    });
                }
            });

            promotionList.push({
                promotionId : p.promotionId,
                itemList : promotionItemList,
                quantity : p.quantity,
                diningStatus : p.diningStatus,
                processingStatus : p.processingStatus
            });
        });

        // total payable mean total value that should pay
        let totalPayable = 0;
        let totalCost = 0;

        let promotionDetailList = [];
       // pormotion details before enter to the database
       promotionDetailsArray.forEach(pd => {
            promotionList.forEach(p => {  
                if(pd.id == p.promotionId){   
                    totalPayable += (Number(pd.unitPrice))*(Number(p.quantity));
                    totalCost += (Number(pd.unitCost))*(Number(p.quantity));
                    promotionDetailList.push({              
                        promotionId : p.promotionId,
                        promotionName : pd.promotionName,
                        itemList : JSON.stringify(p.itemList),
                        quantity : p.quantity,
                        unitCost : pd.unitCost,
                        unitPrice : pd.unitPrice,
                        serviceCharge : pd.serviceCharge,
                        serviceChargeType : pd.serviceChargeType,
                        avgPrepareTime : pd.avgPrepareTime,
                        diningStatus : p.diningStatus,
                        processingStatus : p.processingStatus,
                        orderId : newOrder.id,
                        hotelId : pd.hotelId,
                        restaurantId : pd.restaurantId
                    })
                }
            })
       }); 
       
       
       // do the induvidual items part
       let itemDetailList = [];
       itemDetailsArray.forEach(id => {
            sortedItemsArray.forEach(i => {
                if(i.itemId == id.id){
                    totalPayable += (Number(id.unitPrice))*(Number(i.quantity));
                    totalCost += (Number(id.unitPrice))*(Number(i.quantity));
                    itemDetailList.push({
                        itemId : id.id,
                        itemName : id.itemName,
                        quantity : i.quantity,
                        unit : id.unit,
                        unitAmount : id.unitAmount,
                        unitCost : id.unitCost,
                        unitPrice : id.unitPrice,
                        serviceCharge : id.serviceCharge,
                        serviceChargeType : id.serviceChargeType,
                        avgPrepareTime : id.avgPrepareTime,
                        diningStatus : i.diningStatus,
                        processingStatus : i.processingStatus,
                        orderId : newOrder.id,
                        departmentId : id.departmentId,
                        restaurantId : id.restaurantId,
                        hotelId : id.hotelId
                    });
                }
            });
       });

       // check tax price also in here
       // get tax rate
       let totalTax = 0;
       if(!isEmpty(taxRate)){
            totalTax = totalPayable * (taxRate.dataValues.taxRate / 100);
       }
       // total payable add the service charge
       totalPayable += (req.body.serviceCharge+totalTax);
       
       // total tax prices same or not in the front and back
       if(parseFloat(totalTax.toFixed(2)) != parseFloat(req.body.totalTax.toFixed(2))){
            statusCode = 400;
            customError.comment = "totalTax is not same please refresh the page to get latest tax rates!!";
            throw new Error();
       }

       // check the totalPayable from front end and back end are similar or not
       if(parseFloat(totalPayable.toFixed(2)) != parseFloat(req.body.totalPayable.toFixed(2))){
            statusCode = 400;
            customError.comment = "totalPayable is not same plese refresh the page to get latest prices!!";
            throw new Error();
       }
       
       let finalArray = [...itemDetailList , ...promotionDetailList];
       

       // do the bulk insertion 
       const newOrderDetails = await order_item.bulkCreate(finalArray , {transaction : tr});
       if(isEmpty(newOrderDetails)){
            customError.comment = "new category_item not created!!";
            throw new Error();
        }
        // check the payment part
        let profit = parseFloat(totalPayable.toFixed(2)) - parseFloat(totalCost.toFixed(2));

        // get the balance
        let balance = 0;
        let totalPayment = 0;
        // calculate the total payment here
        if(!isEmpty(req.body.payment?.card?.amount)){
            if(isNaN(req.body.payment.card.amount)){
                statusCode = 400;
                customError.comment = "enter a valid value for card amount!!";
                throw new Error();
            }
            totalPayment += Number(req.body.payment.card.amount);
        }
        if(!isEmpty(req.body.payment?.cash?.amount)){
            if(isNaN(req.body.payment.cash.amount)){
                statusCode = 400;
                customError.comment = "enter a valid value for cash amount!!";
                throw new Error();
            }
            totalPayment += Number(req.body.payment.cash.amount);
        }

        // calculate the balance here
        balance = totalPayment - Number(totalPayable);
        if(balance < 0){
            statusCode = 400;
            customError.comment = "not enough payment for purchase!!";
            throw new Error();
        }

        let paymentKeyvalues = Object.keys(req.body.payment);

        if(paymentKeyvalues.includes('card')){}
        else if(paymentKeyvalues.includes('cash')){}
        else {
            statusCode = 400;
            customError.comment = "invalid type for payment!!";
            throw new Error();
        }

        if(isEmpty(req.body.payment.cash) && !isEmpty(req.body.payment.card)){
            balance = 0;
        }


        // again release the table
        // change the relavent table status to 1 again
        // if the tableId is empty for the order then no need to update the table status
        if(!isEmpty(req.body.tableId)){
            const updatedResult1 = await table.update({
                tableStatus : 1
            },{where : {id : req.body.tableId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId} , transaction : tr})
    
            if(updatedResult1 != 1 || updatedResult1 < 0){
                statusCode = 500;
                customError.comment = "System error!!";
                throw new Error();
            }
        }

        // put the transaction details into the database
        const newTransactionDetail = await transaction_detail.create({
            orderId : newOrder.id,
            totalCost : totalCost,
            totalPrice : totalPayable,
            totalServiceCharge : req.body.serviceCharge,
            profit : profit,
            hotelId : req.body.hotelId,
            restaurantId : req.body.restaurantId,
            balance : balance,
            cardNumber : req.body.payment?.card?.cardNumber,
            paymentDetails : JSON.stringify(req.body.payment)
        } , {transaction : tr});

        if(isEmpty(newTransactionDetail)){
            customError.comment = "transaction details not enter properly!!";
            throw new Error();
        }

        statusCode = 200;
        await tr.commit();
        customSuccess.data = newTransactionDetail;
        return res.status(statusCode).json(customSuccess);

    }catch(error){
        console.log(error);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}