const {sequelize} = require("../models");
const {owner} = require('../models');
const {users} = require('../models');


const config = require("../config");
const isEmpty = require("is-empty");
const bcrypt = require('bcryptjs');


exports.registerNewOwner = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;
    try{
        // check empty email , mobile , user name , password
        if(isEmpty(req.body.email)){
            statusCode = 400;
            customError.comment = "email can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.mobile)){
            statusCode = 400;
            customError.comment = "mobile can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.firstName)){
            statusCode = 400;
            customError.comment = "first name can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.lastName)){
            statusCode = 400;
            customError.comment = "last name can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.userName)){
            statusCode = 400;
            customError.comment = "user name can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.password)){
            statusCode = 400;
            customError.comment = "password can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.confirmPassword)){
            statusCode = 400;
            customError.comment = "confirm password can't be empty!!";
            throw new Error();
        }

        // check password and confirm password
        if(req.body.password != req.body.confirmPassword){
            statusCode = 400;
            customError.comment = "confirm password and password not matched!!";
            throw new Error();
        }

        // check the email is not register before
        const emailResult = await owner.findAll({
            where : {email : req.body.email}
        });
        if(!isEmpty(emailResult)){
            statusCode = 400;
            customError.comment = "this email is already registered!!";
            throw new Error();
        }

        // check the user name exist or not
        const userNameResult = await owner.findAll({
            where : {userName : req.body.userName}
        });
        if(!isEmpty(userNameResult)){
            statusCode = 400;
            customError.comment = "try another user name this user name is already in use!!";
            throw new Error();
        }

        // check the user name is exist in the users table also
        const userNameResult1 = await users.findAll({
            where : {userName : req.body.userName}
        });
        if(!isEmpty(userNameResult1)){
            statusCode = 400;
            customError.comment = "try another user name this user name is already in use!!";
            throw new Error();
        }

        // check the mobile number is not register before
        const mobileResult = await owner.findAll({
            where : {mobile : req.body.mobile}
        });

        if(!isEmpty(mobileResult)){
            statusCode = 400;
            customError.comment = "this mobile number is already registered!!";
            throw new Error();
        }

        // check the mobile number in users table also
        const mobileResult1 = await users.findAll({
            where : {mobile : req.body.mobile}
        });

        if(!isEmpty(mobileResult1)){
            statusCode = 400;
            customError.comment = "this mobile number is already registered!!";
            throw new Error();
        }

        let salt = bcrypt.genSaltSync(10);
        let passwordHash = bcrypt.hashSync(req.body.password, salt);
        // register an owner
        const newOwner = await owner.create({
            firstName : req.body.firstName,
            lastName : req.body.lastName,
            userName : req.body.userName,
            email : req.body.email,
            mobile : req.body.mobile,
            password : passwordHash
        } , {transaction : tr});
        if(isEmpty(newOwner)){
            statusCode = 500;
            customError.comment = "can't create a new owner!!";
            throw new Error();
        }

        // send an email to relavent email address
        statusCode = 200;
        await tr.commit();
        return res.status(statusCode).json(customSuccess);
    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

