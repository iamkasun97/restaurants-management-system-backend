const {sequelize} = require("../models");
const {owner} = require('../models');
const {users} = require('../models');
const {role} = require('../models');


const config = require("../config");
const isEmpty = require("is-empty");
const bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
require("dotenv").config();

exports.login = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.userName)){
            statusCode = 400;
            customError.comment = "user name can't be empty!!";
            throw new Error();
        }
        if(isEmpty(req.body.password)){
            statusCode = 400;
            customError.comment = "password can't be empty!!";
            throw new Error();
        }

        // first check from owners table
        const ownerTrue = await owner.findOne({
            where : {userName : req.body.userName}
        } , {transaction : tr});
        if(!isEmpty(ownerTrue)){
            if(bcrypt.compareSync(req.body.password, ownerTrue.dataValues.password)){
                // successfully login owner
                // token generate for a owner
                const token = jwt.sign(
                    {
                        userName: req.body.userName,
                        role: ['owner'],
                        userId : ownerTrue.dataValues.id
                    },
                    process.env.SECRET_TOKEN,
                    {
                        expiresIn: "24h",
                    }
                );
                await tr.commit();
                statusCode = 200;
                customSuccess.data = {
                    token : token,
                    userData : {
                        userId : `owner_${ownerTrue.dataValues.id}`,
                        userName: req.body.userName,
                        role: ['owner'],
                        firstName : ownerTrue.dataValues.firstName,
                        lastName : ownerTrue.dataValues.lastName,
                        hotelId : null,
                        restaurantId : null,
                        departmentId : null
                    }
                };
                return res.status(statusCode).json(customSuccess);
            }
        }else {
            const userTrue = await users.findOne({
                where : {userName : req.body.userName}
            } , {transaction : tr});
            if(!isEmpty(userTrue)){
                if(bcrypt.compareSync(req.body.password, userTrue.dataValues.password)){
                    // successfully login as a user
                    // token generate for a user
                    // use JSON stringify to get array
                    const getRole = await role.findOne({
                        where : {id : userTrue.dataValues.roleId}
                    });
                    let userRole = getRole.dataValues.permissionLevel;
                    const token = jwt.sign(
                        {
                            userName: req.body.userName,
                            role: userRole,
                            employeeId : userTrue.dataValues.id
                        },
                        process.env.SECRET_TOKEN,
                        {
                            expiresIn: "24h",
                        }
                    );
                    await tr.commit();
                    statusCode = 200;
                    customSuccess.data = {
                        token : token,
                        userData : {
                            userId : `employee_${userTrue.dataValues.id}`,
                            userName: req.body.userName,
                            role: userRole,
                            firstName : userTrue.dataValues.firstName,
                            lastName : userTrue.dataValues.lastName,
                            hotelId : userTrue.dataValues.hotelId,
                            restaurantId : userTrue.dataValues.restaurantId,
                            departmentId : userTrue.dataValues.departmentId
                        }
                    };
                    return res.status(statusCode).json(customSuccess);
                }
            }
        }

        // user not found
        statusCode = 400;
        customError.comment = "user not found!!";
        throw new Error();  

    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}