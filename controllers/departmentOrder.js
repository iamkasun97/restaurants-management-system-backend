const {sequelize} = require("../models");
const {owner} = require('../models');
const {users} = require('../models');
const {role} = require('../models');
const {hotel} = require('../models');
const {restaurant} = require('../models');
const {department} = require('../models');
const {item} = require('../models');
const {category_item} = require('../models');
const {category} = require('../models');
const {order_item} = require('../models');
const {order} = require('../models');

const config = require("../config");
const isEmpty = require("is-empty");
const bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const moment = require('moment');
require("dotenv").config();

// get dropdown details for get orders for a department
exports.dropdownDetailsForGetOrderItems = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // owners enter a item scenario
        if(req.body.role.includes('owner')){
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }

            const hotels = await hotel.findAll({
                where : {ownerId : req.body.userId , hotelStatus : 1}
            });
            const restaurants = await restaurant.findAll({
                where : {ownerId : req.body.userId , restaurantStatus : 1}
            });
            const departments = await department.findAll({
                where: {ownerId : req.body.userId , departmentStatus : 1}
            });
            const categories = await category.findAll({});

            // add departments array to the response
            let departmentArray = [];
            departments.forEach(department => {
                departmentArray.push(department.dataValues);
            });

            // do the mapping
            let hotelAndRestaurantArray = [];
            hotels.forEach(hotel => {
                let restaurantArray = [];
                restaurants.forEach(restaurant => {
                    let categoryArray = [];
                    categories.forEach(category => {
                        if(hotel.dataValues.id == category.dataValues.hotelId && restaurant.dataValues.id == category.dataValues.restaurantId){
                            categoryArray.push(category.dataValues);
                        }
                    });
                    if(restaurant.dataValues.hotelId == hotel.dataValues.id){
                        restaurantArray.push({
                            id : restaurant.dataValues.id,
                            restaurantName : restaurant.dataValues.restaurantName,
                            ownerId : restaurant.dataValues.ownerId,
                            hotelId : restaurant.dataValues.hotelId,
                            categories : categoryArray
                        });
                    }
                });
                let temp = {
                    id : hotel.dataValues.id,
                    hotelName : hotel.dataValues.hotelName,
                    hotelAddress : hotel.dataValues.hotelAddress,
                    statusComment : hotel.dataValues.statusComment,
                    ownerId : hotel.dataValues.ownerId,
                    restaurants : restaurantArray,
                }
                hotelAndRestaurantArray.push(temp);
            });
            let response = {
                hotels : hotelAndRestaurantArray,
                departments : departmentArray
            }
            // console.log(response);
            statusCode = 200;
            await tr.commit();
            customSuccess.data = response;
            return res.status(statusCode).json(customSuccess);

        }else if(req.body.role.includes('manager')){
            // manager employee scenario
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            // get the hotel , restaurant and department details for and employee
            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            const hotelE = await hotel.findOne({
                where : {id : searchResult.dataValues.hotelId , hotelStatus : 1}
            });
            const restaurantE = await restaurant.findOne({
                where : {id : searchResult.dataValues.restaurantId , restaurantStatus : 1}
            });
            const departmentE = await department.findOne({
                where: {id : searchResult.dataValues.departmentId , departmentStatus : 1}
            });
            const categoryE = await category.findAll({
                where : {hotelId : searchResult.dataValues.hotelId , restaurantId : searchResult.dataValues.restaurantId}
            });

            let restaurantArray = [];
            let departmentArray = [];
            let categoryArray = [];
            let hotelAndRestaurantArray = [];
            
            categoryE.forEach(e => {
                categoryArray.push(e.dataValues);
            });

            let restaurantAndCategories = {
                id : restaurantE.dataValues.id,
                restaurantName : restaurantE.dataValues.restaurantName,
                ownerId : restaurantE.dataValues.ownerId,
                hotelId : restaurantE.dataValues.hotelId,
                categories : categoryArray
            }

            departmentArray.push(departmentE.dataValues);
            restaurantArray.push(restaurantAndCategories);

            let temp = {
                id : hotelE.dataValues.id,
                hotelName : hotelE.dataValues.hotelName,
                hotelAddress : hotelE.dataValues.hotelAddress,
                statusComment : hotelE.dataValues.statusComment,
                ownerId : hotelE.dataValues.ownerId,
                restaurants : restaurantArray,
            }
            hotelAndRestaurantArray.push(temp);

            let response = {
                hotels : hotelAndRestaurantArray,
                departments : departmentArray
            }
            statusCode = 200;
            await tr.commit();
            customSuccess.data = response;
            return res.status(statusCode).json(customSuccess);
        }

        customError.comment = "error occurs!!";
        throw new Error();
    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// pending orders for the department
exports.getPendingOrdersForDepartment = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.departmentId)){
            statusCode = 400;
            customError.comment = "departmentId is missing!!";
            throw new Error();
        }

        if(req.body.role.includes('owner')){
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });
            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

            // have to check the department is available
            const departmentCheck = await department.findAll({
                where : {id : req.body.departmentId , ownerId : req.body.userId}
            });
            if(isEmpty(departmentCheck)){
                statusCode = 400;
                customError.comment = "departmentId is not valid!!";
                throw new Error(); 
            }
        }else if(req.body.role.includes('manager') || req.body.role.includes('chef')){
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId || req.body.departmentId != searchResult.dataValues.departmentId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
            
            // have to check the department is available
            const departmentCheck = await department.findAll({
                where : {id : req.body.departmentId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(departmentCheck)){
                statusCode = 400;
                customError.comment = "departmentId is not valid!!";
                throw new Error(); 
            }
        }

        // check items in the order for the relevant department
        const orderItems = await order_item.findAll({
            where : {hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , departmentId : req.body.departmentId , promotionId : null},
            order : [
                ['orderId' , 'ASC']
            ]
        });

        // check promotions in the order for the relevant department
        const orderPromotions = await order_item.findAll({
            where : {hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , itemId : null},
            order : [
                ['orderId' , 'ASC']
            ]
        });
        // get order id and then distinct as an array
        // then get the order data


        let tempArray = [];

        orderItems.forEach(e => {
            tempArray.push({
                id : e.id,
                itemId : e.itemId,
                itemName : e.itemName,
                unit : e.unit,
                unitAmount : e.unitAmount,
                avgPrepareTime : e.avgPrepareTime,
                quantity : e.quantity,
                diningStatus : e.diningStatus,
                processingStatus : e.processingStatus,
                orderId : e.orderId,
            });
        });

        // add promotion items to the same array
        orderPromotions.forEach(e => {
            let items = JSON.parse(e.itemList);
            items.forEach(i => {
                if(i.departmentId == req.body.departmentId){
                    tempArray.push({
                        id : e.id,
                        itemId : i.itemId,
                        itemName : i.itemName,
                        unit : 'none',
                        unitAmount : 'none',
                        avgPrepareTime : e.avgPrepareTime,
                        quantity : Number(e.quantity)*Number(i.quantity),
                        diningStatus : e.diningStatus,
                        processingStatus : e.processingStatus,
                        orderId : e.orderId,
                    });
                }
            })
        });

        // then sort the order orderId
        let sortedTempArray = tempArray.sort((a, b) => {
            return a.orderId - b.orderId;
        });

        statusCode = 200;
        await tr.commit();
        customSuccess.data = sortedTempArray;
        return res.status(statusCode).json(customSuccess);
    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// get all the orderId for a relavet department
exports.getOrdersIds = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.departmentId)){
            statusCode = 400;
            customError.comment = "departmentId is missing!!";
            throw new Error();
        }

        if(req.body.role.includes('owner')){
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });
            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

            // have to check the department is available
            const departmentCheck = await department.findAll({
                where : {id : req.body.departmentId , ownerId : req.body.userId}
            });
            if(isEmpty(departmentCheck)){
                statusCode = 400;
                customError.comment = "departmentId is not valid!!";
                throw new Error(); 
            }
        }else if(req.body.role.includes('manager') || req.body.role.includes('chef')){
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId || req.body.departmentId != searchResult.dataValues.departmentId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
            
            // have to check the department is available
            const departmentCheck = await department.findAll({
                where : {id : req.body.departmentId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(departmentCheck)){
                statusCode = 400;
                customError.comment = "departmentId is not valid!!";
                throw new Error(); 
            }
        }

        // check items in the order for the relevant department
        const orderItems = await order_item.findAll({
            where : {hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , departmentId : req.body.departmentId , promotionId : null},
            order : [
                ['orderId' , 'ASC']
            ]
        });

        // check promotions in the order for the relevant department
        const orderPromotions = await order_item.findAll({
            where : {hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , itemId : null },
            order : [
                ['orderId' , 'ASC']
            ]
        });

        let tempArray = [];

        orderItems.forEach(e => {
            tempArray.push(e.orderId);
        });

        // add promotion items to the same array
        orderPromotions.forEach(e => {
            let items = JSON.parse(e.itemList);
            items.forEach(i => {
                if(i.departmentId == req.body.departmentId){
                    tempArray.push(e.orderId);
                }
            });
        });

        // then sort the order orderId
        let sortedTempArray = tempArray.sort((a, b) => {
            return a.orderId - b.orderId;
        });

        // get order details
        const orderDetails = await order.findAll({
            where : {id : sortedTempArray , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , cancelStatus : 0}
        });
        console.log(orderDetails);
        let response = orderDetails.map(e => e.id);

        // let response = [...new Set(sortedTempArray)];

        statusCode = 200;
        await tr.commit();
        customSuccess.data = response;
        return res.status(statusCode).json(customSuccess);

    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// get all pending orders under a department
exports.getPendingOrdersAndItemsUnderDepartment = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.departmentId)){
            statusCode = 400;
            customError.comment = "departmentId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.orderId)){
            statusCode = 400;
            customError.comment = "orderId is missing!!";
            throw new Error();
        }

        if(req.body.role.includes('owner')){
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });
            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

            // have to check the department is available
            const departmentCheck = await department.findAll({
                where : {id : req.body.departmentId , ownerId : req.body.userId}
            });
            if(isEmpty(departmentCheck)){
                statusCode = 400;
                customError.comment = "departmentId is not valid!!";
                throw new Error(); 
            }

        }else if(req.body.role.includes('manager') || req.body.role.includes('chef')){
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId || req.body.departmentId != searchResult.dataValues.departmentId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
            
            // have to check the department is available
            const departmentCheck = await department.findAll({
                where : {id : req.body.departmentId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(departmentCheck)){
                statusCode = 400;
                customError.comment = "departmentId is not valid!!";
                throw new Error(); 
            }

        }

        let orderItems = [];
        let orderPromotions = [];
        if(req.body.orderId == 'ALL'){
            // check items in the order for the relevant department
            orderItems = await order_item.findAll({
                where : {hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , departmentId : req.body.departmentId , promotionId : null},
                order : [
                    ['orderId' , 'ASC']
                ]
            });

            // check promotions in the order for the relevant department
            orderPromotions = await order_item.findAll({
                where : {hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , itemId : null},
                order : [
                    ['orderId' , 'ASC']
                ]
            });
        }else {
            // check items in the order for the relevant department
            orderItems = await order_item.findAll({
                where : {hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , departmentId : req.body.departmentId , promotionId : null , orderId : req.body.orderId},
                order : [
                    ['orderId' , 'ASC']
                ]
            });

            // check promotions in the order for the relevant department
            orderPromotions = await order_item.findAll({
                where : {hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , itemId : null , orderId : req.body.orderId},
                order : [
                    ['orderId' , 'ASC']
                ]
            });
        }

        // get order items order ids
        let orderIds = orderItems.map(e => e.orderId);
        let orderItemDetails = await order.findAll({
            where : {id : orderIds , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , cancelStatus : 0}
        });

        let tempArray = [];

        orderItems.forEach(e => {
                let orderTemp = orderItemDetails.find(o => o.id == e.orderId);
                if(!isEmpty(orderTemp)){
                    if(orderTemp.dataValues.cancelStatus == 0){
                        if(e.processingStatus == 0 || e.processingStatus == 1){
                            tempArray.push({
                                id : e.id,
                                itemId : e.itemId,
                                itemName : e.itemName,
                                unit : e.unit,
                                unitAmount : e.unitAmount,
                                avgPrepareTime : e.avgPrepareTime,
                                quantity : e.quantity,
                                diningStatus : e.diningStatus,
                                processingStatus : e.processingStatus,
                                orderId : e.orderId,
                            });
                        }
                    }
                }
        });
        orderIds = orderPromotions.map(e => e.orderId);
        let orderPromotionDetails = await order.findAll({
            where : {id : orderIds , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , cancelStatus : 0}
        })

        // add promotion items to the same array
        orderPromotions.forEach(e => {
            let orderTemp = orderPromotionDetails.find(o => o.id == e.orderId);
            if(!isEmpty(orderTemp)){
                if(orderTemp.dataValues.cancelStatus == 0){
                    let items = JSON.parse(e.itemList);
                    items.forEach(i => {
                        if(i.departmentId == req.body.departmentId){
                            if(e.processingStatus == 0 || e.processingStatus == 1){
                                tempArray.push({
                                    id : e.id,
                                    itemId : i.itemId,
                                    itemName : i.itemName,
                                    unit : 'none',
                                    unitAmount : 'none',
                                    avgPrepareTime : e.avgPrepareTime,
                                    quantity : Number(e.quantity)*Number(i.quantity),
                                    diningStatus : e.diningStatus,
                                    processingStatus : e.processingStatus,
                                    orderId : e.orderId,
                                });
                            }
                        }
                    })
                }
            }
        });

        // sort the tempArray according to the orderId
        tempArray = tempArray.sort((a,b) => {
            if(a.orderId > b.orderId){
                return 1;
            }else if(a.orderId < b.orderId){
                return -1;
            }else{
                return 0;
            }
        });

        statusCode = 200;
        await tr.commit();
        customSuccess.data = tempArray;
        return res.status(statusCode).json(customSuccess);

    }catch(error){
        console.log(error);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// get the items under a specific orderId
exports.getItemsForAnOrder = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.departmentId)){
            statusCode = 400;
            customError.comment = "departmentId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.orderId)){
            statusCode = 400;
            customError.comment = "orderId is missing!!";
            throw new Error();
        }

        if(req.body.role.includes('owner')){
            // owner scenatio to add an item
            if(isEmpty(req.body.userId)){
                statusCode = 400;
                customError.comment = "userId is missing!!";
                throw new Error();
            }

            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : req.body.userId}
            });
            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }

            // have to check the department is available
            const departmentCheck = await department.findAll({
                where : {id : req.body.departmentId , ownerId : req.body.userId}
            });
            if(isEmpty(departmentCheck)){
                statusCode = 400;
                customError.comment = "departmentId is not valid!!";
                throw new Error(); 
            }

            // have to check the userId map with orderId
            const orderCheck = await order.findOne({
                where : {id : req.body.orderId , userId : req.body.userId , userType : 'owner'}
            })

            if(isEmpty(orderCheck)){
                statusCode = 400;
                customError.comment = "userId is not mapping with the user!!";
                throw new Error(); 
            }
        }else if(req.body.role.includes('manager') || req.body.role.includes('chef')){
            // employee manager scenatio to add an item
            if(isEmpty(req.body.employeeId)){
                statusCode = 400;
                customError.comment = "employeeId is missing!!";
                throw new Error();
            }

            const searchResult = await users.findOne({
                where : {id : req.body.employeeId}
            });
            // ensure that employee manager user can add things to only his department
            if(req.body.hotelId != searchResult.dataValues.hotelId || req.body.restaurantId != searchResult.dataValues.restaurantId || req.body.departmentId != searchResult.dataValues.departmentId){
                statusCode = 401;
                customError.comment = 'unauthorized access!!';
                throw new Error();
            }
            
            // have to check the mapping of hotelId and the restaurant Id
            const mapingCheck = await restaurant.findAll({
                where : {hotelId : req.body.hotelId , id : req.body.restaurantId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(mapingCheck)){
                statusCode = 400;
                customError.comment = "hotelId and restaurantId is not map each other!!";
                throw new Error();
            }
            
            // have to check the department is available
            const departmentCheck = await department.findAll({
                where : {id : req.body.departmentId , ownerId : searchResult.dataValues.createdOwner}
            });

            if(isEmpty(departmentCheck)){
                statusCode = 400;
                customError.comment = "departmentId is not valid!!";
                throw new Error(); 
            }

            // have to check the userId map with orderId
            const orderCheck = await order.findOne({
                where : {id : req.body.orderId , userId : req.body.userId , userType : 'employee'}
            })

            if(isEmpty(orderCheck)){
                statusCode = 400;
                customError.comment = "userId is not mapping with the user!!";
                throw new Error(); 
            }
        }

        // check items in the order for the relevant department
        const orderItems = await order_item.findAll({
            where : {hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , departmentId : req.body.departmentId , promotionId : null , orderId : req.body.orderId},
            order : [
                ['orderId' , 'ASC']
            ]
        });

        // check promotions in the order for the relevant department
        const orderPromotions = await order_item.findAll({
            where : {hotelId : req.body.hotelId , restaurantId : req.body.restaurantId , itemId : null , orderId : req.body.orderId},
            order : [
                ['orderId' , 'ASC']
            ]
        });

        let tempArray = [];

        orderItems.forEach(e => {
            if(e.orderId == req.body.orderId){
                tempArray.push({
                    id : e.id,
                    itemId : e.itemId,
                    itemName : e.itemName,
                    unit : e.unit,
                    unitAmount : e.unitAmount,
                    avgPrepareTime : e.avgPrepareTime,
                    quantity : e.quantity,
                    diningStatus : e.diningStatus,
                    processingStatus : e.processingStatus,
                    orderId : e.orderId,
                });
            }
        });

        // add promotion items to the same array
        orderPromotions.forEach(e => {
            let items = JSON.parse(e.itemList);
            items.forEach(i => {
                if(i.departmentId == req.body.departmentId && e.orderId == req.body.orderId){
                    tempArray.push({
                        id : e.id,
                        itemId : i.itemId,
                        itemName : i.itemName,
                        unit : 'none',
                        unitAmount : 'none',
                        avgPrepareTime : e.avgPrepareTime,
                        quantity : Number(e.quantity)*Number(i.quantity),
                        diningStatus : e.diningStatus,
                        processingStatus : e.processingStatus,
                        orderId : e.orderId,
                    });
                }
            })
        });

        statusCode = 200;
        await tr.commit();
        customSuccess.data = tempArray;
        return res.status(statusCode).json(customSuccess);

    }catch(error){
        console.log(error);
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}

// change the status of the order item
exports.changeStatusPendingOrder = async (req , res , next) => {
    const tr = await sequelize.transaction();
    const customError = Object.assign({}, config.custom_error);
    const customSuccess = Object.assign({}, config.custom_success);
    let statusCode = 500;

    try{
        // check empty
        if(isEmpty(req.body.hotelId)){
            statusCode = 400;
            customError.comment = "hotelId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.restaurantId)){
            statusCode = 400;
            customError.comment = "restaurantId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.departmentId)){
            statusCode = 400;
            customError.comment = "departmentId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.orderId)){
            statusCode = 400;
            customError.comment = "orderId is missing!!";
            throw new Error();
        }

        if(isEmpty(req.body.orderItemId)){
            statusCode = 400;
            customError.comment = "orderItemId is missing!!";
            throw new Error();
        }

        if(req.body.processingStatus === "" || req.body.processingStatus == undefined){
            statusCode = 400;
            customError.comment = "processing status is missing!!";
            throw new Error();
        }

        // check the status
        const searchResult = await order_item.findOne({
            where : {id : req.body.orderItemId , orderId : req.body.orderId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId}
        });

        if(isEmpty(searchResult)){
            statusCode = 400;
            customError.comment = "bad request!";
            throw new Error();
        }

        if(searchResult.dataValues.processingStatus == req.body.processingStatus){
            statusCode = 400;
            customError.comment = "bad request!!";
            throw new Error();
        }

        // change the status
        const updatedResult = await order_item.update({
            processingStatus : req.body.processingStatus
        },{where : {id : req.body.orderItemId , hotelId : req.body.hotelId , restaurantId : req.body.restaurantId} , transaction : tr});

        if(updatedResult != 1 || updatedResult < 0){
            statusCode = 500;
            customError.comment = "System error!!";
            throw new Error();
        }

        statusCode = 200;
        await tr.commit();
        return res.status(statusCode).json(customSuccess);

    }catch(error){
        await tr.rollback();
        return res.status(statusCode).json(customError);
    }
}
