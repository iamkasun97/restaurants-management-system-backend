-- MySQL dump 10.13  Distrib 8.0.31, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: restaurant_management_system_db
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `id` int NOT NULL AUTO_INCREMENT,
  `categoryName` varchar(255) DEFAULT NULL,
  `categoryDescription` varchar(255) DEFAULT NULL,
  `hotelId` int DEFAULT NULL,
  `restaurantId` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hotelId` (`hotelId`),
  KEY `restaurantId` (`restaurantId`),
  CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`hotelId`) REFERENCES `hotels` (`id`),
  CONSTRAINT `categories_ibfk_2` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'dinner','dinner description',1,1,'2023-04-06 07:15:10','2023-04-06 07:15:10'),(2,'lunch','lunch description',1,1,'2023-04-10 06:30:24','2023-04-10 06:30:24'),(3,'breakfast','breakfast description',2,2,'2023-04-10 06:32:29','2023-04-10 06:32:29');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_items`
--

DROP TABLE IF EXISTS `category_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category_items` (
  `id` int NOT NULL AUTO_INCREMENT,
  `categoryId` int DEFAULT NULL,
  `itemId` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `categoryId` (`categoryId`),
  KEY `itemId` (`itemId`),
  CONSTRAINT `category_items_ibfk_1` FOREIGN KEY (`categoryId`) REFERENCES `categories` (`id`),
  CONSTRAINT `category_items_ibfk_2` FOREIGN KEY (`itemId`) REFERENCES `items` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_items`
--

LOCK TABLES `category_items` WRITE;
/*!40000 ALTER TABLE `category_items` DISABLE KEYS */;
INSERT INTO `category_items` VALUES (5,1,3,'2023-04-07 05:01:34','2023-04-07 05:01:34'),(6,1,4,'2023-04-11 00:33:10','2023-04-11 00:33:10'),(7,1,5,'2023-04-11 05:04:32','2023-04-11 05:04:32'),(8,1,6,'2023-04-11 05:20:44','2023-04-11 05:20:44'),(9,1,7,'2023-04-11 09:04:26','2023-04-11 09:04:26'),(10,2,8,'2023-04-11 10:13:49','2023-04-11 10:13:49'),(11,1,8,'2023-04-11 10:13:49','2023-04-11 10:13:49'),(12,1,2,'2023-04-19 15:37:52','2023-04-19 15:37:52'),(13,2,9,'2023-04-19 15:52:55','2023-04-19 15:52:55'),(14,1,9,'2023-04-19 15:52:55','2023-04-19 15:52:55'),(15,2,10,'2023-04-19 16:27:13','2023-04-19 16:27:13'),(16,1,10,'2023-04-19 16:27:13','2023-04-19 16:27:13');
/*!40000 ALTER TABLE `category_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `customerName` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `hotelId` int DEFAULT NULL,
  `restaurantId` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hotelId` (`hotelId`),
  KEY `restaurantId` (`restaurantId`),
  CONSTRAINT `customers_ibfk_1` FOREIGN KEY (`hotelId`) REFERENCES `hotels` (`id`),
  CONSTRAINT `customers_ibfk_2` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=164 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (159,'kasun','0771330065','test1@gmail.com',1,1,'2023-04-12 01:05:50','2023-04-12 01:05:50'),(162,'','0719012099','',1,1,'2023-04-27 11:19:49','2023-04-27 11:19:49'),(163,'','0719012098','',1,1,'2023-05-03 07:59:32','2023-05-03 07:59:32');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `departments` (
  `id` int NOT NULL AUTO_INCREMENT,
  `departmentName` varchar(255) DEFAULT NULL,
  `departmentDescription` varchar(255) DEFAULT NULL,
  `departmentStatus` int DEFAULT NULL,
  `statusComment` varchar(255) DEFAULT NULL,
  `ownerId` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ownerId` (`ownerId`),
  CONSTRAINT `departments_ibfk_1` FOREIGN KEY (`ownerId`) REFERENCES `owners` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departments`
--

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` VALUES (1,'bar/black','description bar',1,NULL,1,'2023-04-05 10:18:51','2023-04-05 10:18:51');
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hotels`
--

DROP TABLE IF EXISTS `hotels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hotels` (
  `id` int NOT NULL AUTO_INCREMENT,
  `hotelName` varchar(255) DEFAULT NULL,
  `hotelAddress` varchar(255) DEFAULT NULL,
  `hotelStatus` int DEFAULT NULL,
  `statusComment` varchar(255) DEFAULT NULL,
  `ownerId` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ownerId` (`ownerId`),
  CONSTRAINT `hotels_ibfk_1` FOREIGN KEY (`ownerId`) REFERENCES `owners` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotels`
--

LOCK TABLES `hotels` WRITE;
/*!40000 ALTER TABLE `hotels` DISABLE KEYS */;
INSERT INTO `hotels` VALUES (1,'jetwing white','galle road , colombo 02',1,NULL,1,'2023-04-05 10:18:29','2023-04-05 10:18:29'),(2,'jetwing blue','galle road , colombo 02',1,NULL,2,'2023-04-10 06:31:05','2023-04-10 06:31:05');
/*!40000 ALTER TABLE `hotels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `items` (
  `id` int NOT NULL AUTO_INCREMENT,
  `itemName` varchar(255) DEFAULT NULL,
  `itemDescription` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `unit` varchar(255) DEFAULT NULL,
  `unitAmount` varchar(255) DEFAULT NULL,
  `unitCost` double DEFAULT NULL,
  `unitPrice` double DEFAULT NULL,
  `serviceCharge` double DEFAULT NULL,
  `serviceChargeType` int DEFAULT NULL,
  `avgPrepareTime` int DEFAULT NULL,
  `serveTime` varchar(255) DEFAULT NULL,
  `availability` int DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `hotelId` int DEFAULT NULL,
  `restaurantId` int DEFAULT NULL,
  `departmentId` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hotelId` (`hotelId`),
  KEY `restaurantId` (`restaurantId`),
  KEY `departmentId` (`departmentId`),
  CONSTRAINT `items_ibfk_1` FOREIGN KEY (`hotelId`) REFERENCES `hotels` (`id`),
  CONSTRAINT `items_ibfk_2` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`),
  CONSTRAINT `items_ibfk_3` FOREIGN KEY (`departmentId`) REFERENCES `departments` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (2,'wild apple','test4 description','[\"keyword1\" , \"keyword2\" , \"keyword3\"]','ml','100',100.98,120.76,10.56,0,16,'[{\"startTime\":\"07:55:00\",\"endTime\":\"12:00:00\"},{\"startTime\":\"12:00:00\",\"endTime\":\"22:00:00\"}]',1,'1681898872643.jpeg',1,1,1,'2023-04-06 07:15:30','2023-04-25 07:47:44'),(3,'Chicken Kottu','new description','[\"keyword1\" , \"keyword2\" , \"keyword3\"]','1','1',1250,1600,10.56,1,15,'[{\"startTime\":\"07:55:00\",\"endTime\":\"12:00:00\"},{\"startTime\":\"12:00:00\",\"endTime\":\"22:00:00\"}]',1,'1680843694426.jpeg',1,1,1,'2023-04-07 05:01:34','2023-04-07 05:01:34'),(4,'Chicken Biriyani','new description','[\"keyword1\" , \"keyword2\" , \"keyword3\"]','1','1',1350,1700,10.56,1,15,'[{\"startTime\":\"05:55:00\",\"endTime\":\"12:00:00\"},{\"startTime\":\"12:00:00\",\"endTime\":\"22:00:00\"}]',1,'1681173190660.jpeg',1,1,1,'2023-04-11 00:33:10','2023-04-11 00:33:10'),(5,'Egg Role','vishwa 1 new description','[\"keyword1\" , \"keyword2\" , \"keyword3\"]','1','1',150,250,10.56,1,15,'[{\"startTime\":\"05:55:00\",\"endTime\":\"12:00:00\"},{\"startTime\":\"12:00:00\",\"endTime\":\"22:00:00\"}]',1,'1681189472515.jpeg',1,1,1,'2023-04-11 05:04:32','2023-04-11 05:04:32'),(6,'Lion Beer Can','vishwa 1 new description','[\"keyword1\" , \"keyword2\" , \"keyword3\"]','1','1',550,650,10.56,1,15,'[{\"startTime\":\"05:55:00\",\"endTime\":\"12:00:00\"},{\"startTime\":\"12:00:00\",\"endTime\":\"22:00:00\"}]',1,'1681190444557.jpeg',1,1,1,'2023-04-11 05:20:44','2023-04-11 05:20:44'),(7,'Fried Fish','vishwa 1 new description','[\"keyword1\" , \"keyword2\" , \"keyword3\"]','1','1',650,700,10.56,1,15,'[{\"startTime\":\"05:55:00\",\"endTime\":\"12:00:00\"},{\"startTime\":\"12:00:00\",\"endTime\":\"22:00:00\"}]',1,'1681203866080.jpeg',1,1,1,'2023-04-11 09:04:26','2023-04-11 09:04:26'),(8,'Black Pork Curry','vishwa 1 new description','[\"keyword1\" , \"keyword2\" , \"keyword3\"]','1','1',550,850,10.56,1,15,'[{\"startTime\":\"05:55:00\",\"endTime\":\"12:00:00\"},{\"startTime\":\"12:00:00\",\"endTime\":\"22:00:00\"}]',1,'1681208029490.jpeg',1,1,1,'2023-04-11 10:13:49','2023-04-11 10:13:49'),(9,'chicken kottu','vishwa 1 new description','[\"keyword1\" , \"keyword2\" , \"keyword3\"]','ml','100',100.98,120.76,10.56,1,15,'[{\"startTime\":\"05:55:00\",\"endTime\":\"12:00:00\"},{\"startTime\":\"12:00:00\",\"endTime\":\"22:00:00\"}]',1,'1681899775865.jpeg',1,1,1,'2023-04-19 15:52:55','2023-04-19 15:52:55'),(10,'chicken kottu new','vishwa 1 new description','[\"keyword1\" , \"keyword2\" , \"keyword3\"]','ml','100',100.98,120.76,10.56,1,15,'[{\"startTime\":\"05:55:00\",\"endTime\":\"12:00:00\"},{\"startTime\":\"12:00:00\",\"endTime\":\"22:00:00\"}]',1,'1681901833677.jpeg',1,1,1,'2023-04-19 16:27:13','2023-04-19 16:27:13');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_items`
--

DROP TABLE IF EXISTS `order_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_items` (
  `id` int NOT NULL AUTO_INCREMENT,
  `itemId` int DEFAULT NULL,
  `itemName` varchar(255) DEFAULT NULL,
  `promotionId` int DEFAULT NULL,
  `promotionName` varchar(255) DEFAULT NULL,
  `itemList` longtext,
  `unit` varchar(255) DEFAULT NULL,
  `unitAmount` varchar(255) DEFAULT NULL,
  `avgPrepareTime` int DEFAULT NULL,
  `diningStatus` int DEFAULT NULL,
  `unitCost` double DEFAULT NULL,
  `unitPrice` double DEFAULT NULL,
  `quantity` int DEFAULT NULL,
  `processingStatus` int DEFAULT NULL,
  `serviceCharge` double DEFAULT NULL,
  `serviceChargeType` int DEFAULT NULL,
  `orderId` int DEFAULT NULL,
  `hotelId` int DEFAULT NULL,
  `restaurantId` int DEFAULT NULL,
  `departmentId` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `itemId` (`itemId`),
  KEY `promotionId` (`promotionId`),
  KEY `orderId` (`orderId`),
  KEY `hotelId` (`hotelId`),
  KEY `restaurantId` (`restaurantId`),
  KEY `departmentId` (`departmentId`),
  CONSTRAINT `order_items_ibfk_1` FOREIGN KEY (`itemId`) REFERENCES `items` (`id`),
  CONSTRAINT `order_items_ibfk_2` FOREIGN KEY (`promotionId`) REFERENCES `promotions` (`id`),
  CONSTRAINT `order_items_ibfk_3` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`),
  CONSTRAINT `order_items_ibfk_4` FOREIGN KEY (`hotelId`) REFERENCES `hotels` (`id`),
  CONSTRAINT `order_items_ibfk_5` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`),
  CONSTRAINT `order_items_ibfk_6` FOREIGN KEY (`departmentId`) REFERENCES `departments` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_items`
--

LOCK TABLES `order_items` WRITE;
/*!40000 ALTER TABLE `order_items` DISABLE KEYS */;
INSERT INTO `order_items` VALUES (1,3,'Chicken Kottu',NULL,NULL,NULL,'1','1',15,0,1250,1600,1,0,10.56,1,7,1,1,1,'2023-05-03 11:59:05','2023-05-03 11:59:05'),(2,5,'Egg Role',NULL,NULL,NULL,'1','1',15,0,150,250,2,0,10.56,1,7,1,1,1,'2023-05-03 11:59:05','2023-05-03 11:59:05'),(3,NULL,NULL,2,'Biriyani Sawan','[{\"itemId\":2,\"promotionId\":2,\"itemName\":\"wild apple\",\"quantity\":2,\"unit\":\"ml\",\"unitAmount\":\"100\",\"departmentId\":1,\"restaurantId\":1,\"hotelId\":1},{\"itemId\":3,\"promotionId\":2,\"itemName\":\"Chicken Kottu\",\"quantity\":1,\"unit\":\"1\",\"unitAmount\":\"1\",\"departmentId\":1,\"restaurantId\":1,\"hotelId\":1}]',NULL,NULL,45,0,3000,3500,1,0,20.67,0,7,1,1,NULL,'2023-05-03 11:59:05','2023-05-03 11:59:05'),(4,3,'Chicken Kottu',NULL,NULL,NULL,'1','1',15,0,1250,1600,1,0,10.56,1,8,1,1,1,'2023-05-03 12:30:23','2023-05-03 12:30:23'),(5,5,'Egg Role',NULL,NULL,NULL,'1','1',15,0,150,250,2,0,10.56,1,8,1,1,1,'2023-05-03 12:30:23','2023-05-03 12:30:23'),(6,NULL,NULL,2,'Biriyani Sawan','[{\"itemId\":2,\"promotionId\":2,\"itemName\":\"wild apple\",\"quantity\":2,\"unit\":\"ml\",\"unitAmount\":\"100\",\"departmentId\":1,\"restaurantId\":1,\"hotelId\":1},{\"itemId\":3,\"promotionId\":2,\"itemName\":\"Chicken Kottu\",\"quantity\":1,\"unit\":\"1\",\"unitAmount\":\"1\",\"departmentId\":1,\"restaurantId\":1,\"hotelId\":1}]',NULL,NULL,45,0,3000,3500,1,0,20.67,0,8,1,1,NULL,'2023-05-03 12:30:23','2023-05-03 12:30:23'),(7,3,'Chicken Kottu',NULL,NULL,NULL,'1','1',15,0,1250,1600,1,0,10.56,1,9,1,1,1,'2023-05-03 12:43:37','2023-05-03 12:43:37'),(8,5,'Egg Role',NULL,NULL,NULL,'1','1',15,0,150,250,2,0,10.56,1,9,1,1,1,'2023-05-03 12:43:37','2023-05-03 12:43:37'),(9,NULL,NULL,2,'Biriyani Sawan','[{\"itemId\":2,\"promotionId\":2,\"itemName\":\"wild apple\",\"quantity\":2,\"unit\":\"ml\",\"unitAmount\":\"100\",\"departmentId\":1,\"restaurantId\":1,\"hotelId\":1},{\"itemId\":3,\"promotionId\":2,\"itemName\":\"Chicken Kottu\",\"quantity\":1,\"unit\":\"1\",\"unitAmount\":\"1\",\"departmentId\":1,\"restaurantId\":1,\"hotelId\":1}]',NULL,NULL,45,0,3000,3500,1,0,20.67,0,9,1,1,NULL,'2023-05-03 12:43:37','2023-05-03 12:43:37'),(10,3,'Chicken Kottu',NULL,NULL,NULL,'1','1',15,0,1250,1600,1,0,10.56,1,10,1,1,1,'2023-05-03 13:22:47','2023-05-03 13:22:47'),(11,5,'Egg Role',NULL,NULL,NULL,'1','1',15,0,150,250,2,0,10.56,1,10,1,1,1,'2023-05-03 13:22:47','2023-05-03 13:22:47'),(12,NULL,NULL,2,'Biriyani Sawan','[{\"itemId\":2,\"promotionId\":2,\"itemName\":\"wild apple\",\"quantity\":2,\"unit\":\"ml\",\"unitAmount\":\"100\",\"departmentId\":1,\"restaurantId\":1,\"hotelId\":1},{\"itemId\":3,\"promotionId\":2,\"itemName\":\"Chicken Kottu\",\"quantity\":1,\"unit\":\"1\",\"unitAmount\":\"1\",\"departmentId\":1,\"restaurantId\":1,\"hotelId\":1}]',NULL,NULL,45,1,3000,3500,1,0,20.67,0,10,1,1,NULL,'2023-05-03 13:22:47','2023-05-03 13:22:47'),(13,3,'Chicken Kottu',NULL,NULL,NULL,'1','1',15,0,1250,1600,1,0,10.56,1,11,1,1,1,'2023-05-03 13:23:44','2023-05-03 13:23:44'),(14,5,'Egg Role',NULL,NULL,NULL,'1','1',15,0,150,250,2,0,10.56,1,11,1,1,1,'2023-05-03 13:23:44','2023-05-03 13:23:44'),(15,NULL,NULL,2,'Biriyani Sawan','[{\"itemId\":2,\"promotionId\":2,\"itemName\":\"wild apple\",\"quantity\":2,\"unit\":\"ml\",\"unitAmount\":\"100\",\"departmentId\":1,\"restaurantId\":1,\"hotelId\":1},{\"itemId\":3,\"promotionId\":2,\"itemName\":\"Chicken Kottu\",\"quantity\":1,\"unit\":\"1\",\"unitAmount\":\"1\",\"departmentId\":1,\"restaurantId\":1,\"hotelId\":1}]',NULL,NULL,45,1,3000,3500,1,0,20.67,0,11,1,1,NULL,'2023-05-03 13:23:44','2023-05-03 13:23:44'),(16,3,'Chicken Kottu',NULL,NULL,NULL,'1','1',15,0,1250,1600,1,0,10.56,1,12,1,1,1,'2023-05-03 13:25:01','2023-05-03 13:25:01'),(17,5,'Egg Role',NULL,NULL,NULL,'1','1',15,0,150,250,2,0,10.56,1,12,1,1,1,'2023-05-03 13:25:01','2023-05-03 13:25:01'),(18,NULL,NULL,2,'Biriyani Sawan','[{\"itemId\":2,\"promotionId\":2,\"itemName\":\"wild apple\",\"quantity\":2,\"unit\":\"ml\",\"unitAmount\":\"100\",\"departmentId\":1,\"restaurantId\":1,\"hotelId\":1},{\"itemId\":3,\"promotionId\":2,\"itemName\":\"Chicken Kottu\",\"quantity\":1,\"unit\":\"1\",\"unitAmount\":\"1\",\"departmentId\":1,\"restaurantId\":1,\"hotelId\":1}]',NULL,NULL,45,1,3000,3500,1,0,20.67,0,12,1,1,NULL,'2023-05-03 13:25:01','2023-05-03 13:25:01');
/*!40000 ALTER TABLE `order_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orders` (
  `id` int NOT NULL AUTO_INCREMENT,
  `orderDateTime` datetime DEFAULT NULL,
  `paymentStatus` int DEFAULT NULL,
  `customerId` int DEFAULT NULL,
  `userId` int DEFAULT NULL,
  `userType` varchar(255) DEFAULT NULL,
  `serviceCharge` double DEFAULT NULL,
  `taxRate` double DEFAULT NULL,
  `cancelStatus` int DEFAULT NULL,
  `tableId` int DEFAULT NULL,
  `hotelId` int DEFAULT NULL,
  `restaurantId` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `customerId` (`customerId`),
  KEY `userId` (`userId`),
  KEY `tableId` (`tableId`),
  KEY `hotelId` (`hotelId`),
  KEY `restaurantId` (`restaurantId`),
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`customerId`) REFERENCES `customers` (`id`),
  CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`tableId`) REFERENCES `tables` (`id`),
  CONSTRAINT `orders_ibfk_4` FOREIGN KEY (`hotelId`) REFERENCES `hotels` (`id`),
  CONSTRAINT `orders_ibfk_5` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (7,'2023-05-03 11:59:05',0,163,1,'owner',242.43,3.5,0,NULL,1,1,'2023-05-03 11:59:05','2023-05-03 11:59:05'),(8,'2023-05-03 12:30:23',0,163,1,'owner',242.43,3.5,0,NULL,1,1,'2023-05-03 12:30:23','2023-05-03 12:30:23'),(9,'2023-05-03 12:43:37',1,163,1,'owner',242.43,3.5,0,NULL,1,1,'2023-05-03 12:43:37','2023-05-03 12:57:46'),(10,'2023-05-03 13:22:47',1,163,1,'owner',242.43,NULL,0,1,1,1,'2023-05-03 13:22:47','2023-05-03 13:22:47'),(11,'2023-05-03 13:23:44',1,163,1,'owner',242.43,NULL,0,1,1,1,'2023-05-03 13:23:44','2023-05-03 13:23:44'),(12,'2023-05-03 13:25:01',1,163,1,'owner',242.43,3.5,0,1,1,1,'2023-05-03 13:25:01','2023-05-03 13:25:01');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `owners`
--

DROP TABLE IF EXISTS `owners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `owners` (
  `id` int NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `userName` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `emailValidation` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `owners`
--

LOCK TABLES `owners` WRITE;
/*!40000 ALTER TABLE `owners` DISABLE KEYS */;
INSERT INTO `owners` VALUES (1,'kasun','thenuwara','kasun','test@gmail.com','0771332465','$2a$10$eXeK97L6xVad61mD6QhKOeWN0DDBv1hOQAeBy6Qt0HR92qo4BVi3O',NULL,'2023-04-05 10:18:22','2023-04-05 10:18:22'),(2,'tharindu','thenuwara','tharindu','test2@gmail.com','0771331465','$2a$10$IkOhoFvwehd4zNxVnvNh1.MnxcrFjxmI0q50KPVwfI9rPhcJx6nc.',NULL,'2023-04-06 08:56:31','2023-04-06 08:56:31');
/*!40000 ALTER TABLE `owners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promotion_items`
--

DROP TABLE IF EXISTS `promotion_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `promotion_items` (
  `id` int NOT NULL AUTO_INCREMENT,
  `promotionId` int DEFAULT NULL,
  `itemId` int DEFAULT NULL,
  `quantity` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `promotionId` (`promotionId`),
  KEY `itemId` (`itemId`),
  CONSTRAINT `promotion_items_ibfk_1` FOREIGN KEY (`promotionId`) REFERENCES `promotions` (`id`),
  CONSTRAINT `promotion_items_ibfk_2` FOREIGN KEY (`itemId`) REFERENCES `items` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promotion_items`
--

LOCK TABLES `promotion_items` WRITE;
/*!40000 ALTER TABLE `promotion_items` DISABLE KEYS */;
INSERT INTO `promotion_items` VALUES (23,1,2,2,'2023-04-07 03:14:40','2023-04-07 03:14:40'),(24,2,2,2,'2023-04-07 05:02:06','2023-04-07 05:02:06'),(25,2,3,1,'2023-04-07 05:02:06','2023-04-07 05:02:06');
/*!40000 ALTER TABLE `promotion_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promotions`
--

DROP TABLE IF EXISTS `promotions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `promotions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `promotionName` varchar(255) DEFAULT NULL,
  `promotionDescription` varchar(255) DEFAULT NULL,
  `unitPrice` double DEFAULT NULL,
  `unitCost` double DEFAULT NULL,
  `serviceCharge` double DEFAULT NULL,
  `serviceChargeType` int DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `fromDate` date DEFAULT NULL,
  `toDate` date DEFAULT NULL,
  `serveTime` varchar(255) DEFAULT NULL,
  `availability` int DEFAULT NULL,
  `avgPrepareTime` int DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `hotelId` int DEFAULT NULL,
  `restaurantId` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hotelId` (`hotelId`),
  KEY `restaurantId` (`restaurantId`),
  CONSTRAINT `promotions_ibfk_1` FOREIGN KEY (`hotelId`) REFERENCES `hotels` (`id`),
  CONSTRAINT `promotions_ibfk_2` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promotions`
--

LOCK TABLES `promotions` WRITE;
/*!40000 ALTER TABLE `promotions` DISABLE KEYS */;
INSERT INTO `promotions` VALUES (1,'Family Pack','test1 v-1 promotion description',2700,2100,20.67,0,'[\"keyword1\" , \"keyword2\" , \"keyword3\"]','2023-04-01','2023-05-24','[{\"startTime\":\"06:55:00\",\"endTime\":\"12:00:00\"},{\"startTime\":\"12:00:00\",\"endTime\":\"22:00:00\"}]',1,30,'1680837280739.jpeg',1,1,'2023-04-06 07:16:04','2023-04-25 07:49:22'),(2,'Biriyani Sawan','test1 promotion description',3500,3000,20.67,0,'[\"keyword1\" , \"keyword2\" , \"keyword3\"]','2023-04-20','2023-05-24','[{\"startTime\":\"05:55:00\",\"endTime\":\"12:00:00\"},{\"startTime\":\"12:00:00\",\"endTime\":\"22:00:00\"}]',1,45,'1680843726016.jpg',1,1,'2023-04-07 05:02:06','2023-04-07 05:02:06');
/*!40000 ALTER TABLE `promotions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurants`
--

DROP TABLE IF EXISTS `restaurants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `restaurants` (
  `id` int NOT NULL AUTO_INCREMENT,
  `restaurantName` varchar(255) DEFAULT NULL,
  `restaurantDescription` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `restaurantStatus` int DEFAULT NULL,
  `statusComment` varchar(255) DEFAULT NULL,
  `ownerId` int DEFAULT NULL,
  `hotelId` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ownerId` (`ownerId`),
  KEY `hotelId` (`hotelId`),
  CONSTRAINT `restaurants_ibfk_1` FOREIGN KEY (`ownerId`) REFERENCES `owners` (`id`),
  CONSTRAINT `restaurants_ibfk_2` FOREIGN KEY (`hotelId`) REFERENCES `hotels` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurants`
--

LOCK TABLES `restaurants` WRITE;
/*!40000 ALTER TABLE `restaurants` DISABLE KEYS */;
INSERT INTO `restaurants` VALUES (1,'hells kitchen-v1','roof top','0111330065',1,NULL,1,1,'2023-04-05 10:18:42','2023-04-05 10:18:42'),(2,'hells kitchen-v1 blue','roof top','0111330065',1,NULL,2,2,'2023-04-10 06:31:29','2023-04-10 06:31:29');
/*!40000 ALTER TABLE `restaurants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `roleName` varchar(255) DEFAULT NULL,
  `permissionLevel` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roleName` (`roleName`),
  UNIQUE KEY `permissionLevel` (`permissionLevel`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'manager','[\"manager\"]','2023-04-05 10:18:59','2023-04-05 10:18:59'),(2,'chef','[\"chef\"]','2023-04-12 04:21:09','2023-04-12 04:21:09');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tables`
--

DROP TABLE IF EXISTS `tables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tables` (
  `id` int NOT NULL AUTO_INCREMENT,
  `tableStatus` int DEFAULT NULL,
  `tableName` varchar(255) DEFAULT NULL,
  `numberOfChairs` int DEFAULT NULL,
  `enableStatus` int DEFAULT NULL,
  `hotelId` int DEFAULT NULL,
  `restaurantId` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hotelId` (`hotelId`),
  KEY `restaurantId` (`restaurantId`),
  CONSTRAINT `tables_ibfk_1` FOREIGN KEY (`hotelId`) REFERENCES `hotels` (`id`),
  CONSTRAINT `tables_ibfk_2` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tables`
--

LOCK TABLES `tables` WRITE;
/*!40000 ALTER TABLE `tables` DISABLE KEYS */;
INSERT INTO `tables` VALUES (1,1,'table number 1 v-1',5,1,1,1,'2023-04-10 04:59:42','2023-05-03 13:25:01');
/*!40000 ALTER TABLE `tables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taxes`
--

DROP TABLE IF EXISTS `taxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `taxes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `taxRate` double DEFAULT NULL,
  `restaurantId` int DEFAULT NULL,
  `ownerId` int DEFAULT NULL,
  `hotelId` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `restaurantId` (`restaurantId`),
  KEY `ownerId` (`ownerId`),
  KEY `hotelId` (`hotelId`),
  CONSTRAINT `taxes_ibfk_1` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`),
  CONSTRAINT `taxes_ibfk_2` FOREIGN KEY (`ownerId`) REFERENCES `owners` (`id`),
  CONSTRAINT `taxes_ibfk_3` FOREIGN KEY (`hotelId`) REFERENCES `hotels` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taxes`
--

LOCK TABLES `taxes` WRITE;
/*!40000 ALTER TABLE `taxes` DISABLE KEYS */;
INSERT INTO `taxes` VALUES (1,3.5,1,1,1,'2023-04-19 07:27:40','2023-04-19 13:14:37');
/*!40000 ALTER TABLE `taxes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_details`
--

DROP TABLE IF EXISTS `transaction_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction_details` (
  `id` int NOT NULL AUTO_INCREMENT,
  `orderId` int DEFAULT NULL,
  `totalCost` double DEFAULT NULL,
  `totalPrice` double DEFAULT NULL,
  `totalServiceCharge` double DEFAULT NULL,
  `profit` double DEFAULT NULL,
  `cardNumber` varchar(255) DEFAULT NULL,
  `paymentDetails` varchar(255) DEFAULT NULL,
  `balance` double DEFAULT NULL,
  `hotelId` int DEFAULT NULL,
  `restaurantId` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderId` (`orderId`),
  KEY `hotelId` (`hotelId`),
  KEY `restaurantId` (`restaurantId`),
  CONSTRAINT `transaction_details_ibfk_1` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`),
  CONSTRAINT `transaction_details_ibfk_2` FOREIGN KEY (`hotelId`) REFERENCES `hotels` (`id`),
  CONSTRAINT `transaction_details_ibfk_3` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_details`
--

LOCK TABLES `transaction_details` WRITE;
/*!40000 ALTER TABLE `transaction_details` DISABLE KEYS */;
INSERT INTO `transaction_details` VALUES (1,9,4550,6038.43,242.43,1488.4300000000003,'xxxx','{\"card\":{\"amount\":110000,\"cardNumber\":\"xxxx\"},\"cash\":{\"amount\":500}}',104461.57,1,1,'2023-05-03 12:57:46','2023-05-03 12:57:46'),(2,NULL,5100,6038.43,242.43,938.4300000000003,'xxxx','{\"card\":{\"amount\":110000,\"cardNumber\":\"xxxx\"},\"cash\":{\"amount\":500}}',104461.57,1,1,'2023-05-03 13:22:47','2023-05-03 13:22:47'),(3,11,5100,6038.43,242.43,938.4300000000003,'xxxx','{\"card\":{\"amount\":110000,\"cardNumber\":\"xxxx\"},\"cash\":{\"amount\":500}}',104461.57,1,1,'2023-05-03 13:23:44','2023-05-03 13:23:44'),(4,12,5100,6038.43,242.43,938.4300000000003,'xxxx','{\"card\":{\"amount\":110000,\"cardNumber\":\"xxxx\"},\"cash\":{\"amount\":500}}',104461.57,1,1,'2023-05-03 13:25:01','2023-05-03 13:25:01');
/*!40000 ALTER TABLE `transaction_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `userName` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `employeeNumber` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `roleId` int DEFAULT NULL,
  `createdOwner` int DEFAULT NULL,
  `hotelId` int DEFAULT NULL,
  `restaurantId` int DEFAULT NULL,
  `departmentId` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `roleId` (`roleId`),
  KEY `createdOwner` (`createdOwner`),
  KEY `hotelId` (`hotelId`),
  KEY `restaurantId` (`restaurantId`),
  KEY `departmentId` (`departmentId`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`roleId`) REFERENCES `roles` (`id`),
  CONSTRAINT `users_ibfk_2` FOREIGN KEY (`createdOwner`) REFERENCES `owners` (`id`),
  CONSTRAINT `users_ibfk_3` FOREIGN KEY (`hotelId`) REFERENCES `hotels` (`id`),
  CONSTRAINT `users_ibfk_4` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`),
  CONSTRAINT `users_ibfk_5` FOREIGN KEY (`departmentId`) REFERENCES `departments` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'vishwa','mahanama','vishwa-v2','$2a$10$uRCbe2kh4zSstLVXG/pp0.qfR43gs9leNBq8AGySN6dg.vLs4PCFK','emp-001','0771039533',1,1,1,1,1,'2023-04-05 10:19:16','2023-04-05 10:19:16'),(2,'kasun','thenuwara','kasun-emp','$2a$10$9nZpLKblpQ9kKJn1e36jDenJNuKOIoUXEij7P0mZutkl2RDV1Ij2C','emp-001','0771039534',2,1,1,1,1,'2023-04-12 04:21:56','2023-04-12 04:21:56');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-05-03 14:44:53
